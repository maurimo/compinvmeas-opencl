#ifndef __EXCEPTIONS_H__
#define __EXCEPTIONS_H__

#include <string>
#include <exception>
#include "format.h"

class BasicException : public std::exception {
protected:
	BasicException() {
	}
public:
	BasicException(const std::string& message) throw()
		: _message(message) {
	}
	BasicException(const char* fmt, ...) throw() __attribute__((__format__ (__printf__, 2, 3))) {
		va_list ap;

		va_start(ap, fmt);
		_message = vstrf(fmt, ap);
		va_end(ap);
	}
	~BasicException() throw() {}
	std::string message() throw() {
		return _message;
	}
protected:
	std::string _message;
};

class ParseException : public BasicException {
public:
	ParseException(const std::string& message)
		: BasicException(message) {
	}
	ParseException(const char* fmt, ...) throw() __attribute__((__format__ (__printf__, 2, 3))) {
		va_list ap;

		va_start(ap, fmt);
		_message = vstrf(fmt, ap);
		va_end(ap);
	}
};

class UsageException : public BasicException {
 public:
	UsageException(const std::string& message)
		: BasicException(message) {
	}
	UsageException(const char* fmt, ...) throw() __attribute__((__format__ (__printf__, 2, 3))) {
		va_list ap;

		va_start(ap, fmt);
		_message = vstrf(fmt, ap);
		va_end(ap);
	}
};

#endif //__EXCEPTIONS_H__

// Local Variables:
// mode: c++
// End:
