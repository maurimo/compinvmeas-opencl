#ifndef __CL_MATRIX_SPARSE_H__
#define __CL_MATRIX_GROUPED_INPUT_H__

#include <CL/cl.hpp>
#include "matrix.h"
#include "cl_matrix_dense.h"

/** Matrix with condensing data for grouping input data.

    It is efficient when in a group of consecutive rows, the set of columns
    set is smaller than the total number of non-zero in such rows. This is the
    case for matrices coming from piecewise expanding dynamical systems, where
    the rows have non-zero column sets that change slowly.
 */ 
template<typename I, typename D>
class ClMatrixGroupedInput : public Matrix<I> {
public:
	int block_size;
    cl::Buffer row_ptr;
    cl::Buffer lookup_b;
    cl::Buffer lookup_range_b;
    cl::Buffer loc_col_b;
    cl::Buffer data;

	ClMatrixGroupedInput(const MatrixCsr<I, D>& mat,
	               cl::Context& ctx,
	               cl::CommandQueue& queue);
};

/* multiplier to apply a csr to row-wise dense matrices */
template<typename I, typename D>
class ClMultiplierGroupedInputCw {
public:
	int block_size;
	cl::Program mult_prog;
	cl::Kernel mult;
	I mw, mh, iw, ih, ow, oh;

	ClMultiplierGroupedInputCw(cl::Context& context,
	                           cl::Device& device);
	void bind_mat(const ClMatrixGroupedInput<I, D>&);
	void bind_in(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void run(cl::CommandQueue& queue);
};

#endif //__CL_MATRIX_GROUPED_INPUT_H__

// Local Variables:
// mode: c++
// End:
