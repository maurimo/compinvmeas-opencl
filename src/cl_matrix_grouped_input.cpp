
#include <set>
#include <map>
#include "exceptions.h"
#include "scalar.h"
#include "cl_utils.h"
#include "cl_matrix_grouped_input.h"

#define LOCAL_SIZE 4096

template<typename I, typename D>
ClMatrixGroupedInput<I,D>::ClMatrixGroupedInput(const MatrixCsr<I, D>& mat,
                                    cl::Context& context,
                                    cl::CommandQueue& queue) 
	: Matrix<I>(mat) {

	unsigned int available_local_mem = LOCAL_SIZE;
	block_size = 256;

	while(true) {
		std::cout << "Try bs = " << block_size << std::endl;
		bool bad = false;
		std::vector<I> lookup;
		std::vector<I> lookup_range;
		std::vector<I> loc_col;

		lookup_range.push_back(lookup.size());
		for(I r = 0; r < mat.height; r += block_size) {
			std::set<I> cols;
			std::map<I,I> col_to_loc;
			int tot = 0;
			for(I i=r; i<std::min(r+block_size, mat.height); ++i) {
				for(I j=mat.row_ptr[i]; j<mat.row_ptr[i+1]; ++j) {
					tot ++;
					cols.insert(mat.col_id[j]);
				}
			}
			if(cols.size() > available_local_mem) {
				bad = true;
				block_size /= 2;
				if(block_size < 4)
					throw UsageException("Too many elements in some rows, cannot\n"
					                     "  copy all of them in GPU local memory!");
				else
					std::cout << "Too many local data, reducing group size to "
					          << block_size << std::endl;
				break;
			}

			I locidx = 0;
			for(I c: cols) {
				lookup.push_back(c);
				col_to_loc[c] = locidx;
				locidx++;
			}
			lookup_range.push_back(lookup.size());

			for(I i=r; i<std::min(r+block_size, mat.height); ++i) {
				for(I j=mat.row_ptr[i]; j<mat.row_ptr[i+1]; ++j) {
					tot ++;
					loc_col.push_back(col_to_loc[mat.col_id[j]]);
				}
			}
		}

		if(bad)
			continue;
		
		row_ptr = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(I)*(this->height+1));
		lookup_b = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(I)*lookup.size());
		lookup_range_b = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(I)*lookup_range.size());
		loc_col_b = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(I)*this->nnz);
		data = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(D)*this->nnz);

		queue.enqueueWriteBuffer(row_ptr, CL_TRUE, 0, sizeof(I)*(this->height+1), mat.row_ptr);
		queue.enqueueWriteBuffer(lookup_b, CL_TRUE, 0, sizeof(I)*lookup.size(), lookup.data());
		queue.enqueueWriteBuffer(lookup_range_b, CL_TRUE, 0, sizeof(I)*lookup_range.size(),
		                         lookup_range.data());
		queue.enqueueWriteBuffer(loc_col_b, CL_TRUE, 0, sizeof(I)*this->nnz, loc_col.data());
		queue.enqueueWriteBuffer(data, CL_TRUE, 0, sizeof(D)*this->nnz, mat.data);
		break;
	}
}

template<typename I, typename D>
ClMultiplierGroupedInputCw<I, D>::ClMultiplierGroupedInputCw(cl::Context& context,
                                                       cl::Device& device) {
	mult_prog = create_program(context, device,
	                           {"cl/grouped_input_mult.cl"},
	                           Scalar<D>::Flags);
	mult = cl::Kernel(mult_prog, "gi_mult_cw");
}

template<typename I, typename D>
void ClMultiplierGroupedInputCw<I, D>::bind_mat(const ClMatrixGroupedInput<I, D>& mat) {
	block_size = mat.block_size;
	mw = mat.width;
	mh = mat.height;
	mult.setArg(0, mat.data);
	mult.setArg(1, mat.row_ptr);
	mult.setArg(2, mat.loc_col_b);
	mult.setArg(3, mat.lookup_b);
	mult.setArg(4, mat.lookup_range_b);
	mult.setArg(5, mat.height);
}

template<typename I, typename D>
void ClMultiplierGroupedInputCw<I, D>::bind_in(const ClMatrixDense<I, D>& in) {
	iw = in.width;
	ih = in.height;
	mult.setArg(6, in.data);
}

template<typename I, typename D>
void ClMultiplierGroupedInputCw<I, D>::bind_out(const ClMatrixDense<I, D>& out) {
	ow = out.width;
	oh = out.height;
	mult.setArg(7, out.data);
}

template<typename I, typename D>
void ClMultiplierGroupedInputCw<I, D>::run(cl::CommandQueue& queue) {
	if(ow != iw || oh != mh || mw != ih)
		throw UsageException("Multiply M_%dx%d <- M_%dx%d * M_%dx%d",
		                     oh, ow, mh, mw, ih, iw);
	uint32_t gsize = (mh+block_size-1)&(-block_size), lsize = block_size;
	queue.enqueueNDRangeKernel(mult,
	                           cl::NullRange,
	                           cl::NDRange(gsize),
	                           cl::NDRange(lsize) );
}


/* explicit template instantiation */
template class ClMatrixGroupedInput<uint32_t, float>;
template class ClMatrixGroupedInput<uint32_t, double>;
template class ClMultiplierGroupedInputCw<uint32_t, float>;
template class ClMultiplierGroupedInputCw<uint32_t, double>;
