#ifndef __CL_NORM_H__
#define __CL_NORM_H__

#include <CL/cl.hpp>
#include "cl_matrix_dense.h"

/* initialize columns in a cw matrix to some basis */
template<typename I, typename D>
class ClNormCw {
public:
	uint32_t mh, mw, oh, ow;
	cl::Program norm_prog;
	cl::Kernel k_L1;
	cl::Kernel k_Linf;

	ClNormCw(cl::Context& context,
	              cl::Device& device);
	void bind_mat(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void compute_L1(cl::CommandQueue& queue, uint32_t from = 0, uint32_t to = 0);
	void compute_Linf(cl::CommandQueue& queue, uint32_t from = 0, uint32_t to = 0);
};


/* initialize columns in a cw matrix to some basis */
template<typename I, typename D>
class ClNormRw {
public:
	uint32_t mh, mw, oh, ow;
	cl::Program norm_prog;
	cl::Kernel k_L1;
	cl::Kernel k_Linf;
	uint32_t k_L1_d2size, k_Linf_d2size;

	ClNormRw(cl::Context& context,
	              cl::Device& device);
	void bind_mat(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void compute_L1(cl::CommandQueue& queue, uint32_t from = 0, uint32_t to = 0);
	void compute_Linf(cl::CommandQueue& queue, uint32_t from = 0, uint32_t to = 0);
};

#endif //__CL_NORM_H__

// Local Variables:
// mode: c++
// End:
