
#define __CL_ENABLE_EXCEPTIONS
#include "exceptions.h"
#include "cl_utils.h"
#include "scalar.h"
#include "cl_matrix_csr.h"

template<typename I, typename D>
ClMatrixCsr<I, D>::ClMatrixCsr(const MatrixCsr<I, D>& mat,
                                   cl::Context& context,
                                   cl::CommandQueue& queue)
	: Matrix<I>(mat) {
	row_ptr = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(I)*(this->height+1));
	col_id = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(I)*this->nnz);
	data = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(D)*this->nnz);

	queue.enqueueWriteBuffer(row_ptr, CL_TRUE, 0, sizeof(I)*(this->height+1), mat.row_ptr);
	queue.enqueueWriteBuffer(col_id, CL_TRUE, 0, sizeof(I)*this->nnz, mat.col_id);
	queue.enqueueWriteBuffer(data, CL_TRUE, 0, sizeof(D)*this->nnz, mat.data);
}

template<typename I, typename D>
ClMultiplierCsrCw<I, D>::ClMultiplierCsrCw(cl::Context& context,
                                           cl::Device& device) {
	mult_prog = create_program(context, device,
	                           {"cl/csr_mult.cl"},
	                           Scalar<D>::Flags);
	mult = cl::Kernel(mult_prog, "csr_mult_cw");
}

template<typename I, typename D>
void ClMultiplierCsrCw<I, D>::bind_mat(const ClMatrixCsr<I, D>& mat) {
	mw = mat.width;
	mh = mat.height;
	mult.setArg(0, mat.data);
	mult.setArg(1, mat.row_ptr);
	mult.setArg(2, mat.col_id);
	mult.setArg(3, mat.width);
	mult.setArg(4, mat.height);
}

template<typename I, typename D>
void ClMultiplierCsrCw<I, D>::bind_in(const ClMatrixDense<I, D>& in) {
	iw = in.width;
	ih = in.height;
	mult.setArg(5, in.data);
	mult.setArg(6, in.width);
}

template<typename I, typename D>
void ClMultiplierCsrCw<I, D>::bind_out(const ClMatrixDense<I, D>& out) {
	ow = out.width;
	oh = out.height;
	mult.setArg(7, out.data);
}

template<typename I, typename D>
void ClMultiplierCsrCw<I, D>::run(cl::CommandQueue& queue) {
	if(ow != iw || oh != mh || mw != ih)
		throw UsageException("Multiply M_%dx%d <- M_%dx%d * M_%dx%d",
		                     oh, ow, mh, mw, ih, iw);
	uint32_t gsize = iw, lsize;
	compute_best_local_decomposition(gsize, lsize);
	queue.enqueueNDRangeKernel(mult,
	                           cl::NullRange,
	                           cl::NDRange(mh, gsize),
	                           cl::NDRange(1, lsize) );
}

template<typename I, typename D>
ClMultiplierCsrRw<I, D>::ClMultiplierCsrRw(cl::Context& context,
                                                       cl::Device& device) {
	mult_prog = create_program(context, device,
	                           {"cl/csr_mult.cl"},
	                           Scalar<D>::Flags);
	mult = cl::Kernel(mult_prog, "csr_mult_rw");
}

template<typename I, typename D>
void ClMultiplierCsrRw<I, D>::bind_mat(const ClMatrixCsr<I, D>& mat) {
	mw = mat.width;
	mh = mat.height;
	mult.setArg(0, mat.data);
	mult.setArg(1, mat.row_ptr);
	mult.setArg(2, mat.col_id);
}

template<typename I, typename D>
void ClMultiplierCsrRw<I, D>::bind_in(const ClMatrixDense<I, D>& in) {
	iw = in.width;
	ih = in.height;
	mult.setArg(3, in.data);
	mult.setArg(4, in.width);
}

template<typename I, typename D>
void ClMultiplierCsrRw<I, D>::bind_out(const ClMatrixDense<I, D>& out) {
	ow = out.width;
	oh = out.height;
	mult.setArg(5, out.data);
}

template<typename I, typename D>
void ClMultiplierCsrRw<I, D>::run(cl::CommandQueue& queue) {
	if(ow != iw || oh != mh || mw != ih)
		throw UsageException("Multiply M_%dx%d <- M_%dx%d * M_%dx%d",
		                     oh, ow, mh, mw, ih, iw);
	uint32_t gsize = iw, lsize;
	compute_best_local_decomposition(gsize, lsize);
	queue.enqueueNDRangeKernel(mult,
	                           cl::NullRange,
	                           cl::NDRange(mh, gsize),
	                           cl::NDRange(1, lsize) );
}

/* explicit template instantiation */
template class ClMatrixCsr<uint32_t, float>;
template class ClMatrixCsr<uint32_t, double>;
template class ClMultiplierCsrCw<uint32_t, float>;
template class ClMultiplierCsrCw<uint32_t, double>;
template class ClMultiplierCsrRw<uint32_t, float>;
template class ClMultiplierCsrRw<uint32_t, double>;
