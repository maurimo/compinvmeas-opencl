#ifndef __CL_UTILS_H__
#define __CL_UTILS_H__

#include <vector>
#include <string>
#include <ostream>
#include <CL/cl.hpp>

/* Get default platform */
cl::Platform get_platform(unsigned int platform_index = 0);

/* Get default device */
cl::Device get_device(cl::Platform platform,
                      cl_device_type type = CL_DEVICE_TYPE_ALL,
                      unsigned int device_index = 0);

/* Get device using the -p and -d command line arguments for
   platform and device, and reading the CL_PLATFORM and CL_DEVICE
   environment variables (command line args have the priority) */
cl::Device get_device_from_params(int argc, char* argv[]);

/* Creates a program from a file list */
cl::Program create_program(cl::Context context,
                           cl::Device dev,
                           const std::vector<const char*>& files,
                           const std::string& flags = "");

void compute_best_local_decomposition(uint32_t& gsize, uint32_t& lsize);

/* prints an opencl error */
void print_opencl_error_code(cl_int error);

#endif //__CL_UTILS_H__

// Local Variables:
// mode: c++
// End:
