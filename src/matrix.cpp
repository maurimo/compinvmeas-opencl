#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <fstream>
#include <sstream>
#include <limits>
#include <boost/filesystem.hpp>
#include "exceptions.h"
#include "mmap.h"
#include "matrix.h"
#include "scalar.h"

using namespace std;
namespace fs = boost::filesystem;

template<typename I, typename D>
void save_cw_plain_text(const char* filename, MatrixDense<I,D>& mat) {
	ofstream os(filename);
	os.precision(numeric_limits<D>::max_digits10);

	for(I r = 0; r < mat.height; ++r) {
		for(I c = 0, off = r; c < mat.width; ++c, off += mat.height) {
			if(c != 0)
				os << " ";
			//os << mat.data[off];
			os << mat.data[r * mat.width + c];
		}
		os << endl;
	}
}


template<typename I, typename D>
void save_rw_plain_text(const char* filename, MatrixDense<I,D>& mat) {
	ofstream os(filename);
	os.precision(numeric_limits<D>::max_digits10);

	for(I r = 0; r < mat.height; ++r) {
		for(I c = 0; c < mat.width; ++c) {
			if(c != 0)
				os << " ";
			//os << mat.data[off];
			os << mat.data[r * mat.width + c];
		}
		os << endl;
	}
}

template<typename I, typename D>
void load_cw_plain_text(const char* filename, MatrixDense<I,D>& mat) {
	list<vector<D> > data;
	ifstream is(filename);
	string line;

	while(getline(is, line)) {
		if(line.empty() || line[0] == '%')
			continue;
		stringstream ss(line);
		data.push_back(vector<D>());
		vector<D>& row = data.back();

		while(!ss.eof()) {
			D val;
			I chpos = ss.tellg();
			ss >> val;
			if(ss.fail())
				throw UsageException("Loading \"%s\", invalid conversion after char %lu:%u!",
				                     filename, data.size(), chpos);
			row.push_back(val);
		}
		if(data.size() >= 2 && row.size() != data.front().size())
			throw UsageException("Loading \"%s\", incoherent row length %lu != %lu at %lu-th row!",
			                     filename, row.size(), data.front().size(), data.size() );
	}

	if(data.empty())
		throw UsageException("Empty plain text matrix \"%s\"?", filename);

	mat.init(data.size(), data.front().size());
	
	I r = 0;
	for(vector<D>& row : data) {
		I off = r;
		for(D val: row) {
			mat.data[off] = val;
			off += mat.height;
		}
		++r;
	}
}

template<typename I, typename D>
void save_binary_matrix(const char* filename, MatrixCoo<I, D>& mat) {
	ofstream m(filename);
	ofstream mr(filename+string(".rows"));
	ofstream mc(filename+string(".cols"));
	ofstream mv(filename+string(".vals"));

	m << mat.height << " " << mat.width << " " << mat.nnz << " " << (sizeof(D)==8?"d":"f");
	mr.write((char*)mat.row_id, sizeof(I)*mat.nnz);
	mc.write((char*)mat.col_id, sizeof(I)*mat.nnz);
	mv.write((char*)mat.data, sizeof(D)*mat.nnz);
}

template<typename I, typename D>
void load_binary_matrix(const char* filename, MatrixCoo<I, D>& mat) {
	ifstream m(filename);
	ifstream mr(filename+string(".rows"));
	ifstream mc(filename+string(".cols"));
	ifstream mv(filename+string(".vals"));

	string line, tag;
	if(!getline(m, line))
		throw ParseException("Empty file?");
	stringstream ss(line);
	ss >> mat.height >> mat.width >> mat.nnz >> tag;
	if(tag != Scalar<D>::Tag)
		throw ParseException("Wrong precision loading matrix, got '%s', expected '%s'",
		                     tag.c_str(), Scalar<D>::Tag);

	mat.row_id = new I[mat.nnz];
	mat.col_id = new I[mat.nnz];
	mat.data = new D[mat.nnz];
	
	mr.read((char*)mat.row_id, sizeof(I)*mat.nnz);
	mc.read((char*)mat.col_id, sizeof(I)*mat.nnz);
	mv.read((char*)mat.data, sizeof(D)*mat.nnz);
}

template<typename I, typename D>
void map_binary_matrix(const char* filename, MatrixCoo<I, D>& mat) {
	ifstream m(filename);
	ifstream mr(filename+string(".rows"));
	ifstream mc(filename+string(".cols"));
	ifstream mv(filename+string(".vals"));

	string line, tag;
	if(!getline(m, line))
		throw ParseException("Empty file?");
	stringstream ss(line);
	ss >> mat.height >> mat.width >> mat.nnz >> tag;
	if(tag != Scalar<D>::Tag)
		throw ParseException("Wrong precision loading matrix, got '%s', expected '%s'",
		                     tag.c_str(), Scalar<D>::Tag);

	uint64_t rsize = 0, csize = 0, dsize = 0;
	mat.row_id = (I*)MMap::map_file((filename+string(".rows")).c_str(), &rsize);
	mat.col_id = (I*)MMap::map_file((filename+string(".cols")).c_str(), &csize);
	mat.data = (D*)MMap::map_file((filename+string(".vals")).c_str(), &dsize);
	//cout << filename+string(".vals") << endl;
	//cout << rsize << " " << csize << " " << dsize << " " << mat.nnz << " " << sizeof(D) << endl;
	if(!mat.row_id
	   || !mat.col_id
	   || !mat.data
	   || rsize!=mat.nnz*sizeof(I)
	   || csize!=mat.nnz*sizeof(I)
	   || dsize!=mat.nnz*sizeof(D)) {
		if(mat.row_id) MMap::unmap_file(mat.row_id, rsize);
		if(mat.col_id) MMap::unmap_file(mat.row_id, csize);
		if(mat.data) MMap::unmap_file(mat.data, dsize);
		const char* error = !mat.row_id ? "Couldn't load rows" :
			!mat.col_id ? "Couldn't load cols" :
			!mat.data ? "Couldn't load values" :
			rsize!=mat.nnz*sizeof(I) ? "Wrong rows file size" :
			csize!=mat.nnz*sizeof(I) ? "Wrong cols file size" :
			"Wrong values file size";
		mat.row_id = NULL;
		mat.col_id = NULL;
		mat.data = NULL;
		throw BasicException(string(error));
	}
	mat.is_mmapped = true;
}

template<typename I, typename D>
void unmap_binary_matrix(MatrixCoo<I, D>& mat) {
	MMap::unmap_file(mat.row_id, mat.nnz*sizeof(I));
	MMap::unmap_file(mat.col_id, mat.nnz*sizeof(I));
	MMap::unmap_file(mat.data, mat.nnz*sizeof(D));
	mat.row_id = NULL;
	mat.col_id = NULL;
	mat.data = NULL;
}

template<typename I, typename D>
void MatrixCoo<I, D>::free() {
	if(is_mmapped)
		unmap_binary_matrix(*this);
	if(row_id) delete[] row_id;
	if(col_id) delete[] col_id;
	if(data) delete[] data;
	is_mmapped = false;
	row_id = NULL;
	col_id = NULL;
	data = NULL;
}

template<typename I, typename D>
void MatrixDense<I, D>::set_random() {
	for(I i = 0; i < this->width * this->height; ++i)
		this->data[i] = (D)rand() / RAND_MAX;	
}

template<typename I>
void load_matrix_market_header(ifstream& is, Matrix<I>& mat) {
	string line;	
	if(!getline(is, line))
		throw ParseException("Empty file?");

	stringstream ss(line);
	string magic, what, format, field, symmetry;
	ss.str(line);
	ss >> magic >> what >> format >> field >> symmetry;

	if(magic != "%%MatrixMarket" || what != "matrix")
		throw ParseException("Invalid magic: \"%s\"", line.c_str());
	if(format != "coordinate") //other is: array
		throw ParseException("Unsupported format: \"%s\" (supported: coordinate)", format.c_str());
	if(field != "real") //other are: double, complex, integer, pattern
		throw ParseException("Unsupported field: \"%s\" (supported: real)", field.c_str());
	if(symmetry != "general") //other are: symmetrix, skew-symmetrix, hermitian
		throw ParseException("Unsupported symmetry: \"%s\" (supported: general)", symmetry.c_str());

	do {
		getline(is, line);
	}
	while(line.empty() || line[0] == '%');

	stringstream ss2(line);
	ss2 >> mat.height >> mat.width >> mat.nnz;
}

template<typename I, typename D>
void load_matrix_market_sliced(const char* file_format, MatrixCoo<I, D>& dst) {
	int nslices = 0;
	uint32_t w = 0, h = 0, nnz = 0;
	
	while(true) {
		char filename[1024];
		snprintf(filename, 1024, file_format, nslices);
		if(!fs::exists(filename))
			break;
		std::cout << filename << std::endl;

		Matrix<uint32_t> mat;
		ifstream is(filename);
		load_matrix_market_header<uint32_t>(is, mat);
		if(w != 0 && w != mat.width) {
			std::cout << "Incoherent width! was " << w << " not "
			          << mat.width << " at slice " << nslices << std::endl;
			return;
		}
		w = mat.width;
		h += mat.height;
		nnz += mat.nnz;
		nslices++;
	}

	std::cout << "w = " << w << "; h = " << h << "; nnz = " << nnz << std::endl;
	dst.width = w;
	dst.height = h;
	dst.nnz = nnz;
	dst.row_id = new I[nnz];
	dst.col_id = new I[nnz];
	dst.data = new D[nnz];

	int prevrows = 0;
	int num_entries = 0;
	for(int s = 0; s<nslices; ++s) {
		char filename[1024];
		snprintf(filename, 1024, file_format, s);
		std::cout << filename << "/" << nslices << std::endl;

		Matrix<uint32_t> mat;
		ifstream is(filename);
		load_matrix_market_header<uint32_t>(is, mat);
		I loc_idx = 0;

		string line;
		while(getline(is, line)) {
			if(line.empty() || line[0] == '%')
				continue;

			if(loc_idx >= mat.nnz)
				throw ParseException("Too many lines? (nnz = %d)", mat.nnz);
			I rid, cid;
			D val;
			stringstream ss(line);
			ss >> rid >> cid >> val;
			
			if(rid <= 0 || rid > mat.height)
				throw ParseException("at entry %d, row %d out of range (1,%d)",
				                     loc_idx, rid, mat.height );
			if(cid <= 0 || cid > mat.width)
				throw ParseException("at entry %d, col %d out of range (1,%d)",
				                     loc_idx, cid, mat.width );
			dst.row_id[num_entries] = rid - 1 + prevrows;
			dst.col_id[num_entries] = cid - 1;
			dst.data[num_entries] = val;
			loc_idx++;
			num_entries++;
		}
		if(loc_idx < mat.nnz)
			throw ParseException("Too few lines? (ne = %d, nnz = %d)", loc_idx, mat.nnz);
		prevrows += mat.height;
	}
}

template<typename I, typename D>
void load_matrix_market(const char* filename, MatrixCoo<I, D>& mat) {
	ifstream is(filename);
	string line;
	if(!getline(is, line))
		throw ParseException("Empty file?");

	stringstream ss(line);
	string magic, what, format, field, symmetry;
	ss.str(line);
	ss >> magic >> what >> format >> field >> symmetry;

	if(magic != "%%MatrixMarket" || what != "matrix")
		throw ParseException("Invalid magic: \"%s\"", line.c_str());
	if(format != "coordinate") //other is: array
		throw ParseException("Unsupported format: \"%s\" (supported: coordinate)", format.c_str());
	if(field != "real") //other are: double, complex, integer, pattern
		throw ParseException("Unsupported field: \"%s\" (supported: real)", field.c_str());
	if(symmetry != "general") //other are: symmetrix, skew-symmetrix, hermitian
		throw ParseException("Unsupported symmetry: \"%s\" (supported: general)", symmetry.c_str());

	bool got_size = false;
	I num_entries = 0;
	while(getline(is, line)) {
		if(line.empty() || line[0] == '%')
			continue;

		if(got_size && num_entries >= mat.nnz)
			throw ParseException("Too many lines? (nnz = %d)", mat.nnz);

		stringstream ss(line);

		if(!got_size) {
			mat.free();
			ss >> mat.height >> mat.width >> mat.nnz;
			mat.row_id = new I[mat.nnz];
			mat.col_id = new I[mat.nnz];
			mat.data = new D[mat.nnz];
			got_size = true;
			continue;
		}
		ss >> mat.row_id[num_entries] >> mat.col_id[num_entries] >> mat.data[num_entries];
		if(mat.row_id[num_entries] <= 0 || mat.row_id[num_entries] > mat.height)
			throw ParseException("at entry %d, row %d out of range (1,%d)",
			                     num_entries, mat.row_id[num_entries], mat.height );
		if(mat.col_id[num_entries] <= 0 || mat.col_id[num_entries] > mat.width)
			throw ParseException("at entry %d, col %d out of range (1,%d)",
			                     num_entries, mat.col_id[num_entries], mat.width );
		mat.row_id[num_entries]--;
		mat.col_id[num_entries]--;
		num_entries++;
	}
	if(num_entries < mat.nnz)
		throw ParseException("Too few lines? (ne = %d, nnz = %d)", num_entries, mat.nnz);
}

/* explicit template instantiation */
template void MatrixCoo<uint32_t, float>::free();
template void MatrixCoo<uint32_t, double>::free();
template void MatrixCoo<uint32_t, long double>::free();
template void MatrixDense<uint32_t, float>::set_random();
template void MatrixDense<uint32_t, double>::set_random();
template void MatrixDense<uint32_t, long double>::set_random();
template void save_rw_plain_text<uint32_t, float>(const char* filename,
                                                  MatrixDense<uint32_t, float>& mat);
template void save_rw_plain_text<uint32_t, double>(const char* filename,
                                                   MatrixDense<uint32_t, double>& mat);
template void save_rw_plain_text<uint32_t, long double>(const char* filename,
                                                        MatrixDense<uint32_t, long double>& mat);
template void save_cw_plain_text<uint32_t, float>(const char* filename,
                                                  MatrixDense<uint32_t, float>& mat);
template void save_cw_plain_text<uint32_t, double>(const char* filename,
                                                   MatrixDense<uint32_t, double>& mat);
template void save_cw_plain_text<uint32_t, long double>(const char* filename,
                                                        MatrixDense<uint32_t, long double>& mat);
template void load_cw_plain_text<uint32_t, float>(const char* filename,
                                                  MatrixDense<uint32_t, float>& mat);
template void load_cw_plain_text<uint32_t, double>(const char* filename,
                                                   MatrixDense<uint32_t, double>& mat);
template void load_cw_plain_text<uint32_t, long double>(const char* filename,
                                                        MatrixDense<uint32_t, long double>& mat);
template void load_matrix_market<uint32_t, float>(const char* filename,
                                                  MatrixCoo<uint32_t, float>& mat);
template void load_matrix_market<uint32_t, double>(const char* filename,
                                                   MatrixCoo<uint32_t, double>& mat);
template void load_matrix_market<uint32_t, long double>(const char* filename,
                                                        MatrixCoo<uint32_t, long double>& mat);

template
void load_matrix_market_sliced<uint32_t, float>(const char* filename,
                                                MatrixCoo<uint32_t, float>& mat);
template
void load_matrix_market_sliced<uint32_t, double>(const char* filename,
                                                 MatrixCoo<uint32_t, double>& mat);
template
void load_matrix_market_sliced<uint32_t, long double>(const char* filename,
                                                 MatrixCoo<uint32_t, long double>& mat);

template void save_binary_matrix<uint32_t, float>(const char* filename,
                                                  MatrixCoo<uint32_t, float>& mat);
template void save_binary_matrix<uint32_t, double>(const char* filename,
                                          MatrixCoo<uint32_t, double>& mat);
template void save_binary_matrix<uint32_t, long double>(const char* filename,
                                          MatrixCoo<uint32_t, long double>& mat);
template void load_binary_matrix<uint32_t, float>(const char* filename,
                                                  MatrixCoo<uint32_t, float>& mat);
template void load_binary_matrix<uint32_t, double>(const char* filename,
                                                   MatrixCoo<uint32_t, double>& mat);
template void load_binary_matrix<uint32_t, long double>(const char* filename,
                                                   MatrixCoo<uint32_t, long double>& mat);
template void map_binary_matrix<uint32_t, float>(const char* filename,
                                         MatrixCoo<uint32_t, float>& mat);
template void map_binary_matrix<uint32_t, double>(const char* filename,
                                         MatrixCoo<uint32_t, double>& mat);
template void map_binary_matrix<uint32_t, long double>(const char* filename,
                                         MatrixCoo<uint32_t, long double>& mat);
template void unmap_binary_matrix<uint32_t, float>(MatrixCoo<uint32_t, float>& mat);
template void unmap_binary_matrix<uint32_t, double>(MatrixCoo<uint32_t, double>& mat);
template void unmap_binary_matrix<uint32_t, long double>(MatrixCoo<uint32_t, long double>& mat);
