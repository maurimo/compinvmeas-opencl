
#define __CL_ENABLE_EXCEPTIONS
#include "exceptions.h"
#include "cl_utils.h"
#include "scalar.h"
#include "cl_matrix_dense.h"

template<typename I, typename D>
ClMatrixDense<I,D>::ClMatrixDense(I height, I width,
                                  cl::Context& context) {
	this->height = height;
	this->width = width;
	this->nnz = height * width;
	data = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(D)*this->nnz);
}

template<typename I, typename D>
ClMatrixDense<I,D>::ClMatrixDense(const MatrixDense<I,D>& mat,
                                       cl::Context& context,
                                       cl::CommandQueue& queue)
	: ClMatrixDense(mat.height, mat.width, context) {
	write(mat, queue);
}

template<typename I, typename D>
void ClMatrixDense<I,D>::write(const MatrixDense<I, D>& mat,
                               cl::CommandQueue& queue) {
	if(mat.height != this->height || mat.width != this->width)
		throw UsageException("Matrix write size mismatch (%dx%d, sould be %dx%d)",
		                     mat.height, mat.width, this->height, this->width);
	queue.enqueueWriteBuffer(data, CL_TRUE, 0, sizeof(D)*this->nnz, mat.data);
}

template<typename I, typename D>
void ClMatrixDense<I,D>::read(const MatrixDense<I, D>& mat,
                              cl::CommandQueue& queue) {
	if(mat.height != this->height || mat.width != this->width)
		throw UsageException("Matrix read size mismatch (%dx%d, sould be %dx%d)",
		                     mat.height, mat.width, this->height, this->width);		
	queue.enqueueReadBuffer(data, CL_TRUE, 0, sizeof(D)*this->nnz, mat.data);
}

/*
template<typename I, typename D>
ClMultiplierDenseCw<I,D>::ClMultiplierDenseCw(cl::Context& context,
                                              cl::Device& device) {
	mult_prog = create_program(context, device,
	                           {"cl/dense_mult.cl"},
	                           Scalar<D>::Flags);
	                           //"-cl-fast-relaxed-math");
	mult = cl::Kernel(mult_prog, "dense_mult_cw");
}

template<typename I, typename D>
void ClMultiplierDenseCw<I,D>::bind_mat(const ClMatrixDense<I, D>& mat) {
	mw = mat.width;
	mh = mat.height;
	mult.setArg(0, mat.data);
	mult.setArg(1, mat.width);
	mult.setArg(2, mat.height);
}

template<typename I, typename D>
void ClMultiplierDenseCw<I,D>::bind_in(const ClMatrixDense<I, D>& in) {
	iw = in.width;
	ih = in.height;
	mult.setArg(3, in.data);
	mult.setArg(4, in.width);
}

template<typename I, typename D>
void ClMultiplierDenseCw<I,D>::bind_out(const ClMatrixDense<I, D>& out) {
	ow = out.width;
	oh = out.height;
	mult.setArg(5, out.data);
}

template<typename I, typename D>
void ClMultiplierDenseCw<I,D>::run(cl::CommandQueue& queue) {
	if(ow != iw || oh != mh || mw != ih)
		throw UsageException("Multiply M_%dx%d <- M_%dx%d * M_%dx%d",
		                     oh, ow, mh, mw, ih, iw);
	uint32_t gsize = iw, lsize;
	compute_best_local_decomposition(gsize, lsize);
	queue.enqueueNDRangeKernel(mult,
	                           cl::NullRange,
	                           cl::NDRange(mh, gsize),
	                           cl::NDRange(1, lsize) );
}
*/

/*
template<typename I, typename D>
ClMultiplierDenseCw<I,D>::ClMultiplierDenseCw(cl::Context& context,
                                              cl::Device& device) {
	mult_prog = create_program(context, device,
	                           {"cl/dense_mult_exp.cl"},
	                           Scalar<D>::Flags);
	                           //"-cl-fast-relaxed-math");
	mult = cl::Kernel(mult_prog, "myGEMM8");
}

template<typename I, typename D>
void ClMultiplierDenseCw<I,D>::bind_mat(const ClMatrixDense<I, D>& mat) {
	mw = mat.width;
	mh = mat.height;
	mult.setArg(0, mat.height);
	mult.setArg(2, mat.width);
	mult.setArg(3, mat.data);
}

template<typename I, typename D>
void ClMultiplierDenseCw<I,D>::bind_in(const ClMatrixDense<I, D>& in) {
	ih = in.width;
	iw = in.height;
	mult.setArg(1, in.height);
	mult.setArg(4, in.data);
}

template<typename I, typename D>
void ClMultiplierDenseCw<I,D>::bind_out(const ClMatrixDense<I, D>& out) {
	ow = out.width;
	oh = out.height;
	mult.setArg(5, out.data);
}

#include "../cl/exp_settings.h"

template<typename I, typename D>
void ClMultiplierDenseCw<I,D>::run(cl::CommandQueue& queue) {
	if(ow != iw || oh != mh || mw != ih)
		throw UsageException("Multiply M_%dx%d <- M_%dx%d * M_%dx%d",
		                     oh, ow, mh, mw, ih, iw);
	//uint32_t gsize = iw, lsize;
	//compute_best_local_decomposition(gsize, lsize);
	cl::NDRange local(RTSM, RTSN);
	cl::NDRange global(oh/WPTM, ow/WPTN);
	queue.enqueueNDRangeKernel(mult,
	                           cl::NullRange,
	                           global,
	                           local );
}

template<typename I, typename D>
ClMultiplierDenseRw<I,D>::ClMultiplierDenseRw(cl::Context& context,
                                              cl::Device& device) {
	mult_prog = create_program(context, device,
	                           {"cl/dense_mult.cl"},
	                           Scalar<D>::Flags);
	mult = cl::Kernel(mult_prog, "dense_mult_rw");
}

template<typename I, typename D>
void ClMultiplierDenseRw<I,D>::bind_mat(const ClMatrixDense<I, D>& mat) {
	mw = mat.width;
	mh = mat.height;
	mult.setArg(0, mat.data);
	mult.setArg(1, mat.height);
	mult.setArg(2, mat.width);
}

template<typename I, typename D>
void ClMultiplierDenseRw<I,D>::bind_in(const ClMatrixDense<I, D>& in) {
	iw = in.width;
	ih = in.height;
	mult.setArg(3, in.data);
	mult.setArg(4, in.width);
}

template<typename I, typename D>
void ClMultiplierDenseRw<I,D>::bind_out(const ClMatrixDense<I, D>& out) {
	ow = out.width;
	oh = out.height;
	mult.setArg(5, out.data);
}

template<typename I, typename D>
void ClMultiplierDenseRw<I,D>::run(cl::CommandQueue& queue) {
	if(ow != iw || oh != mh || mw != ih)
		throw UsageException("Multiply M_%dx%d <- M_%dx%d * M_%dx%d",
		                     oh, ow, mh, mw, ih, iw);
	uint32_t gsize = iw, lsize;
	compute_best_local_decomposition(gsize, lsize);
	queue.enqueueNDRangeKernel(mult,
	                           cl::NullRange,
	                           cl::NDRange(mh, gsize),
	                           cl::NDRange(1, lsize) );
}
*/

/* explicit template instantiation */
template class ClMatrixDense<uint32_t, float>;
template class ClMatrixDense<uint32_t, double>;
/*template class ClMultiplierDenseCw<uint32_t, float>;
template class ClMultiplierDenseCw<uint32_t, double>;
template class ClMultiplierDenseRw<uint32_t, float>;
template class ClMultiplierDenseRw<uint32_t, double>;*/
