#ifndef __CL_MATRIX_DENSE_H__
#define __CL_MATRIX_DENSE_H__

#include <CL/cl.hpp>
#include "matrix.h"

/* a cl buffer containing a dense matrix (can be considered both as being either column-wise, either
   row-wise) */
template<typename I, typename D>
class ClMatrixDense : public Matrix<I> {
public:
	cl::Buffer data;

	ClMatrixDense(I height, I width,
	              cl::Context& context);

	ClMatrixDense(const MatrixDense<I, D>& mat,
	              cl::Context& context,
	              cl::CommandQueue& queue);

	void write(const MatrixDense<I, D>& mat,
	           cl::CommandQueue& queue);

	void read(const MatrixDense<I, D>& mat,
	          cl::CommandQueue& queue);
};

/* multiplier to apply a csr to column-wise dense matrices */
/*
template<typename I, typename D>
class ClMultiplierDenseCw {
public:
	cl::Program mult_prog;
	cl::Kernel mult;
	I mw, mh, iw, ih, ow, oh;

	ClMultiplierDenseCw(cl::Context& context,
	                  cl::Device& device);
	void bind_mat(const ClMatrixDense<I, D>&);
	void bind_in(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void run(cl::CommandQueue& queue);
};
*/

/* multiplier to apply a csr to row-wise dense matrices */
/*
template<typename I, typename D>
class ClMultiplierDenseRw {
public:
	cl::Program mult_prog;
	cl::Kernel mult;
	I mw, mh, iw, ih, ow, oh;

	ClMultiplierDenseRw(cl::Context& context,
	                      cl::Device& device);
	void bind_mat(const ClMatrixDense<I, D>&);
	void bind_in(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void run(cl::CommandQueue& queue);
};
*/

#endif //__CL_MATRIX_DENSE_H__

// Local Variables:
// mode: c++
// End:
