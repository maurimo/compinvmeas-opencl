#ifndef __UTILS_H__
#define __UTILS_H__

#include <vector>
#include <string>
#include <ostream>

/* reads a whole file */
std::string get_file_contents(const char *filename);

/* formats a number of seconds into a string */
std::string seconds_string(int64_t seconds);

/* initialize the logging */
void init_log(const char* prog);

/* delays a number of milliseconds */
void delay(uint32_t msec);

/* print message about (video) memory current used */
void inc_mem_usage(uint64_t bytes);

class Timer {
	class TimerImpl* priv;
public:
	Timer();
	~Timer();
	void reset();
	int64_t elapsed_microsec();
};

#endif //__UTILS_H__

// Local Variables:
// mode: c++
// End:
