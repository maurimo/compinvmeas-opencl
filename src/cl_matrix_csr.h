#ifndef __CL_MATRIX_CSR_H__
#define __CL_MATRIX_CSR_H__

#include <CL/cl.hpp>
#include "cl_matrix_dense.h"

template<typename I, typename D>
class ClMatrixCsr : public Matrix<I> {
public:
    cl::Buffer row_ptr;
    cl::Buffer col_id;
    cl::Buffer data;

	ClMatrixCsr(const MatrixCsr<I, D>& mat,
	            cl::Context& ctx,
	            cl::CommandQueue& queue);
};

/* multiplier to apply a csr to column-wise dense matrices */
template<typename I, typename D>
class ClMultiplierCsrCw {
public:
	cl::Program mult_prog;
	cl::Kernel mult;
	I mw, mh, iw, ih, ow, oh;

	ClMultiplierCsrCw(cl::Context& context,
	                  cl::Device& device);
	void bind_mat(const ClMatrixCsr<I, D>&);
	void bind_in(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void run(cl::CommandQueue& queue);
};

/* multiplier to apply a csr to row-wise dense matrices */
template<typename I, typename D>
class ClMultiplierCsrRw {
public:
	cl::Program mult_prog;
	cl::Kernel mult;
	I mw, mh, iw, ih, ow, oh;

	ClMultiplierCsrRw(cl::Context& context,
	                      cl::Device& device);
	void bind_mat(const ClMatrixCsr<I, D>&);
	void bind_in(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void run(cl::CommandQueue& queue);
};

#endif //__CL_MATRIX_CSR_H__

// Local Variables:
// mode: c++
// End:
