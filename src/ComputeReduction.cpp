
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <system_error>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <inttypes.h>
#include <boost/numeric/interval/hw_rounding.hpp>
#include <boost/filesystem.hpp>
#include <glog/logging.h>
#define __CL_ENABLE_EXCEPTIONS
#include <limits>
#include "exceptions.h"
#include "utils.h"
#include "mmap.h"

typedef double D;

using namespace std;

int main(int argc, char* argv[]) {
	uint32_t size = atoi(argv[1]);
	char* measure_file = argv[2];
	uint32_t out_size = atoi(argv[3]);
	char* out_data_file = argv[4];

	cout.precision(numeric_limits<D>::max_digits10);
	
	uint64_t size1;
	D* vec_meas = (D*)MMap::map_file(measure_file, &size1);

	if(size1 != size*sizeof(D)) {
		cout << "Wrong file sizes!!" << endl;
		return 1;
	}

	vector<D> out_vec(out_size);
	for(uint32_t i = 0; i<out_size; i++) {
		uint32_t from = (((uint64_t)i)*size)/out_size;
		uint32_t to = (((uint64_t)(i+1))*size)/out_size;
		D val = 0;
		for(uint32_t j = from; j<to; j++)
			val += vec_meas[j];
		out_vec[i] = val / (to-from);
	}
	
	ofstream out(out_data_file);
	out.write((char*)out_vec.data(), sizeof(D)*out_size);
	
	return 0;
}
