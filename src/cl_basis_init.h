#ifndef __CL_BASIS_INIT_H__
#define __CL_BASIS_INIT_H__

#include <CL/cl.hpp>
#include "cl_matrix_dense.h"

/* initialize columns in a cw matrix to some basis */
template<typename I, typename D>
class ClBasisInitCw {
public:
	uint32_t mh, mw;
	cl::Program basis_prog;
	cl::Kernel k_init_0_Np1;
	cl::Kernel k_init_N_Np1;

	ClBasisInitCw(cl::Context& context,
	              cl::Device& device);
	void bind_mat(const ClMatrixDense<I, D>&);
	uint32_t init_0_Np1(uint32_t start, cl::CommandQueue& queue);
	uint32_t init_N_Np1(uint32_t start, cl::CommandQueue& queue);
};

/* initialize columns in a rw matrix to some basis */
template<typename I, typename D>
class ClBasisInitRw {
public:
	uint32_t mh, mw;
	cl::Program basis_prog;
	cl::Kernel k_init_0_Np1;
	cl::Kernel k_init_N_Np1;
	cl::Kernel k_init_noise_gap;

	ClBasisInitRw(cl::Context& context,
	              cl::Device& device,
	              bool mod1);
	void bind_mat(const ClMatrixDense<I, D>&);
	uint32_t init_0_Np1(uint32_t start, cl::CommandQueue& queue);
	uint32_t init_N_Np1(uint32_t start, cl::CommandQueue& queue);
	uint32_t init_noise_gap(uint32_t start, uint32_t gap, cl::CommandQueue& queue);
};

#endif //__CL_BASIS_INIT_H__

// Local Variables:
// mode: c++
// End:
