
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <system_error>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <inttypes.h>
#include <boost/numeric/interval/hw_rounding.hpp>
#include <boost/filesystem.hpp>
#include <glog/logging.h>
#define __CL_ENABLE_EXCEPTIONS
#include <limits>
#include "exceptions.h"
#include "utils.h"
#include "mmap.h"

typedef double D;

using namespace std;

int main(int argc, char* argv[]) {
	uint32_t size = atoi(argv[1]);
	char* measure_file = argv[2];
	char* out_data_file = argv[3];

	cout.precision(numeric_limits<D>::max_digits10);
	
	uint64_t size1;
	D* vec_meas = (D*)MMap::map_file(measure_file, &size1);

	if(size1 != size*sizeof(D)) {
		cout << "Wrong file sizes!!" << endl;
		return 1;
	}

	D lower = 0, upper = 0;
	boost::numeric::interval_lib::rounded_math<D> rnd;
	for(uint32_t i = 0; i<size; i++) {
		lower = rnd.add_down(lower, abs(vec_meas[i]));
		upper = rnd.add_up(upper, abs(vec_meas[i]));
	}
	lower = rnd.div_down(lower, size);
	upper = rnd.div_up(upper, size);

	cout << "L1 in [" << lower << "," << upper << "]" << endl;
	
	ofstream out(out_data_file);
	out.write((char*)&lower, sizeof(D));
	out.write((char*)&upper, sizeof(D));
	
	return 0;
}
