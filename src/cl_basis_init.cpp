
#define __CL_ENABLE_EXCEPTIONS
#include "exceptions.h"
#include "cl_utils.h"
#include "scalar.h"
#include "cl_basis_init.h"

template<typename I, typename D>
ClBasisInitCw<I, D>::ClBasisInitCw(cl::Context& context,
                                   cl::Device& device) {
	basis_prog = create_program(context, device,
	                           {"cl/basis_init.cl"},
	                           Scalar<D>::Flags);
	k_init_0_Np1 = cl::Kernel(basis_prog, "V_basis_0_Np1_cw");
	k_init_N_Np1 = cl::Kernel(basis_prog, "V_basis_N_Np1_cw");
}

template<typename I, typename D>
void ClBasisInitCw<I, D>::bind_mat(const ClMatrixDense<I, D>& mat) {
	mh = mat.height;
	mw = mat.width;
	k_init_0_Np1.setArg(0, mh);
	k_init_0_Np1.setArg(2, mat.data);
	k_init_N_Np1.setArg(0, mh);
	k_init_N_Np1.setArg(2, mat.data);
}

template<typename I, typename D>
uint32_t ClBasisInitCw<I, D>::init_0_Np1(uint32_t start, cl::CommandQueue& queue) {
	k_init_0_Np1.setArg(1, start);
	uint32_t ncols = std::min((mh-1)-start, mw);
	queue.enqueueNDRangeKernel(k_init_0_Np1,
	                           cl::NullRange,
	                           cl::NDRange(mh, ncols),
	                           cl::NullRange //cl::NDRange(1, 1)
	                           );
	return ncols;
}

template<typename I, typename D>
uint32_t ClBasisInitCw<I, D>::init_N_Np1(uint32_t start, cl::CommandQueue& queue) {
	k_init_N_Np1.setArg(1, start);
	uint32_t ncols = std::min((mh-1)-start, mw);
	queue.enqueueNDRangeKernel(k_init_N_Np1,
	                           cl::NullRange,
	                           cl::NDRange(mh, ncols),
	                           cl::NullRange //cl::NDRange(1, 1)
	                           );
	return ncols;
}

template<typename I, typename D>
ClBasisInitRw<I, D>::ClBasisInitRw(cl::Context& context,
                                   cl::Device& device,
                                   bool mod1) {
	basis_prog = create_program(context, device,
	                           {"cl/basis_init.cl"},
	                           Scalar<D>::Flags);
	k_init_0_Np1 = cl::Kernel(basis_prog, "V_basis_0_Np1_rw");
	k_init_N_Np1 = cl::Kernel(basis_prog, "V_basis_N_Np1_rw");
	if(mod1)
		k_init_noise_gap = cl::Kernel(basis_prog, "V_basis_noise_gap_rw_mod_1");
	else
		k_init_noise_gap = cl::Kernel(basis_prog, "V_basis_noise_gap_rw");
}

template<typename I, typename D>
void ClBasisInitRw<I, D>::bind_mat(const ClMatrixDense<I, D>& mat) {
	mh = mat.height;
	mw = mat.width;
	k_init_0_Np1.setArg(0, mw);
	k_init_0_Np1.setArg(1, mh);
	k_init_0_Np1.setArg(3, mat.data);
	k_init_N_Np1.setArg(0, mw);
	k_init_N_Np1.setArg(2, mat.data);
	k_init_noise_gap.setArg(0, mw);
	k_init_noise_gap.setArg(1, mh);
	k_init_noise_gap.setArg(4, mat.data);
}

template<typename I, typename D>
uint32_t ClBasisInitRw<I, D>::init_0_Np1(uint32_t start, cl::CommandQueue& queue) {
	if(start >= mh-1)
		return 0;
	k_init_0_Np1.setArg(2, start);
	uint32_t ncols = std::min((mh-1)-start, mw);
	queue.enqueueNDRangeKernel(k_init_0_Np1,
	                           cl::NullRange,
	                           cl::NDRange(mh, ncols),
	                           // FIXME!!
	                           cl::NullRange //cl::NDRange(1, 128)
	                           );
	return ncols;
}

template<typename I, typename D>
uint32_t ClBasisInitRw<I, D>::init_N_Np1(uint32_t start, cl::CommandQueue& queue) {
	k_init_N_Np1.setArg(1, start);
	uint32_t ncols = std::min((mh-1)-start, mw);
	queue.enqueueNDRangeKernel(k_init_N_Np1,
	                           cl::NullRange,
	                           cl::NDRange(mh, ncols),
	                           // FIXME!!
	                           cl::NullRange //cl::NDRange(1, 128)
	                           );
	return ncols;
}

template<typename I, typename D>
uint32_t ClBasisInitRw<I, D>::init_noise_gap(uint32_t start,
                                             uint32_t gap,
                                             cl::CommandQueue& queue) {
	if(start >= mh)
		return 0;
	k_init_noise_gap.setArg(2, start);
	k_init_noise_gap.setArg(3, gap);
	uint32_t ncols = std::min(mh-start, mw);
	queue.enqueueNDRangeKernel(k_init_noise_gap,
	                           cl::NullRange,
	                           cl::NDRange(mh, ncols),
	                           // FIXME!!
	                           cl::NullRange //cl::NDRange(1, 128)
	                           );
	return ncols;
}

/* explicit template instantiation */
template class ClBasisInitCw<uint32_t, float>;
template class ClBasisInitCw<uint32_t, double>;
template class ClBasisInitRw<uint32_t, float>;
template class ClBasisInitRw<uint32_t, double>;
