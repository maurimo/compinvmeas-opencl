#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <iostream>
#include <algorithm>
//#include <boost/numeric/interval/hw_rounding.hpp>
#include <cmath>
#include <fenv.h>
//#pragma STDC FENV_ACCESS ON

/** base class for matrices */
template <class I>
class Matrix {
public:
	I height = 0;
	I width = 0;
	I nnz = 0;
};

/** one vector of size nnz: data(s)
    data is one column, then another, etc... */
template<typename I, typename D>
class MatrixDense : public Matrix<I> {
public:
	D* data = NULL;

	void free() {
		if(data) { delete[] data; data = NULL; }
	}

	MatrixDense() { }

	MatrixDense(I h, I w = 1) : MatrixDense() {
		init(h,w);
	}

	~MatrixDense() { free(); }

	void init(I h, I w = 1) {
		if(data && this->width == w && this->height == h)
			return;
		free();
		this->height = h;
 		this->width = w;
		this->nnz = w*h;
		if(this->nnz > 0)
			data = new D[this->nnz];
	}

	I basis_0_Np1_cw(I start) {
		set(0);
		if(start >= this->height)
			return 0;
		I ncols = std::min(this->height-1-start, this->width);
		for(I i = 0; i<ncols; i++) {
			data[i*this->height] = 1.0;
			data[i*this->height+start+i+1] = -1.0;
		}
		return ncols;
	}

	I basis_N_Np1_cw(I start) {
		set(0);
		if(start >= this->height)
			return 0;
		I ncols = std::min(this->height-1-start, this->width);
		for(I i = 0; i<ncols; i++) {
			data[i*this->height+start+i] = 1.0;
			data[i*this->height+start+i+1] = -1.0;
		}
		return ncols;
	}

	I basis_0_Np1_rw(I start) {
		set(0);
		if(start >= this->height)
			return 0;
		I ncols = std::min(this->height-1-start, this->width);
		for(I i = 0; i<ncols; i++) {
			data[i] = 1.0;
			data[i+(start+i+1)*this->width] = -1.0;
		}
		return ncols;
	}

	I basis_N_Np1_rw(I start) {
		set(0);
		if(start >= this->height)
			return 0;
		I ncols = std::min(this->height-1-start, this->width);
		for(I i = 0; i<ncols; i++) {
			data[i+(start+i)*this->width] = 1.0;
			data[i+(start+i+1)*this->width] = -1.0;
		}
		return ncols;
	}

	I basis_noise_gap_rw(I start, I gap) {
		set(0);
		I ncols = std::min(this->height-start, this->width);
		for(I i = 0; i<ncols; i++) {
			I bidx = start+i;
			I bidx1 = (bidx>=gap) ? bidx-gap : gap-1-bidx;
			I bidx2 = (bidx+gap) < this->height ? bidx+gap :
				2*this->height-bidx-gap-1;
			data[i+bidx1*this->width] = 0.5;
			data[i+bidx2*this->width] = -0.5;
		}
		return ncols;
	}

	I basis_noiseM1_gap_rw(I start, I gap) {
		set(0);
		I ncols = std::min(this->height-start, this->width);
		for(I i = 0; i<ncols; i++) {
			I bidx = start+i;
			I bidx1 = (bidx - gap - this->height) % this->height;
			I bidx2 = (bidx + gap) % this->height;
			
			data[i+bidx1*this->width] = 0.5;
			data[i+bidx2*this->width] = -0.5;
		}
		return ncols;
	}

	void set(D val) {
		for(I i = 0; i < this->width * this->height; ++i)
			this->data[i] = val;
	}

	void set_random();

	/* compute the L-infinity norm (as rw matrices) for a range of columns */
	void norm_Linf_rw(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == 1);	
		if(end_col == 0)
			end_col = this->width;
		for(I c = start_col, coff = start_col; c < end_col; ++c, ++coff) {
			D norm = 0;
			for(I r = 0, off = coff; r < this->height; ++r, off += this->width)
				norm = std::max(norm, std::abs(data[off]));
			that.data[c] = norm;
		}
	}

	/* compute the L-infinity norm (as cw matrices) for a range of columns */
	void norm_Linf_cw(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == 1);	
		if(end_col == 0)
			end_col = this->width;
		for(I c = start_col, coff = start_col*this->height;
		      c < end_col;
		      ++c, coff += this->height) {
			D norm = 0;
			for(I r = 0, off = coff; r < this->height; ++r, ++off)
				norm = std::max(norm, std::abs(data[off]));
			that.data[c] = norm;
		}
	}
	/* compute the L1 norm (as rw matrices) for a range of columns */
	void norm_L1_rw(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == 1);	
		if(end_col == 0)
			end_col = this->width;
		for(I c = start_col, coff = start_col; c < end_col; ++c, ++coff) {
			D norm = 0;
			for(I r = 0, off = coff; r < this->height; ++r, off += this->width)
				norm += std::abs(data[off]);
			that.data[c] = norm;
		}
	}

	/* compute the L1 norm (as cw matrices) for a range of columns */
	void norm_L1_cw(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == 1);	
		if(end_col == 0)
			end_col = this->width;
		for(I c = start_col, coff = start_col*this->height;
		      c < end_col;
		      ++c, coff += this->height) {
			D norm = 0;
			for(I r = 0, off = coff; r < this->height; ++r, ++off)
				norm += std::abs(data[off]);
			that.data[c] = norm;
		}
	}
	
	/* compute the L-infinity norm (as rw matrices) for a range of columns */
	void norm_L1_rw_down(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == 1);	
		if(end_col == 0)
			end_col = this->width;
		
		const int originalRounding = fegetround();
		fesetround(FE_DOWNWARD);
		for(I c = start_col, coff = start_col; c < end_col; ++c, ++coff) {
			D norm = 0;
			for(I r = 0, off = coff; r < this->height; ++r, off += this->width)
				norm += std::abs(data[off]);
			that.data[c] = norm;
		}
		fesetround(originalRounding);
	}

	/* compute the L-infinity norm (as cw matrices) for a range of columns */
	void norm_L1_cw_down(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == 1);	
		if(end_col == 0)
			end_col = this->width;
		
		const int originalRounding = fegetround();
		fesetround(FE_DOWNWARD);
		for(I c = start_col, coff = start_col*this->height;
		    c < end_col;
		    ++c, coff += this->height) {
			D norm = 0;
			for(I r = 0, off = coff; r < this->height; ++r, ++off)
				norm += std::abs(data[off]);
			that.data[c] = norm;
		}
		fesetround(originalRounding);
	}

	/* compute the L-infinity distance (as rw matrices) for a range of columns */
	D distance_Linf_rw(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == this->height);
		D retv = 0;
		if(end_col == 0)
			end_col = this->width;
		for(I c = start_col, coff = start_col; c < end_col; ++c, ++coff)
			for(I r = 0, off = coff; r < this->height; ++r, off += this->width)
				retv = std::max(retv, std::abs(data[off]-that.data[off]));
		return retv;
	}

	/* compute the L-infinity distance (as cw matrices) for a range of columns */
	D distance_Linf_cw(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == this->height);
		D retv = 0;
		if(end_col == 0)
			end_col = this->width;
		for(I c = start_col, coff = start_col*this->height; c < end_col; ++c, coff += this->height)
			for(I r = 0, off = coff; r < this->height; ++r, ++off)
				retv = std::max(retv, std::abs(data[off]-that.data[off]));
		return retv;
	}

	/* compute L1 distance */
	D distance_L1_cw_up(const MatrixDense& that, I start_col = 0, I end_col = 0) {
		//boost::numeric::interval_lib::rounded_math<D> rnd;
		assert(that.width == this->width);
		assert(that.height == this->height);
		D retv = 0;
		if(end_col == 0)
			end_col = this->width;
		const int originalRounding = fegetround();
		fesetround(FE_UPWARD);
		for(I c = start_col, coff = start_col*this->height;
		    c < end_col;
		    ++c, coff += this->height) {
			D sum = 0;
			for(I r = 0, off = coff; r < this->height; ++r, ++off) {
				sum += std::max(data[off]-that.data[off], that.data[off]-data[off]);
				//sum = rnd.add_up(sum, std::abs(data[off]-that.data[off]));
			}
			retv = std::max(retv, sum);
		}
		fesetround(originalRounding);
		return retv;
	}

	void transpose_cw(MatrixDense<I,D>& dst) {
		dst.free();
		dst.init(this->width, this->height);
		for(I c = 0; c < this->width; ++c)
		for(I r = 0; r < this->height; ++r)
			dst.data[r*dst.height + c] = data[c*this->height + r];
	}

	/* applies (as column-wise) to src and stores into dst,
	   assuming they are column-wise stored */
	void apply_cw(const MatrixDense<I,D>& src, MatrixDense<I,D>& dst) {
		assert(src.height == this->width);
		assert(dst.height == this->height);
		assert(src.width == dst.width);
		for(I q = 0; q < src.width; ++q) {
			D* in_vec = src.data + q*src.height;
			D* out_vec = dst.data + q*dst.height;
			for(I r = 0; r < dst.height; ++r)
				out_vec[r] = 0;
			for(I c = 0; c < this->width; ++c) {
				D v = in_vec[c];
				D* mat_vec = data + c*this->height;
				for(I r = 0; r < this->height; ++r)
					out_vec[r] += v * mat_vec[r];
			}
		}
	}

	/* applies (as column-wise) to src and stores into dst,
	   assuming they are row-wise stored */
	void apply_rw(const MatrixDense<I,D>& src,
	              MatrixDense<I,D>& dst) {
		assert(src.height == this->width);
		assert(dst.height == this->height);
		assert(src.width == dst.width);

		for(I i = 0; i < dst.width * dst.height; ++i)
			dst.data[i] = 0;
		for(I c = 0; c < this->width; ++c)
		for(I r = 0; r < this->height; ++r) {
			D* out_vec = dst.data + dst.width*r;
			D* in_vec = src.data + dst.width*c;
			D d = data[r + c*this->height];
			for(I q = 0; q < src.width; ++q)
				out_vec[q] += in_vec[q] * d;
		}
	}

	int noise_flops() {
		return 6 * this->height * this->width;
	}
	
	void apply_noise_rw_dummy(I noise_size,
	                    I shift,
	                    const MatrixDense& that,
	                    I start_col = 0,
	                    I end_col = 0) {
		int skipns = noise_size/2;
		for(int c = 0; c < this->width; c++)
			for(int r = 0; r < this->height; r++) {
				D sum = 0;
				for(int i = -skipns+1; i < skipns; i++) {
					int t = r + i;
					t = t < 0 ? -1 - t : (t >= this->height ?
					                     2*this->height - t - 1 : t);
					//sum += data[c + t * this->width];
					if(t >= shift)
						sum += data[c + (t-shift) * this->width];
					if(t+shift >= this->height)
						sum += data[c + (2*this->height -1 - (t+shift)) * this->width];
				}
				int t = r - skipns;
				t = t < 0 ? -1 - t : (t >= this->height ?
				                     2*this->height - t - 1 : t);
				if(t >= shift)
					sum += data[c + (t-shift) * this->width] / 2;
				if(t+shift >= this->height)
					sum += data[c + (2*this->height -1 - (t+shift)) * this->width] / 2;
				t = r + skipns;
				t = t < 0 ? -1 - t : (t >= this->height ?
				                     2*this->height - t - 1 : t);
				if(t >= shift)
					sum += data[c + (t-shift) * this->width] / 2;
				if(t+shift >= this->height)
					sum += data[c + (2*this->height -1 - (t+shift)) * this->width] / 2;
				that.data[c + r * this->width] = sum / noise_size;
			}
	}

	
	void apply_noiseM1_rw_dummy(I noise_size,
	                    I shift,
	                    const MatrixDense& that,
	                    I start_col = 0,
	                    I end_col = 0) {
		int skipns = noise_size/2;
		for(int c = 0; c < this->width; c++)
			for(int r = 0; r < this->height; r++) {
				D sum = 0;
				for(int i = -skipns+1; i < skipns; i++) {
					int t = (r + i + this->height) % this->height;
					if(t >= shift)
						sum += data[c + (t-shift) * this->width];
					if(t+shift >= this->height)
						sum += data[c + (2*this->height -1 - (t+shift)) * this->width];
				}
				int t = (r - skipns + this->height) % this->height;
				if(t >= shift)
					sum += data[c + (t-shift) * this->width] / 2;
				if(t+shift >= this->height)
					sum += data[c + (2*this->height -1 - (t+shift)) * this->width] / 2;
				t = (r + skipns + this->height) % this->height;
				if(t >= shift)
					sum += data[c + (t-shift) * this->width] / 2;
				if(t+shift >= this->height)
					sum += data[c + (2*this->height -1 - (t+shift)) * this->width] / 2;
				that.data[c + r * this->width] = sum / noise_size;
			}
	}
	
	void apply_noise_rw(I noise_size,
	                    I shift,
	                    const MatrixDense& that,
	                    const MatrixDense& tmp,
	                    I start_col = 0,
	                    I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == this->height);
		assert(tmp.width == this->width);
		assert(tmp.height == this->height * 2);
		I sec_size = noise_size - 2;
		// assume noise_size to be EVEN!!
		I rem = this->height % sec_size;
		I clamp = rem > 0 ? this->height - rem : 0;
		//assert(tmp.height % sec_size == 0); // we can lift this I guess

		if(end_col == 0)
			end_col = this->width;
		I left = sec_size / 2;
		I right = sec_size - left;
		D factor = D(1.0) / noise_size;
		D* tmp2data = tmp.data + this->height*this->width;
		for(I c = start_col, coff = start_col;
                c < end_col;
                ++c, coff += 1) {
			for(I r = 0; r < this->height; r += sec_size) {
				I len = std::min(this->height - r, sec_size);
				D accum = 0;
				for(I b = 0, v = r, off = coff + r*this->width;
				       b < len;
				       ++b, v += 1, off += this->width) {
					//accum += data[off];
					if(v >= shift)
						accum += data[coff + (v-shift) * this->width];
					if(v+shift >= this->height)
						accum += data[coff + (2*this->height -1 - (v+shift)) * this->width];
					tmp.data[off] = accum;
				}
				accum = 0;
				for(I b = 0, v = r+len-1, off = coff + (r + len - 1)*this->width;
				        b < len;
				        ++b, --v, off -= this->width) {
					//accum += data[off];
					if(v >= shift)
						accum += data[coff + (v-shift) * this->width];
					if(v+shift >= this->height)
						accum += data[coff + (2*this->height -1 - (v+shift)) * this->width];
					tmp2data[off] = accum;
				}
			}

			for(I r = 0; r < this->height; ++r) {
				D sum;
				if(r < left) // left reflection
					sum = tmp.data[coff + (r + right)*this->width]
						+ tmp.data[coff + (left -  r - 1)*this->width];
				else if(r + right >= this->height) { // right reflection
					I idx0 = r - left;
					I idx1 = 2*this->height - 1 - r - right;
					sum = tmp2data[coff + idx0*this->width]
						+ tmp2data[coff + idx1*this->width];
					if(idx1<clamp)
						sum += tmp2data[coff + clamp*this->width];
					if(idx0<clamp)
						sum += tmp2data[coff + clamp*this->width];
				}
				else
					sum = tmp.data[coff + (r + right)*this->width]
						+ tmp2data[coff + (r - left)*this->width];
				I lh = (r >= left+1) ? (r-left-1) : (-r+left);
				I rh = (r+right+1 < this->height) ? (r+right+1) :
					         (2*this->height - 2 - r - right);
				if(lh >= shift)
					sum += data[coff + (lh-shift) * this->width]/2;
				if(lh+shift >= this->height)
					sum += data[coff + (2*this->height -1 - (lh+shift)) * this->width]/2;
				if(rh >= shift)
					sum += data[coff + (rh-shift) * this->width]/2;
				if(rh+shift >= this->height)
					sum += data[coff + (2*this->height -1 - (rh+shift)) * this->width]/2;
				//sum += (data[coff + lh*this->width] + data[coff + rh*this->width]) / 2;
				that.data[coff + r*this->width] = sum * factor;
			}
		}
	}

	
	void apply_noiseM1_rw(I noise_size,
	                    I shift,
	                    const MatrixDense& that,
	                    const MatrixDense& tmp,
	                    I start_col = 0,
	                    I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == this->height);
		assert(tmp.width == this->width);
		assert(tmp.height == this->height * 2);
		I sec_size = noise_size - 2;
		// assume noise_size to be EVEN!!
		I rem = this->height % sec_size;
		I clamp = rem > 0 ? this->height - rem : 0;
		//assert(tmp.height % sec_size == 0); // we can lift this I guess

		if(end_col == 0)
			end_col = this->width;
		I left = sec_size / 2;
		I right = sec_size - left;
		D factor = D(1.0) / noise_size;
		D* tmp2data = tmp.data + this->height*this->width;
		for(I c = start_col, coff = start_col;
                c < end_col;
                ++c, coff += 1) {
			for(I r = 0; r < this->height; r += sec_size) {
				I len = std::min(this->height - r, sec_size);
				D accum = 0;
				for(I b = 0, v = r, off = coff + r*this->width;
				       b < len;
				       ++b, v += 1, off += this->width) {
					//accum += data[off];
					if(v >= shift)
						accum += data[coff + (v-shift) * this->width];
					if(v+shift >= this->height)
						accum += data[coff + (2*this->height -1 - (v+shift)) * this->width];
					tmp.data[off] = accum;
				}
				accum = 0;
				for(I b = 0, v = r+len-1, off = coff + (r + len - 1)*this->width;
				        b < len;
				        ++b, --v, off -= this->width) {
					//accum += data[off];
					if(v >= shift)
						accum += data[coff + (v-shift) * this->width];
					if(v+shift >= this->height)
						accum += data[coff + (2*this->height -1 - (v+shift)) * this->width];
					tmp2data[off] = accum;
				}
			}

			for(I r = 0; r < this->height; ++r) {
				D sum;
				if(r < left) { // left reflection
					I idx0 = this->height + r - left;
					sum = tmp2data[coff + idx0*this->width]
						+ tmp.data[coff + (r + right)*this->width];
					if(idx0<clamp)
						sum += tmp2data[coff + clamp*this->width];
				}
				else if(r + right >= this->height) { // right reflection
					I idx0 = r - left;
					I idx1 = r + right - this->height;
					sum = tmp2data[coff + idx0*this->width]
						+ tmp.data[coff + idx1*this->width];
					if(idx0<clamp)
						sum += tmp2data[coff + clamp*this->width];
				}
				else
					sum = tmp.data[coff + (r + right)*this->width]
						+ tmp2data[coff + (r - left)*this->width];
				I lh = (r-left-1+this->height) % this->height;
				I rh = (r+right+1) % this->height;
				if(lh >= shift)
					sum += data[coff + (lh-shift) * this->width]/2;
				if(lh+shift >= this->height)
					sum += data[coff + (2*this->height -1 - (lh+shift)) * this->width]/2;
				if(rh >= shift)
					sum += data[coff + (rh-shift) * this->width]/2;
				if(rh+shift >= this->height)
				sum += data[coff + (2*this->height -1 - (rh+shift)) * this->width]/2;
				//sum += (data[coff + lh*this->width] + data[coff + rh*this->width]) / 2;
				that.data[coff + r*this->width] = sum * factor;
			}
		}
	}
	
	/* compute the L-infinity norm (as cw matrices) for a range of columns */
	void apply_noise_cw(I noise_size,
	                    I shift,
	                    const MatrixDense& that,
	                    const MatrixDense& tmp,
	                    I start_col = 0,
	                    I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == this->height);
		assert(tmp.width == this->width * 2);
		assert(tmp.height == this->height);
		I sec_size = noise_size - 2; //noise_size - 1;
		// assume noise_size to be EVEN!!
		I rem = this->height % sec_size;
		I clamp = rem > 0 ? this->height - rem : 0;
		//assert(tmp.height % sec_size == 0); // we can lift this I guess

		if(end_col == 0)
			end_col = this->width;
		I left = sec_size / 2;
		I right = sec_size - left;
		D factor = D(1.0) / noise_size;
		D* tmp2data = tmp.data + this->height * this->width;
		for(I c = start_col, coff = start_col*this->height;
                c < end_col;
                ++c, coff += this->height) {
			for(I r = 0; r < this->height; r += sec_size) {
				I len = std::min(this->height - r, sec_size);
				D accum = 0;
				for(I b = 0, v = r, off = coff + r; b < len; ++b, ++v, ++off) {
					//accum += data[off];
					if(v >= shift)
						accum += data[coff + (v-shift)];
					if(v+shift >= this->height)
						accum += data[coff + (2*this->height -1 - (v+shift))];
					tmp.data[off] = accum;
				}
				accum = 0;
				for(I b = 0, v = r+len-1, off = coff + r + len - 1; b < len;
				       ++b, --v, --off) {
					//accum += data[off];
					if(v >= shift)
						accum += data[coff + (v-shift)];
					if(v+shift >= this->height)
						accum += data[coff + (2*this->height -1 - (v+shift))];
					tmp2data[off] = accum;
				}
			}

			for(I r = 0; r < this->height; ++r) {
				D sum;
				if(r < left) // left reflection
					sum = tmp.data[coff + r + right] + tmp.data[coff + left -  r - 1];
				else if(r + right >= this->height) { // right reflection
					I idx0 = r - left;
					I idx1 = 2*this->height - 1 - r - right;
					sum = tmp2data[coff + idx0] + tmp2data[coff + idx1];
					if(idx1<clamp)
						sum += tmp2data[coff + clamp];
					if(idx0<clamp)
						sum += tmp2data[coff + clamp];
				}
				else
					sum = tmp.data[coff + r + right] + tmp2data[coff + r - left];
				I lh = (r >= left+1) ? (r-left-1) : (-r+left);
				I rh = (r+right+1 < this->height) ? (r+right+1) :
					         (2*this->height - 2 - r - right);
				//sum += (data[coff + lh] + data[coff + rh]) / 2;
				if(lh >= shift)
					sum += data[coff + (lh-shift)]/2;
				if(lh+shift >= this->height)
					sum += data[coff + (2*this->height -1 - (lh+shift))]/2;
				if(rh >= shift)
					sum += data[coff + (rh-shift)]/2;
				if(rh+shift >= this->height)
					sum += data[coff + (2*this->height -1 - (rh+shift))]/2;
				that.data[coff + r] = sum * factor;
			}
		}
	}

	/* compute the L-infinity norm (as cw matrices) for a range of columns */
	void apply_noiseM1_cw(I noise_size,
	                    I shift,
	                    const MatrixDense& that,
	                    const MatrixDense& tmp,
	                    I start_col = 0,
	                    I end_col = 0) {
		assert(that.width == this->width);
		assert(that.height == this->height);
		assert(tmp.width == this->width * 2);
		assert(tmp.height == this->height);
		I sec_size = noise_size - 2; //noise_size - 1;
		// assume noise_size to be EVEN!!
		I rem = this->height % sec_size;
		I clamp = rem > 0 ? this->height - rem : 0;
		//assert(tmp.height % sec_size == 0); // we can lift this I guess

		if(end_col == 0)
			end_col = this->width;
		I left = sec_size / 2;
		I right = sec_size - left;
		D factor = D(1.0) / noise_size;
		D* tmp2data = tmp.data + this->height * this->width;
		for(I c = start_col, coff = start_col*this->height;
                c < end_col;
                ++c, coff += this->height) {
			for(I r = 0; r < this->height; r += sec_size) {
				I len = std::min(this->height - r, sec_size);
				D accum = 0;
				for(I b = 0, v = r, off = coff + r; b < len; ++b, ++v, ++off) {
					//accum += data[off];
					if(v >= shift)
						accum += data[coff + (v-shift)];
					if(v+shift >= this->height)
						accum += data[coff + (2*this->height -1 - (v+shift))];
					tmp.data[off] = accum;
				}
				accum = 0;
				for(I b = 0, v = r+len-1, off = coff + r + len - 1; b < len;
				       ++b, --v, --off) {
					//accum += data[off];
					if(v >= shift)
						accum += data[coff + (v-shift)];
					if(v+shift >= this->height)
						accum += data[coff + (2*this->height -1 - (v+shift))];
					tmp2data[off] = accum;
				}
			}

			for(I r = 0; r < this->height; ++r) {
				D sum;
				if(r < left) { // left reflection
					I idx0 = this->height + r - left;
					sum = tmp2data[coff + idx0] + tmp.data[coff + r + right];
					if(idx0<clamp)
						sum += tmp2data[coff + clamp];
				}
				else if(r + right >= this->height) { // right reflection
					I idx0 = r - left;
					I idx1 = r + right - this->height;
					sum = tmp2data[coff + r - left] + tmp.data[coff + idx1];
					if(idx0<clamp)
						sum += tmp2data[coff + clamp];
				}
				else
					sum = tmp.data[coff + r + right] + tmp2data[coff + r - left];
				I lh = (r-left-1+this->height) % this->height;
				I rh = (r+right+1) % this->height;
				//sum += (data[coff + lh] + data[coff + rh]) / 2;
				if(lh >= shift)
					sum += data[coff + (lh-shift)]/2;
				if(lh+shift >= this->height)
					sum += data[coff + (2*this->height -1 - (lh+shift))]/2;
				if(rh >= shift)
					sum += data[coff + (rh-shift)]/2;
				if(rh+shift >= this->height)
					sum += data[coff + (2*this->height -1 - (rh+shift))]/2;
				that.data[coff + r] = sum * factor;
			}
		}
	}

	I multiplication_flops() {
		return this->nnz * 2;
	}
};

/* overload of swap function */
template<typename I, typename D>
void swap(MatrixDense<I,D>& a, MatrixDense<I,D>& b) {
	std::swap(a.width, b.width);
	std::swap(a.height, b.height);
	std::swap(a.nnz, b.nnz);
	std::swap(a.data, b.data);
}

template<typename I, typename D>
void save_cw_plain_text(const char* file, MatrixDense<I,D>& mat);

template<typename I, typename D>
void save_rw_plain_text(const char* file, MatrixDense<I,D>& mat);

template<typename I, typename D>
void load_cw_plain_text(const char* file, MatrixDense<I,D>& mat);

/** three vectors of size nnz: row(s), col(s) and value(s)  */
template<typename I, typename D>
class MatrixCoo : public Matrix<I> {
public:
	bool is_mmapped;
    I* row_id;
    I* col_id;
    D* data;

	void free();

	MatrixCoo() : is_mmapped(false), row_id(NULL), col_id(NULL), data(NULL) { }

	~MatrixCoo() { free(); }

	D get_entry(I r, I c) {
		for(I i=0; i<this->nnz;++i)
			if(row_id[i]==r && col_id[i]==c)
				return data[i];
		return 0;
	}

	void apply(D* src, D* dst) {
		for(I i=0; i<this->height;++i)
			dst[i] = 0;
		for(I i=0; i<this->nnz;++i)
			dst[row_id[i]] += src[col_id[i]] * data[i];
	}

	/* applies to src and stores into dst, assuming they are column-wise stored */
	void apply_cw(const MatrixDense<I,D>& src, MatrixDense<I,D>& dst) {
		assert(src.height == this->width);
		assert(dst.height == this->height);
		assert(src.width == dst.width);
		for(I q = 0; q < src.width; ++q) {
			D* in_vec = src.data + q*src.height;
			D* out_vec = dst.data + q*dst.height;
			for(I r = 0; r < dst.height; ++r)
				out_vec[r] = 0;
			for(I n = 0; n < this->nnz; ++n)
				out_vec[row_id[n]] += in_vec[col_id[n]] * data[n];
		}
	}

	/* applies to src and stores into dst, assuming they are row-wise stored */
	void apply_rw(const MatrixDense<I,D>& src, MatrixDense<I,D>& dst) {
		assert(src.height == this->width);
		assert(dst.height == this->height);
		assert(src.width == dst.width);
		for(I i = 0; i < dst.width * dst.height; ++i)
			dst.data[i] = 0;
		for(I n = 0; n < this->nnz; ++n) {
			D* out_vec = dst.data + dst.width*row_id[n];
			D* in_vec = src.data + dst.width*col_id[n];
			D d = data[n];
			for(I q = 0; q < src.width; ++q)
				out_vec[q] += in_vec[q] * d;
		}
	}
	
	I multiplication_flops() {
		return this->nnz * 2;
	}
};

template<typename I, typename D>
void load_matrix_market_sliced(const char* file_format, MatrixCoo<I, D>& mat);

template<typename I, typename D>
void load_matrix_market(const char* filename, MatrixCoo<I, D>& mat);

template<typename I, typename D>
void save_binary_matrix(const char* filename, MatrixCoo<I, D>& mat);

template<typename I, typename D>
void load_binary_matrix(const char* filename, MatrixCoo<I, D>& mat);

template<typename I, typename D>
void map_binary_matrix(const char* filename, MatrixCoo<I, D>& mat);

template<typename I, typename D>
void unmap_binary_matrix(MatrixCoo<I, D>& mat);

/** arrays row_ptr of (height + 1) with index of row start,
    then arrays col_id, data of size (nnz) with col and data */
template<typename I, typename D>
class MatrixCsr : public Matrix<I> {
public:
    I* row_ptr;
    I* col_id;
    D* data;

	void free() {
		if(row_ptr) { delete[] row_ptr; row_ptr = NULL; }
		if(col_id) { delete[] col_id; col_id = NULL; }
		if(data) { delete[] data; data = NULL; }
	}

	MatrixCsr() : row_ptr(NULL), col_id(NULL), data(NULL) { }

	~MatrixCsr() { free(); }

	D get_entry(I r, I c) {
		for(I i=row_ptr[r]; i<row_ptr[r+1];++i)
			if(col_id[i]==c)
				return data[i];
		return 0;
	}

	I multiplication_flops() {
		return this->nnz * 2 + this->height;
	}

	/* applies to src and stores into dst, assuming they are row-wise stored */
	void apply_cw(const MatrixDense<I,D>& src, MatrixDense<I,D>& dst) {
		assert(src.height == this->width);
		assert(dst.height == this->height);
		assert(src.width == dst.width);
		for(I q = 0; q < dst.width; ++q)
		for(I r = 0; r < this->height; ++r) {
			I idx = row_ptr[r];
			I end = row_ptr[r+1];
			D* in_vec = src.data + q * src.height;
			D* out_vec = dst.data + q * dst.height;
			D sum = 0;
			for(I i = idx; i < end; ++i)
				sum += data[i] * in_vec[col_id[i]];
			out_vec[r] = sum;
		}
	}

	/* applies to src and stores into dst, assuming they are row-wise stored */
	void apply_rw(const MatrixDense<I,D>& src, MatrixDense<I,D>& dst) {
		assert(src.height == this->width);
		assert(dst.height == this->height);
		assert(src.width == dst.width);
		for(I q = 0; q < dst.width; ++q)
		for(I r = 0; r < this->height; ++r) {
			I idx = row_ptr[r];
			I end = row_ptr[r+1];
			D* in_vec = src.data + q;
			D* out_vec = dst.data + q;
			D sum = 0;
			for(I i = idx; i < end; ++i)
				sum += data[i] * in_vec[col_id[i] * src.width];
			out_vec[r * dst.width] = sum;
		}
	}
};

/** elongated lines, 'height_aligned' lines of length 'num', that
    is bigger than the number of  */
template<typename I, typename D>
class MatrixEll : public Matrix<I> {
public:
    I num;
    I height_aligned;
    I* col_id;
    D* data;

	void free() {
		if(col_id) { delete[] col_id; col_id = NULL; }
		if(data) { delete[] data; data = NULL; }
	}

	MatrixEll() : col_id(NULL), data(NULL) { }

	~MatrixEll() { free(); }

	I multiplication_flops() {
		return height_aligned * (2 * num + 1);
	}
};


template <class I, class D>
void FillRowPtr(const MatrixCoo<I, D>& source, I* row_ptr)
{
    row_ptr[0] = 0;
    I i(0);
    I curRow(0);
    while (curRow < source.height) {
	    while (i < source.nnz && source.row_id[i] == curRow)
		    i++;
	    row_ptr[++curRow] = i;
    }
}

template <class I, class D>
void Coo2Csr(const MatrixCoo<I, D>& source, MatrixCsr<I, D>& dest)
{
	dest.free();
    dest.width = source.width;
    dest.height = source.height;
    dest.nnz = source.nnz;

    dest.row_ptr = new I[source.height + 1];
    dest.col_id = new I[source.nnz];
    dest.data = new D[source.nnz];

    memcpy(dest.col_id, source.col_id, sizeof(I)*source.nnz);
    memcpy(dest.data, source.data, sizeof(D)*source.nnz);

    FillRowPtr(source, dest.row_ptr);
}

template <class I, class D>
void MoveCoo2Csr(MatrixCoo<I, D>& source, MatrixCsr<I, D>& dest)
{
	dest.free();
    dest.width = source.width;
    dest.height = source.height;
    dest.nnz = source.nnz;

    I nnz = source.nnz;
    dest.row_ptr = new I[source.height + 1];
    dest.col_id = source.col_id;
    dest.data = source.data;

    memcpy(dest.col_id, source.col_id, sizeof(I)*nnz);
    memcpy(dest.data, source.data, sizeof(D)*nnz);

    FillRowPtr(source, dest.row_ptr);

    delete[] source.row_id;
    source.row_id = NULL;
    source.col_id = NULL;
    source.data = NULL;
}

template <class I, class D>
void Csr2AlignedCsr(const MatrixCsr<I, D>& source, MatrixCsr<I, D>& dest)
{
	dest.free();
    dest.width = source.width;
    dest.height = source.height;

    dest.nnz = 0;
    for(I r = 0; r < source.height; ++r)
	    dest.nnz += (source.row_ptr[r+1] - source.row_ptr[r] + 3) & (-4);

    I nnz = dest.nnz;
    dest.row_ptr = new I[source.height + 1];
    dest.col_id = new I[nnz];
    dest.data = new D[nnz];

    dest.row_ptr[0] = 0;
    I o(0);
    I curRow(0);
    while (curRow < source.height) {
	    for(I i = source.row_ptr[curRow]; i < source.row_ptr[curRow+1]; ++i) {
		    dest.data[o] = source.data[i];
		    dest.col_id[o] = source.col_id[i];
		    o++;
	    }
	    while(o & 3) {
		    dest.data[o] = 0;
		    dest.col_id[o] = 0;
		    o++;
	    }
	    dest.row_ptr[++curRow] = o;
    }
}

template <class dimType>
dimType aligned_length(dimType length, dimType alignment)
{
    if (length % alignment == 0)
        return length;
    return length + alignment - length % alignment;
}

template <class I, class D>
void Coo2Ell(const MatrixCoo<I, D>& source, 
             MatrixEll<I, D>& dest, I alignment, I ellnum)
{
	dest.free();
    dest.width = source.width;
    dest.height = source.height;
    dest.nnz = source.nnz;

    MatrixCsr<I, D> csrmat;
    Coo2Csr<I, D>(source, csrmat);
   
    if (ellnum == (I)0) {
	    for (I i = (I)0; i < csrmat.height; i++)
		    ellnum = std::max(ellnum, csrmat.row_ptr[i+1] - csrmat.row_ptr[i]);
    }

    dest.num = ellnum;
    I newlength = aligned_length(source.height, alignment);
    dest.height_aligned = newlength;

    std::cout << "ELL = " << ellnum << std::endl;
    std::cout << "Waste ratio = " << double(newlength*ellnum)/source.nnz << std::endl;

    dest.col_id = new I[newlength*ellnum];
    dest.data = new D[newlength*ellnum];

    for (I i = (I)0; i < newlength * ellnum; i++) {
	    dest.col_id[i] = (I)0;
	    dest.data[i] = (D)0;
    }

    for (I i = (I)0; i < source.height; i++) {
	    I start = csrmat.row_ptr[i];
	    I rowlen = csrmat.row_ptr[i+1] - start;
	    if(rowlen > ellnum)
		    throw BasicException("Row entries bigger than Ell, at row %d: row len = %d, ell = %d",
		                         i, rowlen, ellnum);

	    I lastcolid = (I)0;

	    for (I j = 0; j < rowlen; j++) {
		    dest.col_id[i + j * newlength] = csrmat.col_id[j+start];
		    dest.data[i + j * newlength] = csrmat.data[j+start];
		    lastcolid = csrmat.col_id[j+start];
	    }
	    for (I j = rowlen; j < ellnum; j++) {
		    dest.col_id[i + j * newlength] = lastcolid;
		    dest.data[i + j * newlength] = (D)0;
	    }
    }
}

#endif //__MATRIX_H__

// Local Variables:
// mode: c++
// End:
