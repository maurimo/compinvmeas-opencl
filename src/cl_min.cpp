
#define __CL_ENABLE_EXCEPTIONS
#include "exceptions.h"
#include "cl_utils.h"
#include "cl_scalar.h"
#include "cl_norm.h"


template<typename I, typename D>
ClMinRw<I, D>::ClMinRw(cl::Context& context,
                       cl::Device& device) {
	min_prog = create_program(context, device,
	                          {"cl/min.cl"},
	                          Scalar<D>::Flags);
	k_min = cl::Kernel(min_prog, "min_rw");

	/*int ws = k_L1.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(device);
	k_L1_d2size = std::min(8, ws/LOCAL_SIZE);
	ws = k_Linf.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(device);
	k_Linf_d2size = std::min(8, ws/LOCAL_SIZE);
	std::cout << "ws = " << ws << ", s = " << k_Linf_d2size << std::endl;*/
}

template<typename I, typename D>
void ClMinRw<I, D>::bind_mat(const ClMatrixDense<I, D>& mat) {
	mh = mat.height;
	mw = mat.width;
	k_min.setArg(3, mw);
	k_min.setArg(4, mat.data);
}

template<typename I, typename D>
void ClMinRw<I, D>::bind_out(const ClMatrixDense<I, D>& out) {
	oh = out.height;
	ow = out.width;
	k_min.setArg(5, out.data);
}

template<typename I, typename D>
void ClMinRw<I, D>::compute_L1(cl::CommandQueue& queue,
                               uint32_t target,
                               uint32_t from,
                               uint32_t to) {
	if(to == 0)
		to = mw;
	if(ow != mw || oh != 1 || target >= oh || to <= from || to >= mh)
		throw UsageException("RW min M_%ux%u -> M_%ux%u ([%u, %u] -> %u)",
		                     mh, mw, oh, ow, from, to, target);
	k_min.setArg(3, mw);
	queue.enqueueNDRangeKernel(k_L1,
	                           cl::NDRange(0), //, 0),
	                           cl::NDRange(ow) //, LOCAL_SIZE),
	                           //cl::NDRange(k_L1_d2size, LOCAL_SIZE)
	                           );
}

/* explicit template instantiation */
template class ClMinRw<uint32_t, float>;
template class ClMinRw<uint32_t, double>;
