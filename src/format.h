#ifndef __FORMAT_H__
#define __FORMAT_H__

#include <string>
#include <cstdarg>

std::string strf(const char *fmt, ...) __attribute__((__format__ (__printf__, 1, 2)));

std::string vstrf(const char *fmt, va_list ap);

#endif //__FORMAT_H__

// Local Variables:
// mode: c++
// End:
