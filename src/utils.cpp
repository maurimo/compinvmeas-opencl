
#include <glog/logging.h>
#include <unistd.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#define __CL_ENABLE_EXCEPTIONS
#include "utils.h"

using namespace std;
namespace fs = boost::filesystem;

string get_file_contents(const char* filename) {
  ifstream in(filename, ios_base::in | ios_base::binary);
  CHECK(in) << "reading file \"" << filename << "\"";

  string contents;
  in.seekg(0, ios::end);
  contents.resize(in.tellg());
  in.seekg(0, ios::beg);
  in.read(&contents[0], contents.size());

  return contents;
}

std::string seconds_string(int64_t seconds) {
  ostringstream os;
  if (seconds >= 3600)
    os << seconds / 3600 << "h" << (seconds % 3600) / 60;
  else if (seconds >= 60)
    os << seconds / 60 << "m" << seconds % 60;
  else
    os << seconds << "s";
  return os.str();
}

static void stacktrace_writer(const char* data, size_t size) {
  ofstream out((FLAGS_log_dir + "/stacktrace.txt").c_str(),
               ios::out | ios::app);
  out.write(data, size);
  cerr.write(data, size);
}

void init_log(const char* prog) {
  FLAGS_log_dir = "logs";
  FLAGS_logtostderr = true;
  fs::create_directories(FLAGS_log_dir);
  google::InitGoogleLogging(prog);

  // i want as many stack traces as possible
  google::InstallFailureSignalHandler();
  google::InstallFailureWriter(stacktrace_writer);
}

void delay(uint32_t msec) { usleep(msec * 1000ULL); }

class TimerImpl {
 public:
  boost::posix_time::ptime tm;
};

Timer::Timer() : priv(new TimerImpl) { reset(); }

Timer::~Timer() { delete priv; }

void Timer::reset() {
  priv->tm = boost::posix_time::microsec_clock::local_time();
}

int64_t Timer::elapsed_microsec() {
  return (boost::posix_time::microsec_clock::local_time() - priv->tm)
      .total_microseconds();
}
