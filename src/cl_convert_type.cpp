
#define __CL_ENABLE_EXCEPTIONS
#include "exceptions.h"
#include "cl_utils.h"
#include "scalar.h"
#include "cl_convert_type.h"

template<typename I, typename D>
ClConvertType<I, D>::ClConvertType(cl::Context& context,
                                   cl::Device& device) {
	p_prog = create_program(context, device,
	                        {"cl/convert_type.cl"},
	                        Scalar<D>::Flags);
	k_convert = cl::Kernel(p_prog, "convert_cw_to_rw");
}

template<typename I, typename D>
void ClConvertType<I, D>::cw_to_rw(const ClMatrixDense<I, D>& in,
                                   const ClMatrixDense<I, D>& out,
                                   cl::CommandQueue& queue) {
	
	k_convert.setArg(0, in.height);
	k_convert.setArg(1, in.width);
	k_convert.setArg(2, in.data);
	k_convert.setArg(3, out.data);
	queue.enqueueNDRangeKernel(k_convert,
	                           cl::NullRange,
	                           cl::NDRange(in.height, in.width) );
}

template<typename I, typename D>
void ClConvertType<I, D>::rw_to_cw(const ClMatrixDense<I, D>& in,
                                   const ClMatrixDense<I, D>& out,
                                   cl::CommandQueue& queue) {
	
	k_convert.setArg(0, in.width);
	k_convert.setArg(1, in.height);
	k_convert.setArg(2, in.data);
	k_convert.setArg(3, out.data);
	queue.enqueueNDRangeKernel(k_convert,
	                           cl::NullRange,
	                           cl::NDRange(in.width, in.height) );
}

template class ClConvertType<uint32_t, float>;
template class ClConvertType<uint32_t, double>;
