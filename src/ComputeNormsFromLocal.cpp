
#include <glog/logging.h>
#include <inttypes.h>

#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/numeric/interval/hw_rounding.hpp>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <system_error>
#define __CL_ENABLE_EXCEPTIONS
#include <limits>

#include "exceptions.h"
#include "mmap.h"
#include "utils.h"

typedef double D;

using namespace std;

int main(int argc, char* argv[]) {
  uint32_t size = atoi(argv[1]);
  uint32_t max_it = atoi(argv[2]);
  uint32_t noise_size = atoi(argv[3]);

  char* vec_norm_file = argv[4];
  char* vec_scat_file = argv[5];
  char* out_vec_file = argv[6];
  bool mod1 = false;
  if (argc >= 8) {
    if (!strcmp(argv[7], "-noise-mod-1"))
      mod1 = true;
    else {
      printf("Unsupported option!");
      return 1;
    }
  }

  uint64_t size1, size2;
  D* vec_norm = (D*)MMap::map_file(vec_norm_file, &size1);
  D* vec_scat = (D*)MMap::map_file(vec_scat_file, &size2);
  // size1 = size2;

  if (size1 != size * max_it * sizeof(D) ||
      size2 != size * max_it * sizeof(D)) {
    cout << "Wrong file sizes!!" << endl;
    return 1;
  }

  boost::numeric::interval_lib::rounded_math<D> rnd;
  uint32_t hnoise_size = noise_size / 2;
  vector<D> out_vec(max_it);
  vector<D> out_vec1(max_it);
  vector<D> out_vec2(max_it);

  if (mod1) {
    for (uint32_t i = 0; i < size; i++) {
      uint32_t offn = i * max_it;
      uint32_t offs1 = ((i - hnoise_size + size) % size) * max_it;
      uint32_t offs2 = ((i + hnoise_size + size) % size) * max_it;
      for (uint32_t j = 0; j < max_it; j++) {
        D val = rnd.add_up(
            vec_norm[offn + j],
            rnd.div_up(rnd.add_up(vec_scat[offs1 + j], vec_scat[offs2 + j]),
                       2));
        out_vec[j] = max(out_vec[j], val);
        out_vec1[j] = max(out_vec1[j], vec_norm[offn + j]);
        out_vec2[j] = max(out_vec2[j], vec_scat[offn + j]);
      }
    }
  } else {
    for (uint32_t i = 0; i < size; i++) {
      uint32_t offn = i * max_it;
      uint32_t offs1 =
          (i >= hnoise_size ? i - hnoise_size : 1 + hnoise_size - i) * max_it;
      uint32_t offs2 =
          (i + hnoise_size < size ? i + hnoise_size
                                  : 2 * size - i - hnoise_size - 1) *
          max_it;
      for (uint32_t j = 0; j < max_it; j++) {
        D val = rnd.add_up(
            vec_norm[offn + j],
            rnd.div_up(rnd.add_up(vec_scat[offs1 + j], vec_scat[offs2 + j]),
                       2));
        out_vec[j] = max(out_vec[j], val);
        out_vec1[j] = max(out_vec1[j], vec_norm[offn + j]);
        out_vec2[j] = max(out_vec2[j], vec_scat[offn + j]);
      }
    }
  }

  D sum = 0, sum1 = 0, sum2 = 0;
  for (uint32_t j = 0; j < max_it; j++) {
    sum += out_vec[j];
    sum1 += out_vec1[j];
    sum2 += out_vec2[j];
    cout << out_vec[j] << " ";
  }
  cout << endl << "Ci-Sum: " << sum << endl;
  cout << "Partial Ci-Sum1: " << sum1 << endl;
  cout << "Partial Ci-Sum2: " << sum2 << endl;

  ofstream out(out_vec_file);
  out.write((char*)out_vec.data(), out_vec.size() * sizeof(D));

  return 0;
}
