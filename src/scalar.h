#ifndef __SCALAR_H__
#define __SCALAR_H__

template<typename D>
class Scalar {
public:
	static const char* Tag;
	static const char* Flags;
};

#endif //__SCALAR_H__

// Local Variables:
// mode: c++
// End:
