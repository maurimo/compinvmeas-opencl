
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <system_error>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <inttypes.h>
#include <boost/filesystem.hpp>
#include <boost/numeric/interval/hw_rounding.hpp>
#include <glog/logging.h>
#include "exceptions.h"
#include "utils.h"
#include "matrix.h"
#include "scalar.h"

#ifndef CPU_ONLY
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#include "cl_matrix_grouped_input.h"
#include "cl_utils.h"
#include "cl_noise.h"
#include "cl_convert_type.h"
#include "cl_pretty_print.h"
#endif

using namespace std;

#define DEFAULT_NORM_TYPE Norm_L1
#define DEFAULT_NUM_ITERATIONS 3000
#define DEFAULT_OUT_FILE "out.txt"



template<typename D>
bool all_small_eq(D *d1, D *d2, uint32_t num) {
	for(uint32_t i = 0; i<num; i++)
		if(d1[i] > d2[i])
			return false;
	return true;
}

vector<string> norm_type_names = {"Norm_L1", "Norm_Linf"};

string join(const vector<string>& names) {
	ostringstream os;
	for(unsigned int i = 0; i<names.size(); i++) {
		if(i>0) os << ",";
		os << names[i];
	}
	return os.str();
}

template<typename E>
E get_enum_choice(const vector<string>& names, string name) {
	for(unsigned int i = 0; i<names.size(); i++)
		if(name == names[i])
			return (E)i;
	throw UsageException("invalid %s, should be one of %s!",
	                     name.c_str(), join(names).c_str());
}

class IterateSpMArgs {
public:
	enum Prec {
		Float = 1,
		Double = 2,
		Extended = 3
	};
	enum NormType {
		Norm_L1 = 0,
		Norm_Linf = 1
	};
	bool noise_mod1 = false;
	int noise = 0;
	int noise_shift = 0;
	int platform_index = 0;
	int device_index = 0;
    const char* matrix_file = NULL;
    const char* output_file = DEFAULT_OUT_FILE;
	const char* input_file = NULL;
	Prec precision = Float;
    uint32_t num_iterations = DEFAULT_NUM_ITERATIONS;
	NormType norm_type = DEFAULT_NORM_TYPE;
	double step_error = -1;
	double relative_residue = -1;
    bool is_binary = false;
    bool show_help = false;
    bool run_test = false;
    bool use_cpu = false;

	bool init(int argc, char* argv[]);
};


bool IterateSpMArgs::init(int argc, char* argv[]) {

    for(int i = 1; i < argc; i++) {
	    if(!strcmp(argv[i], "-f") && i < argc-1) {
		    i++;
		    if(!strcasecmp(argv[i],"f"))
			    precision = Float;
		    else if(!strcasecmp(argv[i],"d"))
			    precision = Double;
		    else if(!strcasecmp(argv[i],"e"))
			    precision = Extended;
		    else {
			    cout << "Precision one must be one of "
			         << "f=float, d=double, e=long double" << endl;
			    return false;
		    }
	    }
	    else if(!strcmp(argv[i], "-p") && i < argc-1)
	        platform_index = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-d") && i < argc-1)
	        device_index = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-m") && i < argc-1) {
            matrix_file = argv[++i];
            is_binary = false;
        }
        else if(!strcmp(argv[i], "-b") && i < argc-1) {
            matrix_file = argv[++i];
            is_binary = true;
        }
        else if(!strcmp(argv[i], "-o") && i < argc-1)
            output_file = argv[++i];
        else if(!strcmp(argv[i], "-i") && i < argc-1)
            input_file = argv[++i];
        else if(!strcmp(argv[i], "-n") && i < argc-1)
            num_iterations = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-r") && i < argc-1)
            noise = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-noise-shift") && i < argc-1)
            noise_shift = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-norm-type") && i < argc-1)
	        norm_type = get_enum_choice<NormType>(norm_type_names, string(argv[++i]));	    
        else if(!strcmp(argv[i], "-step-error") && i < argc-1)
	        step_error = strtod(argv[++i], NULL);
        else if(!strcmp(argv[i], "-relative-residue") && i < argc-1)
	        relative_residue = strtod(argv[++i], NULL);
        else if(!strcmp(argv[i], "-noise-mod-1"))
            noise_mod1 = true;
        else if(!strcmp(argv[i], "-c"))
            use_cpu = true;
        else if(!strcmp(argv[i], "-h"))
            show_help = true;
        else if(!strcmp(argv[i], "-t"))
            run_test = true;
    }

    if(show_help || !matrix_file) {
        cout << "Usage: IterateSpM -b|-m matrix_file [args]" << endl
             << "    -m matrix.mkt  Loads a matrix in MatrixMarket format" << endl
             << "    -b matrix.bin  Loads a matrix in binary COO format" << endl
             << "    -o out.txt     File for output vector (default:" <<
                                              DEFAULT_OUT_FILE << ")" << endl
             << "    -i in.txt      File for input vector (default: all one's)" << endl
             << "    -n num         Number of iterations (default:" <<
                                              DEFAULT_NUM_ITERATIONS << ")" << endl
             << "    -r size        Apply random noise (default: 0=disabled)" << endl
             << "    -noise-shift sz  Asymmetric noise shift (default: 0=disabled)" << endl
             << endl
             << "    -noise-mod-1   Make the noise cyclic" << endl
             << "    -c             Run on the CPU only (using binary format, the matrix" << endl
             << "                   is mmap'ed, so this is suitable for huge matrices)" << endl
             << "    -p n           The index of the OpenCl platform to use" << endl
             << "    -d n           The index of the platform's device to use" << endl
             << "    -f f|d|e       The float precision (f=float, d=double, e=long double)" << endl
             << "    -t             Run test, if in GPU mode" << endl
             << "    -h             Show this help" << endl;
        return false;
    }

    return true;
}



// iteration loop on the CPU
template<typename D>
void iterate_matrix_cpu(const IterateSpMArgs& a) {

    MatrixCoo<uint32_t, D> mCoo;
    cout << "Running on the CPU (-c)" << endl;

    /* load main matrix */
    if(a.is_binary)
	    map_binary_matrix(a.matrix_file, mCoo);
    else
	    load_matrix_market(a.matrix_file, mCoo);

    if(mCoo.height != mCoo.width) {
	    cout << "Matrix size: " << mCoo.height << "x" << mCoo.width << endl;
	    throw UsageException("Matrix is not a square matrix!");
    }
    uint32_t size = mCoo.height;
    cout << "Matrix loaded, " << size << "x" << size << endl;

    MatrixDense<uint32_t, D> vCurrent(size), vOutput(size), vNorm(1),
	    vNoiseOutput, vNoiseTmp;
    if(a.noise) {
	    vNoiseOutput.init(size);
	    vNoiseTmp.init(size, 2);
    }
    if(a.input_file) {
	    load_cw_plain_text(a.input_file, vCurrent);
	    if(vCurrent.width != 1 || vCurrent.height != size)
		    throw UsageException("Invalid input matrix of size %ux%u (should be a vector 1x%u)",
		                         vCurrent.height, vCurrent.width, size);
    }
    else
	    vCurrent.set(1.0);

    Timer timer;
    bool success = false;
    for(uint32_t i = 0; i<a.num_iterations; i++) {
	    mCoo.apply_cw(vCurrent, vOutput);

	    if(a.noise) {
		    cout << "apply noise!" << endl;
		    if(a.noise_mod1)
			    vOutput.apply_noise_cw(a.noise, a.noise_shift, vNoiseOutput, vNoiseTmp);
		    else
			    vOutput.apply_noise_cw(a.noise, a.noise_shift, vNoiseOutput, vNoiseTmp);
		    swap(vOutput, vNoiseOutput);
	    }

	    swap(vCurrent, vOutput);

	    if(a.norm_type == IterateSpMArgs::Norm_L1)
		    vCurrent.norm_L1_cw_down(vNorm);
		else
			vCurrent.norm_Linf_cw(vNorm);
	    D n = vNorm.data[0];
	   
	    D step = a.norm_type == IterateSpMArgs::Norm_L1
		    ? vCurrent.distance_L1_cw_up(vOutput)
		    : vCurrent.distance_Linf_cw(vOutput);

	    cout << (i+1) <<  "-th norm step: " << step
	         << " (of " << n << ")" << endl;
		boost::numeric::interval_lib::rounded_math<D> rnd;
		step = rnd.add_up(step, rnd.mul_up(n, a.step_error));

		if(rnd.div_up(step, n) < a.relative_residue) {
		    success = true;
		    cout << "Great, relative residue <= " << a.relative_residue << endl;
		    break;
	    }
    }
    double microseconds = timer.elapsed_microsec();
    double flops = (double)mCoo.multiplication_flops();
    if(a.noise)
	    flops += vCurrent.noise_flops();
    cout << "CPU iterations per sec: " << (a.num_iterations*1000000/microseconds) << endl;
    cout << "CPU Gflops: " << (a.num_iterations*flops/(microseconds*1000)) << endl;

    if(success) {
	    cout << "Writing " << a.output_file << endl;
	    ofstream out(a.output_file);
	    out.write((char*)vCurrent.data, vCurrent.height * sizeof(D));
    }
    else {
	    cout << "Failure, not converging after " << a.num_iterations
	         << " iterations!" << endl
	         << "Try with more iterations?" << endl;
	    throw BasicException("No convergence");
    }
}

#ifndef CPU_ONLY
// iteration loop on the GPU
template<typename D>
void iterate_matrix_cl(const IterateSpMArgs& a, cl::Device device) {

	throw BasicException("Broken GPU code, run iterations on the CPU please!");

#if 0
    MatrixCoo<uint32_t, D> mCoo;

    // load main matrix
    if(a.is_binary)
        load_binary_matrix(a.matrix_file, mCoo);
    else
        load_matrix_market(a.matrix_file, mCoo);

    // should be a square matrix
    if(mCoo.height != mCoo.width)
	    throw UsageException("Matrix is not a square matrix!");
    uint32_t size = mCoo.height;

    // convert vCurrent csr format
    MatrixCsr<uint32_t, D> mCsr;
    MoveCoo2Csr(mCoo, mCsr);
    cout << "Matrix loaded, " << size << "x" << size << endl;

    // input matrix
    MatrixDense<uint32_t, D> vCurrent(size);
    if(a.input_file) {
	    load_cw_plain_text(a.input_file, vCurrent);
	    if(vCurrent.width != 1 || vCurrent.height != size)
		    throw UsageException("Invalid input matrix of size %ux%u (should be a vector 1x%u)",
		                         vCurrent.height, vCurrent.width, size);
    }
    else
	    vCurrent.set(1.0);
  
    // create context
    cl::Context context(device);

    //create queue to which we will push commands for the device.
    cl::CommandQueue queue(context, device);

    // create cl objects
    ClMatrixDense<uint32_t, D> vCurrentCL(vCurrent, context, queue);
    ClMatrixDense<uint32_t, D> vOutputCL(vCurrent.height, vCurrent.width, context);

    //FIXME: allocate only when necessary
    //FIXME: height will have to be rounded to a multiple of size-1
    ClMatrixDense<uint32_t, D> vNoiseTmpCL(vCurrent.height, vCurrent.width * 2, context);
    
    ClMatrixGroupedInput<uint32_t, D> matGrpdCL(mCsr, context, queue);

    ClNoiseCw<uint32_t, D> kNoiseCL(context, device);
    kNoiseCL.bind_tmp(vNoiseTmpCL);
    
    /*ClConvertType<uint32_t, D> kConvertCL(context, device);
    ClMatrixDense<uint32_t, D> vSwapCL(vCurrent.width, vCurrent.height, context);
    ClMatrixDense<uint32_t, D> vSwap2CL(vCurrent.width, vCurrent.height, context);
    ClNoiseRw<uint32_t, D> kNoiseCL(context, device);
    kNoiseCL.bind_tmp(vNoiseTmpCL);*/

    // load multiplier kernel
    ClMultiplierGroupedInputCw<uint32_t, D> kMultCL(context, device);
    kMultCL.bind_mat(matGrpdCL);
    if(a.run_test) {
	    kMultCL.bind_in(vCurrentCL);
	    kMultCL.bind_out(vOutputCL);
	    kMultCL.run(queue); //one run to make sure everything is loaded

	    if(a.noise) {
		    kNoiseCL.bind_in(vCurrentCL);
		    kNoiseCL.bind_out(vOutputCL);
		    kNoiseCL.run(17, queue);
	    }
	    queue.finish();
    }

    // loop on the gpu
    Timer timer;
    for(uint32_t i = 0; i < a.num_iterations; ++i) {
	    kMultCL.bind_in(vCurrentCL);
	    kMultCL.bind_out(vOutputCL);
	    kMultCL.run(queue);

        if(!a.noise) {
	        swap(vCurrentCL, vOutputCL); //now vCurrentCL will has the output of the iteration
	        continue;
        }
        
        kNoiseCL.bind_in(vOutputCL);
        kNoiseCL.bind_out(vCurrentCL);
        kNoiseCL.run(a.noise, a.noise_shift, queue);
    }
    queue.finish();
    double gpu_microseconds = timer.elapsed_microsec();

    // gpu computation stats
    double flops = (double)mCsr.multiplication_flops();
    if(a.noise)
	    flops += vCurrent.noise_flops();
    cout << endl;
    cout << "GPU iterations per sec: " << (a.num_iterations*1000000/gpu_microseconds) << endl;
    cout << "GPU Gflops: " << (a.num_iterations*flops/(gpu_microseconds*1000)) << endl;

    // run also on the cpu, for debugging purposes
    if(a.run_test) {
	    MatrixDense<uint32_t, D> vOutput(size), vNoiseOutput, vNoiseTmp;
	    if(a.noise) {
		    vNoiseOutput.init(size);
		    vNoiseTmp.init(size, 2);
	    }

	    // compute once on the CPU if running test
	    cout << endl;
	    std::cout << "Running CPU Verification... (-t)" << std::endl;

	    timer.reset();
	    for(uint32_t i = 0; i<a.num_iterations; i++) {
		    mCsr.apply_cw(vCurrent, vOutput);

		    if(a.noise) {
			    //cout << "apply noise!" << endl;
			    vOutput.apply_noise_cw(a.noise, a.noise_shift, vNoiseOutput, vNoiseTmp);
			    swap(vOutput, vNoiseOutput);
		    }
		    swap(vCurrent, vOutput);
	    }
	    double cpu_microseconds = timer.elapsed_microsec();

	    // read the GPU computed result in vOutput, and compare
	    vCurrentCL.read(vOutput, queue);

	    // compute Linfinity distance
	    D maxerr = vCurrent.distance_Linf_cw(vOutput);
	    std::cout << endl << "Maximum error: " << maxerr << endl;
	    if(maxerr > 0.00001)
		    std::cout << "***** VERY BIG ERROR: SOMETHING IS WRONG! *****" << endl;

	    // check if successfully rounded above or below (not possible at the moment)
	    if(all_small_eq(vCurrent.data, vOutput.data, mCsr.height))
		    std::cout << "All GPU results are <= than those of the CPU" << std::endl;
	    if(all_small_eq(vOutput.data, vCurrent.data, mCsr.height))
		    std::cout << "All GPU results are >= than those of the CPU" << std::endl;

	    // print CPU stats and comparison
	    cout << endl;
	    cout << "CPU iterations per sec: " << (a.num_iterations*1000000/cpu_microseconds) << endl;
	    cout << "CPU Gflops: " << (a.num_iterations*flops/(cpu_microseconds*1000)) << endl;
	    cout << "GPU faster of a factor " << (cpu_microseconds/gpu_microseconds) << endl;
    }
    else {
	    vCurrentCL.read(vCurrent, queue);
	    save_cw_plain_text(a.output_file, vCurrent);

	    /*MatrixDense<uint32_t, D> vNoiseTmp;
	    vNoiseTmp.init(size, 2);
	    vNoiseTmpCL.read(vNoiseTmp, queue);
	    save_cw_plain_text("tmp.txt", vNoiseTmp);*/
    }
#endif
}
#endif

int main(int argc, char* argv[]) {

    init_log(argv[0]);

    try {
	    IterateSpMArgs a;
	    if(!a.init(argc, argv))
		    return 0;

	    if(a.use_cpu) {
		    if(a.precision == IterateSpMArgs::Extended)
			    iterate_matrix_cpu<long double>(a);
		    else if(a.precision == IterateSpMArgs::Double)
			    iterate_matrix_cpu<double>(a);
		    else
			    iterate_matrix_cpu<float>(a);
	    }
	    else {
#ifndef CPU_ONLY
		    if(a.precision == IterateSpMArgs::Extended)
			    throw UsageException("long double precision not supported by OpenCl");
		    
		    cl::Platform platform = get_platform(a.platform_index);
		    cl::Device device = get_device(platform, CL_DEVICE_TYPE_ALL, a.device_index);

		    if(a.precision == IterateSpMArgs::Double) {
			    if(device.getInfo<CL_DEVICE_EXTENSIONS>().find("cl_khr_fp64") == string::npos)
				    throw UsageException("double precision not supported by this device");
			    iterate_matrix_cl<double>(a, device);
		    }
		    else
			    iterate_matrix_cl<float>(a, device);
#else
		    std::cerr << "ERROR: GPU support not enabled at compile time." << std::endl;
		    return 1;
#endif
	    }
    }
#ifndef CPU_ONLY
    catch (cl::Error& err) {
        std::cerr << "ERROR: " << err.what() << "(" << err.err() << ")" << std::endl;
        print_opencl_error_code(err.err());
    }
#endif
    catch (BasicException& e) {
        std::cerr << "Exception: " << e.message() << std::endl;
    }

    return 0;
}
