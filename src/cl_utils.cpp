
#include <fstream>
#include <map>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include <glog/logging.h>
#include "utils.h"
#define __CL_ENABLE_EXCEPTIONS
#include "cl_pretty_print.h"
#include "cl_utils.h"

using namespace std;
namespace fs = boost::filesystem;

cl::Platform get_platform(unsigned int platform_index) {
	std::vector<cl::Platform> all_platforms;
	cl::Platform::get(&all_platforms);
	CHECK_GT(all_platforms.size(), 0) << "searching platforms. Check OpenCL installation!";

	std::cout << "Platforms:" << std::endl;
	for(unsigned int i = 0; i < all_platforms.size(); ++i)
		logInfo(std::cout, all_platforms[i], i);

	CHECK_GT(all_platforms.size(), platform_index) << 
		"searching platforms. No index" << platform_index << "!";

	cl::Platform plat = all_platforms[platform_index];
	std::cout << "Using platform: " << plat.getInfo<CL_PLATFORM_NAME>() << std::endl;
	return plat;
}

cl::Device get_device(cl::Platform platform, cl_device_type type, unsigned int device_index) {
	std::vector<cl::Device> all_devices;
	platform.getDevices(type, &all_devices);
	CHECK_GT(all_devices.size(), 0) << "searching devices. Check OpenCL installation!";

	std::cout << "Devices:" << std::endl;
	for(unsigned int i = 0; i < all_devices.size(); ++i)
		logInfo(std::cout, all_devices[i], i);

	CHECK_GT(all_devices.size(), device_index) << 
		"search device indices. No index " << device_index << "!";

	cl::Device dev = all_devices[device_index];
	std::cout << "Using device: " << dev.getInfo<CL_DEVICE_NAME>() << std::endl;
	return dev;
}


cl::Device get_device_from_params(int argc, char* argv[]) {
	int nplat = 0, ndev = 0;

	if(const char* p = getenv("CL_PLATFORM"))
		nplat = atoi(p);
	if(const char* d = getenv("CL_DEVICE"))
		ndev = atoi(d);

	for(int i = 1; i<argc-1; ++i) {
		if(!strcmp(argv[i],"-p"))
			nplat = atoi(argv[++i]);
		else if(!strcmp(argv[i],"-d"))
			ndev = atoi(argv[++i]);
	}

	cl::Platform plat = get_platform(nplat);
	return get_device(plat, CL_DEVICE_TYPE_ALL, ndev);
}


cl::Program create_program(cl::Context context,
                           cl::Device dev,
                           const std::vector<const char*>& source_files,
                           const std::string& flags) {
	cl::Program::Sources sources;
	std::vector<std::string> source_data;
	for(const char* source_file : source_files) {
		std::ostringstream os;
		os << "#1 \"" << source_file << "\"\n" << get_file_contents(source_file);
		source_data.push_back(os.str());
		sources.push_back({source_data.back().c_str(), source_data.back().length()});
	}

	//create and compile the program
	cl::Program prog(context, sources);
	sources.clear();
	source_data.clear();

	try {
		prog.build({dev}, ("-Icl " + flags).c_str());
		std::string log = prog.getBuildInfo<CL_PROGRAM_BUILD_LOG>(dev);
		if(!log.empty() && log != "\n")
			LOG(WARNING) << "Warnings:\n" << log;
	}
	catch(std::exception& e) {
		LOG(FATAL) << e.what() << ": Compiler error:\n" <<
			prog.getBuildInfo<CL_PROGRAM_BUILD_LOG>(dev);
	}

	return prog;
}

void compute_best_local_decomposition(uint32_t& gsize, uint32_t& lsize) {
	lsize = 128;
	while(lsize >= gsize * 2)
		lsize /= 2;
	gsize = (gsize + lsize - 1) & (-lsize);
}

static std::map<cl_int, std::array<const char*, 3> > OpenCL_ErrorCodes = {
 {0,
  {"CL_SUCCESS", "", "The sweet spot."} },
 {-1,
  {"CL_DEVICE_NOT_FOUND",
   "clGetDeviceIDs",
   "if no OpenCL devices that matched device_type were found."} },
 {-2,
  {"CL_DEVICE_NOT_AVAILABLE",
   "clCreateContext",
   "if a device in devices is currently not available even though the device was returned by\n"
   "clGetDeviceIDs."
  } },
 {-3,
  {"CL_COMPILER_NOT_AVAILABLE",
   "clBuildProgram",
   "if program is created with clCreateProgramWithSource and a compiler is not available i.e.\n"
   "CL_DEVICE_COMPILER_AVAILABLE specified in the table of OpenCL Device Queries for\n"
   "clGetDeviceInfo is set to CL_FALSE."
  } },
 {-4,
  {"CL_MEM_OBJECT_ALLOCATION_FAILURE",
   "",
   "if there is a failure to allocate memory for buffer object."
  } },
 {-5,
  {"CL_OUT_OF_RESOURCES",
   "",
   "if there is a failure to allocate resources required by the OpenCL implementation on the\n"
   "device."
  } },
 {-6,
  {"CL_OUT_OF_HOST_MEMORY",
   "",
   "if there is a failure to allocate resources required by the OpenCL implementation on the host."
  } },
 {-7,
  {"CL_PROFILING_INFO_NOT_AVAILABLE",
   "clGetEventProfilingInfo",
   "if the CL_QUEUE_PROFILING_ENABLE flag is not set for the command-queue, if the execution\n"
   "status of the command identified by event is not CL_COMPLETE or if event is a user event\n"
   "object."
  } },
 {-8,
  {"CL_MEM_COPY_OVERLAP",
   "clEnqueueCopyBuffer, clEnqueueCopyBufferRect, clEnqueueCopyImage",
   "if src_buffer and dst_buffer are the same buffer or subbuffer object and the source and\n"
   "destination regions overlap or if src_buffer and dst_buffer are different sub-buffers of\n"
   "the same associated buffer object and they overlap. The regions overlap if\n"
   "    src_offset ≤ dst_offset ≤ src_offset + size - 1,\n"
   "or if\n"
   "    dst_offset ≤ to src_offset ≤ to dst_offset + size - 1."
  } },
 {-9,
  {"CL_IMAGE_FORMAT_MISMATCH",
   "clEnqueueCopyImage",
   "if src_image and dst_image do not use the same image format."
  } },
 {-10,
  {"CL_IMAGE_FORMAT_NOT_SUPPORTED",
   "clCreateImage",
   "if the image_format is not supported."
  } },
 {-11,
  {"CL_BUILD_PROGRAM_FAILURE",
   "clBuildProgram",
   "if there is a failure to build the program executable. This error will be returned if\n"
   "clBuildProgram does not return until the build has completed."
  } },
 {-12,
  {"CL_MAP_FAILURE",
   "clEnqueueMapBuffer, clEnqueueMapImage",
   "if there is a failure to map the requested region into the host address space. This error\n"
   "cannot occur for image objects created with CL_MEM_USE_HOST_PTR or CL_MEM_ALLOC_HOST_PTR."
  } },
 {-13,
  {"CL_MISALIGNED_SUB_BUFFER_OFFSET",
   "",
   "if a sub-buffer object is specified as the value for an argument that is a buffer object and\n"
   "the offset specified when the sub-buffer object is created is not aligned to\n"
   "CL_DEVICE_MEM_BASE_ADDR_ALIGN value for device associated with queue."
  } },
 {-14,
  {"CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST",
   "",
   "if the execution status of any of the events in event_list is a negative integer value."
  } },
 {-15,
  {"CL_COMPILE_PROGRAM_FAILURE",
   "clCompileProgram",
   "if there is a failure to compile the program source. This error will be returned if\n"
   "clCompileProgram does not return until the compile has completed."
  } },
 {-16,
  {"CL_LINKER_NOT_AVAILABLE",
   "clLinkProgram",
   "if a linker is not available i.e. CL_DEVICE_LINKER_AVAILABLE specified in the table of\n"
   "allowed values for param_name for clGetDeviceInfo is set to CL_FALSE."
  } },
 {-17,
  {"CL_LINK_PROGRAM_FAILURE",
   "clLinkProgram",
   "if there is a failure to link the compiled binaries and/or libraries."} },
 {-18,
  {"CL_DEVICE_PARTITION_FAILED",
   "clCreateSubDevices",
   "if the partition name is supported by the implementation but in_device could not be further\n"
   "partitioned."
  } },
 {-19,
  {"CL_KERNEL_ARG_INFO_NOT_AVAILABLE",
   "clGetKernelArgInfo",
   "if the argument information is not available for kernel."
  } },
 {-30,
  {"CL_INVALID_VALUE",
   "clGetDeviceIDs, clCreateContext",
   "This depends on the function: two or more coupled parameters had errors."
  } },
 {-31,
  {"CL_INVALID_DEVICE_TYPE",
   "clGetDeviceIDs",
   "if an invalid device_type is given"
  } },
 {-32,
  {"CL_INVALID_PLATFORM",
   "clGetDeviceIDs",
   "if an invalid platform was given"
  } },
 {-33,
  {"CL_INVALID_DEVICE",
   "clCreateContext, clBuildProgram",
   "if devices contains an invalid device or are not associated with the specified platform."
  } },
 {-34,
  {"CL_INVALID_CONTEXT",
   "",
   "if context is not a valid context."
  } },
 {-35,
  {"CL_INVALID_QUEUE_PROPERTIES",
   "clCreateCommandQueue",
   "if specified command-queue-properties are valid but are not supported by the device."
  } },
 {-36,
  {"CL_INVALID_COMMAND_QUEUE",
   "",
   "if command_queue is not a valid command-queue."
  } },
 {-37,
  {"CL_INVALID_HOST_PTR",
   "clCreateImage, clCreateBuffer",
   "This flag is valid only if host_ptr is not NULL. If specified, it indicates that the\n"
   "application wants the OpenCL implementation to allocate memory for the memory object and copy\n"
   "the data from memory referenced by host_ptr. CL_MEM_COPY_HOST_PTR and CL_MEM_USE_HOST_PTR\n"
   "are mutually exclusive. CL_MEM_COPY_HOST_PTR can be used with CL_MEM_ALLOC_HOST_PTR to\n"
   "initialize the contents of the cl_mem object allocated using host-accessible (e.g. PCIe)\n"
   "memory."
  } },
 {-38,
  {"CL_INVALID_MEM_OBJECT",
   "",
   "if memobj is not a valid OpenCL memory object."
  } },
 {-39,
  {"CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
   "",
   "if the OpenGL/DirectX texture internal format does not map to a supported OpenCL image format."
  } },
 {-40,
  {"CL_INVALID_IMAGE_SIZE",
   "",
   "if an image object is specified as an argument value and the image dimensions (image width,\n"
   "height, specified or compute row and/or slice pitch) are not supported by device associated\n"
   "with queue."
  } },
 {-41,
  {"CL_INVALID_SAMPLER",
   "clGetSamplerInfo, clReleaseSampler, clRetainSampler, clSetKernelArg",
   "if sampler is not a valid sampler object."
  } },
 {-42,
  {"CL_INVALID_BINARY",
   "clCreateProgramWithBinary, clBuildProgram",
   "The provided binary is unfit for the selected device. if program is created with\n"
   "clCreateProgramWithBinary and devices listed in device_list do not have a valid program\n"
   "binary loaded."
  } },
 {-43,
  {"CL_INVALID_BUILD_OPTIONS",
   "clBuildProgram",
   "if the build options specified by options are invalid."
  } },
 {-44,
  {"CL_INVALID_PROGRAM",
   "",
   "if program is a not a valid program object."
  } },
 {-45,
  {"CL_INVALID_PROGRAM_EXECUTABLE",
   "",
   "if there is no successfully built program executable available for device associated with\n"
   "command_queue."} },
 {-46,
  {"CL_INVALID_KERNEL_NAME",
   "clCreateKernel",
   "if kernel_name is not found in program."
  } },
 {-47,
  {"CL_INVALID_KERNEL_DEFINITION",
   "clCreateKernel",
   "if the function definition for __kernel function given by kernel_name such as the number of\n"
   "arguments, the argument types are not the same for all devices for which the program\n"
   "executable has been built."
  } },
 {-48,
  {"CL_INVALID_KERNEL",
   "",
   "if kernel is not a valid kernel object."
  } },
 {-49,
  {"CL_INVALID_ARG_INDEX",
   "clSetKernelArg, clGetKernelArgInfo",
   "if arg_index is not a valid argument index."
  } },
 {-50,
  {"CL_INVALID_ARG_VALUE",
   "clSetKernelArg, clGetKernelArgInfo",
   "if arg_value specified is not a valid value."
  } },
 {-51,
  {"CL_INVALID_ARG_SIZE",
   "clSetKernelArg",
   "if arg_size does not match the size of the data type for an argument that is not a memory\n"
   "object or if the argument is a memory object and arg_size != sizeof(cl_mem), or if arg_size\n"
   "is zero and theargument is declared with the __local qualifier or if the argument is a\n"
   "sampler and arg_size != sizeof(cl_sampler)."
  } },
 {-52,
  {"CL_INVALID_KERNEL_ARGS",
   "",
   "if the kernel argument values have not been specified."
  } },
 {-53,
  {"CL_INVALID_WORK_DIMENSION",
   "",
   "if work_dim is not a valid value (i.e. a value between 1 and 3)."
  } },
 {-54,
  {"CL_INVALID_WORK_GROUP_SIZE",
   "",
   "if local_work_size is specified and number of work-items specified by global_work_size is\n"
   "not evenly divisable by size of work-group given by local_work_size or does not match the\n"
   "work-group size specified for kernel using the __attribute__((reqd_work_group_size(X, Y, Z)))\n"
   "qualifier in program source;\n"
   "if local_work_size is specified and the total number of work-items in the work-group computed\n"
   "as local_work_size[0] * … * local_work_size[work_dim - 1] is greater than the value specified\n"
   "by CL_DEVICE_MAX_WORK_GROUP_SIZE in the table of OpenCL Device Queries for clGetDeviceInfo;\n"
   "if local_work_size is NULL and the __attribute__((reqd_work_group_size(X, Y, Z))) qualifier\n"
   "is used to declare the work-group size for kernel in the program source."
  } },
 {-55,
  {"CL_INVALID_WORK_ITEM_SIZE",
   "",
   "if the number of work-items specified in any of\n"
   "  local_work_size[0], …, local_work_size[work_dim - 1]\n"
   "is greater than the corresponding values specified by\n"
   "  CL_DEVICE_MAX_WORK_ITEM_SIZES[0], …, CL_DEVICE_MAX_WORK_ITEM_SIZES[work_dim - 1]."
  } },
 {-56,
  {"CL_INVALID_GLOBAL_OFFSET",
   "",
   "if the value specified in global_work_size + the corresponding values in global_work_offset\n"
   "for anydimensions is greater than the sizeof(size_t) for the device on which the kernel\n"
   "execution will be enqueued."
  } },
 {-57,
  {"CL_INVALID_EVENT_WAIT_LIST",
   "",
   "if event_wait_list is NULL and num_events_in_wait_list > 0, or event_wait_list is not NULL\n"
   "and num_events_in_wait_list is 0, or if event objects in event_wait_list are not valid events."
  } },
 {-58,
  {"CL_INVALID_EVENT",
   "",
   "if event objects specified in event_list are not valid event objects."
  } },
 {-59,
  {"CL_INVALID_OPERATION",
   "",
   "if interoperability is specified by setting CL_CONTEXT_ADAPTER_D3D9_KHR,\n"
   "CL_CONTEXT_ADAPTER_D3D9EX_KHR or CL_CONTEXT_ADAPTER_DXVA_KHR to a non-NULL value, and\n"
   "interoperability with another graphics API is also specified. (only if the\n"
   "cl_khr_dx9_media_sharing extension is supported)."
  } },
 {-60,
  {"CL_INVALID_GL_OBJECT",
   "",
   "if texture is not a GL texture object whose type matches texture_target, if the specified\n"
   "miplevel of texture is not defined, or if the width or height of the specified miplevel is\n"
   "zero."
  } },
 {-61,
  {"CL_INVALID_BUFFER_SIZE",
   "clCreateBuffer, clCreateSubBuffer",
   "if size is 0. Implementations may return CL_INVALID_BUFFER_SIZE if size is greater than the\n"
   "CL_DEVICE_MAX_MEM_ALLOC_SIZE value specified in the table of allowed values for param_name\n"
   "for clGetDeviceInfo for all devices in context."
  } },
 {-62,
  {"CL_INVALID_MIP_LEVEL",
   "OpenGL-functions",
   "if miplevel is greater than zero and the OpenGL implementation does not support creating\n"
   "from non-zero mipmap levels."
  } },
 {-63,
  {"CL_INVALID_GLOBAL_WORK_SIZE",
   "",
   "if global_work_size is NULL, or if any of the values specified in global_work_size[0], …,\n"
   "global_work_size [work_dim - 1] are 0 or exceed the range given by the sizeof(size_t) for\n"
   "the device on which the kernel execution will be enqueued."
  } },
 {-64,
  {"CL_INVALID_PROPERTY",
   "clCreateContext",
   "Vague error, depends on the function"
  } },
 {-65,
  {"CL_INVALID_IMAGE_DESCRIPTOR",
   "clCreateImage",
   "if values specified in image_desc are not valid or if image_desc is NULL."
  } },
 {-66,
  {"CL_INVALID_COMPILER_OPTIONS",
   "clCompileProgram",
   "if the compiler options specified by options are invalid."
  } },
 {-67,
  {"CL_INVALID_LINKER_OPTIONS",
   "clLinkProgram",
   "if the linker options specified by options are invalid."
  } },
 {-68,
  {"CL_INVALID_DEVICE_PARTITION_COUNT",
   "clCreateSubDevices",
   "if the partition name specified in properties is CL_DEVICE_PARTITION_BY_COUNTS and the\n"
   "number of sub-devices requested exceeds CL_DEVICE_PARTITION_MAX_SUB_DEVICES or the total\n"
   "number of compute units requested exceeds CL_DEVICE_PARTITION_MAX_COMPUTE_UNITS for\n"
   "in_device, or the number of compute units requested for one or more sub-devices is less\n"
   "than zero or the number of sub-devices requested exceeds\n"
   "CL_DEVICE_PARTITION_MAX_COMPUTE_UNITS for in_device."
  } },
 {-1000,
  {"CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR",
   "clGetGLContextInfoKHR, clCreateContext",
   "CL and GL not on the same device {only when using a GPU)."
  } },
 {-1001,
  {"",
   "clGetPlatform",
   "No valid ICDs found"
  } },
 {-1002,
  {"CL_INVALID_D3D10_DEVICE_KHR",
   "clCreateContext, clCreateContextFromType",
   "if the Direct3D 10 device specified for interoperability is not compatible with the devices\n"
   "against which the context is to be created."
  } },
 {-1003,
  {"CL_INVALID_D3D10_RESOURCE_KHR",
   "clCreateFromD3D10BufferKHR, clCreateFromD3D10Texture2DKHR, clCreateFromD3D10Texture3DKHR",
   "If the resource is not a Direct3D 10 buffer or texture object"
  } },
 {-1004,
  {"CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR",
   "clEnqueueAcquireD3D10ObjectsKHR",
   "If a mem_object is already acquired by OpenCL"
  } },
 {-1005,
  {"CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR",
   "clEnqueueReleaseD3D10ObjectsKHR",
   "If a mem_object is not acquired by OpenCL"
  } }
};

void print_opencl_error_code(cl_int error) {
	std::map<cl_int, std::array<const char*, 3>>::iterator it = OpenCL_ErrorCodes.find(error);
	if(it == OpenCL_ErrorCodes.end()) {
		std::cerr << "UNKNOWN ERROR!! (" << error << ")" << std::endl;
		return;
	}

	std::cerr << "Error name.: " << it->second[0] << std::endl;
	std::cerr << "Functions..: " << it->second[1] << std::endl;
	std::cerr << "Description: " << it->second[2] << std::endl;
}
