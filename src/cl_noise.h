#ifndef __CL_NOISE_H__
#define __CL_NOISE_H__

#include <CL/cl.hpp>
#include "cl_matrix_dense.h"

template<typename I, typename D>
class ClNoiseCw {
	cl::Program p_prog;
	cl::Kernel k_compute_partial;
	cl::Kernel k_compute_noise;
	I tw, th, iw, ih, ow, oh;

 public:
	ClNoiseCw(cl::Context& context,
	        cl::Device& device);

	void bind_in(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void bind_tmp(const ClMatrixDense<I, D>&);
	void run(I size, I shift, cl::CommandQueue& queue);
};

template<typename I, typename D>
class ClNoiseRw {
	cl::Program p_prog;
	cl::Kernel k_compute_partial;
	cl::Kernel k_compute_noise;
	cl::Kernel k_compute_smart;
	cl::Kernel k_compute_smart_nocheck;
	I tw, th, iw, ih, ow, oh;

 public:
	ClNoiseRw(cl::Context& context,
	          cl::Device& device,
	          bool mod1);

	void bind_in(const ClMatrixDense<I, D>&);
	void bind_out(const ClMatrixDense<I, D>&);
	void bind_tmp(const ClMatrixDense<I, D>&);
	void run(I size, I shift, cl::CommandQueue& queue);
};

#endif //__CL_NOISE_H__

// Local Variables:
// mode: c++
// End:
