
#include "scalar.h"

template<>
const char* Scalar<float>::Tag = "f";

template<>
const char* Scalar<float>::Flags = "-DDOUBLE=0";

template<>
const char* Scalar<double>::Tag = "d";

template<>
const char* Scalar<double>::Flags = "-DDOUBLE=1";

template<>
const char* Scalar<long double>::Tag = "e";
