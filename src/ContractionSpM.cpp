
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <system_error>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <inttypes.h>
#include <boost/numeric/interval/hw_rounding.hpp>
#include <boost/filesystem.hpp>
#include <glog/logging.h>
#define __CL_ENABLE_EXCEPTIONS
#include <limits>
#include "exceptions.h"
#include "utils.h"
#include "matrix.h"
#include "mmap.h"
#include "scalar.h"
#ifndef CPU_ONLY
#include <CL/cl.hpp>
#include "cl_basis_init.h"
#include "cl_noise.h"
#include "cl_norm.h"
#include "cl_matrix_csr.h"
#include "cl_utils.h"
#include "cl_pretty_print.h"
#endif

using namespace std;

#define DEFAULT_BASIS_TYPE Basis_N_Np1
#define DEFAULT_NORM_TYPE Norm_L1
#define DEFAULT_ALPHA 0.5
#define DEFAULT_NUM_ITERATIONS_HARD_LIMIT 1000
#define DEFAULT_NUM_VECTORS 8
#define DEFAULT_OUT_FILE "out.txt"

template<typename D>
bool all_small_eq(D *d1, D *d2, uint32_t num) {
	for(uint32_t i = 0; i<num; i++)
		if(d1[i] > d2[i])
			return false;
	return true;
}

class ContractionSpMArgs {
public:
	enum Prec {
		Float = 1,
		Double = 2,
		Extended = 3
	};
	enum BasisType {
		Basis_N_Np1 = 0,
		Basis_0_Np1 = 1,
		Basis_Noise_Gap = 2
	};
	enum NormType {
		Norm_L1 = 0,
		Norm_Linf = 1
	};
	bool noise_mod1 = 0;
	int noise = 0;
	int noise_shift = 0;
	int platform_index = 0;
	int device_index = 0;
    const char* matrix_file = NULL;
	Prec precision = Float;
	BasisType basis_type = DEFAULT_BASIS_TYPE;
	const char* basis_file_path = NULL;
	NormType norm_type = DEFAULT_NORM_TYPE;
	double target_alpha = DEFAULT_ALPHA;
	uint32_t fixed_num_of_iterations = 0;
	uint32_t basis_start_element = 0;
	double step_error = -1;
    uint32_t num_iterations_hard_limit = DEFAULT_NUM_ITERATIONS_HARD_LIMIT;
	uint32_t num_vecs = DEFAULT_NUM_VECTORS;
    bool is_binary = false;
    bool show_help = false;
    bool run_test = false;
    bool use_cpu = false;
	bool start_with_only_noise = false;
	bool allow_quick_contraction_check = false;
	const char* power_norms_file = NULL;
	const char* basis_norms_collection_file = NULL;
	double stop_at_norm = -1;

	bool init(int argc, char* argv[]);
};

vector<string> base_type_names = {"Basis_N_Np1", "Basis_0_Np1", "Basis_Noise_Gap"};
vector<string> norm_type_names = {"Norm_L1", "Norm_Linf"};

string join(const vector<string>& names) {
	ostringstream os;
	for(unsigned int i = 0; i<names.size(); i++) {
		if(i>0) os << ",";
		os << names[i];
	}
	return os.str();
}

template<typename E>
E get_enum_choice(const vector<string>& names, string name) {
	for(unsigned int i = 0; i<names.size(); i++)
		if(name == names[i])
			return (E)i;
	throw UsageException("invalid %s, should be one of %s!",
	                     name.c_str(), join(names).c_str());
}

bool ContractionSpMArgs::init(int argc, char* argv[]) {

    for(int i = 1; i < argc; i++) {
	    if(!strcmp(argv[i], "-f") && i < argc-1) {
		    i++;
		    if(!strcasecmp(argv[i],"f"))
			    precision = Float;
		    else if(!strcasecmp(argv[i],"d"))
			    precision = Double;
		    else if(!strcasecmp(argv[i],"e"))
			    precision = Extended;
		    else {
			    cout << "Precision one must be one of f=float, d=double, e=long double" << endl;
			    return false;
		    }
	    }
	    else if(!strcmp(argv[i], "-p") && i < argc-1)
	        platform_index = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-d") && i < argc-1)
	        device_index = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-m") && i < argc-1) {
            matrix_file = argv[++i];
            is_binary = false;
        }
        else if(!strcmp(argv[i], "-b") && i < argc-1) {
            matrix_file = argv[++i];
            is_binary = true;
        }
        else if(!strcmp(argv[i], "-l") && i < argc-1)
            num_iterations_hard_limit = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-B") && i < argc-1)
	        basis_type = get_enum_choice<BasisType>(base_type_names, string(argv[++i]));
        else if(!strcmp(argv[i], "-basis-file-path") && i < argc-1)
	        basis_file_path = argv[++i];
        else if(!strcmp(argv[i], "-N") && i < argc-1)
	        norm_type = get_enum_choice<NormType>(norm_type_names, string(argv[++i]));
        else if(!strcmp(argv[i], "-F") && i < argc-1)
            fixed_num_of_iterations = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-basis-start-el") && i < argc-1)
	        basis_start_element = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-r") && i < argc-1)
            noise = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-noise-shift") && i < argc-1)
            noise_shift = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-V") && i < argc-1)
            num_vecs = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-a") && i < argc-1)
	        target_alpha = strtod(argv[++i], NULL);
        else if(!strcmp(argv[i], "-s") && i < argc-1)
	        step_error = strtod(argv[++i], NULL);
        else if(!strcmp(argv[i], "-start-with-only-noise"))
            start_with_only_noise = true;
        else if(!strcmp(argv[i], "-power-norms-file") && i < argc-1)
	        power_norms_file = argv[++i];
        else if(!strcmp(argv[i], "-basis-norms-collection-file") && i < argc-1)
	        basis_norms_collection_file = argv[++i];
        else if(!strcmp(argv[i], "-allow-quick-contraction-check"))
	        allow_quick_contraction_check = true;
        else if(!strcmp(argv[i], "-stop-at-norm") && i < argc-1)
	        stop_at_norm = strtod(argv[++i], NULL);
        else if(!strcmp(argv[i], "-noise-mod-1"))
            noise_mod1 = true;
        else if(!strcmp(argv[i], "-c"))
            use_cpu = true;
        else if(!strcmp(argv[i], "-h"))
            show_help = true;
        else if(!strcmp(argv[i], "-t"))
            run_test = true;
        else
	        throw BasicException(string("Argument not understood: ")+argv[i]);
    }

    if(show_help) {
	    cout << "Usage: ContractionSpM -s steperr -b|-m matrix_file [optional args]" << endl
	         << "    -s steperr     Step error (may depend on the precision of the matrix)" << endl
	         << "    -m matrix.mkt  Loads a matrix in MatrixMarket format" << endl
	         << "    -b matrix.bin  Loads a matrix in binary COO format" << endl
	         << "    -a alpha       Target alpha" << endl
             << "    -l num         Hard limit number of iterations (default:"
             << DEFAULT_NUM_ITERATIONS_HARD_LIMIT << ")" << endl
             << "    -V num         Vectors to iterate simultaneously (default:"
	         << DEFAULT_NUM_VECTORS << ")" << endl
	         << "    -F num         Iterate a fixed num of iteration, computing C_i" << endl
	         << "    -basis-file-path file        Load basis vectors from file" << endl
             << "    -B             Basis type, one of " << join(base_type_names) << endl
             << "    -N             Norm type, one of " << join(norm_type_names) << endl
             << "    -r size        Apply random noise (default: 0=disabled)" << endl
             << "    -noise-shift sz  Asymmetric noise shift (default: 0=disabled)" << endl
             << endl
             << "    -noise-mod-1   Make the noise cyclic" << endl
             << "    -c             Run on the CPU only (using binary format, the matrix" << endl
	         << "    -power-norms-file File to save the norms of powers (the C_i)" << endl
	         << "    -basis-norms-collection-file File to save norms of M^i*bj for basis bj"
	         << endl
	         << "    -start-with-only-noise First iteration, apply only the noise" << endl
	         << "    -basis-start-el el Basis start element" << endl
	         << "    -allow-quick-contraction-check  Allow quick exit when finding small norm" << endl
             << "                   is mmap'ed, so this is suitable for huge matrices)" << endl
             << "    -p n           The index of the OpenCl platform to use" << endl
             << "    -d n           The index of the platform's device to use" << endl
             << "    -f f|d|e       The float precision (f=float, d=double, e=long double)" << endl
             << "    -t             Run test, if in GPU mode" << endl
             << "    -h             Show this help" << endl;
        return false;
    }

    if(!matrix_file)
	    cout << "Specifying one matrix with -m or -b is mandatory." << endl;
    else if(step_error<=0)
	    cout << "Specifying one (positive) step error with -s is mandatory." << endl;
    if(!matrix_file || step_error<=0) {
	    cout << "Run with -h for help" << endl;
	    return false;
    }

    return true;
}

template<typename D>
class ColReader {
public:
	uint32_t num_vecs;
	uint32_t num_vals;
	uint32_t *idx;
	uint32_t *rows;
	D *vals;
	uint32_t *refs; //for each column, some "origin" information
	uint32_t current = 0;

	ColReader(const char* filename, bool have_refs);

	~ColReader();

	uint32_t read_columns(MatrixDense<uint32_t, D>& mat,
	                      vector<uint32_t>* col_refs);

#ifndef CPU_ONLY
	uint32_t read_columns(MatrixDense<uint32_t, D>& mat,
                          ClMatrixDense<uint32_t, D>& matCL,
	                      cl::CommandQueue& queue,
	                      vector<uint32_t>* col_refs);
#endif
};

template<typename D>
ColReader<D>::ColReader(const char* filename, bool have_refs) {
	ifstream m(filename);

	string line, tag;
	if(!getline(m, line))
		throw ParseException("Empty file?");
	stringstream ss(line);
	ss >> num_vecs >> num_vals >> tag;
	if(tag != Scalar<D>::Tag)
		throw ParseException("Wrong precision loading matrix, got '%s', expected '%s'",
		                     tag.c_str(), Scalar<D>::Tag);

	uint64_t isize, rsize, vsize, fsize;
	idx = (uint32_t*)MMap::map_file((filename+string(".idx")).c_str(), &isize);
	rows = (uint32_t*)MMap::map_file((filename+string(".rows")).c_str(), &rsize);
	vals = (D*)MMap::map_file((filename+string(".vals")).c_str(), &vsize);
	if(have_refs) {
		refs = (uint32_t*)MMap::map_file((filename+string(".refs")).c_str(), &fsize);
	}
	else {
		refs = NULL;
		fsize = 0;
	}
  
	if(!idx
	   || !rows
	   || !vals
	   || (have_refs && !refs)
	   || isize!=(num_vecs+1)*sizeof(uint32_t)
	   || rsize!=num_vals*sizeof(uint32_t)
	   || vsize!=num_vals*sizeof(D)
	   || (have_refs && fsize!=num_vecs*sizeof(uint32_t))) {
		if(idx) MMap::unmap_file(idx, isize);
		if(rows) MMap::unmap_file(rows, rsize);
		if(vals) MMap::unmap_file(vals, vsize);
		if(refs) MMap::unmap_file(refs, fsize);
		const char* error = !idx ? "Couldn't load idx" :
			!rows ? "Couldn't load rows" :
			!vals ? "Couldn't load values" :
			(have_refs && !refs) ? "Couldn't load refs" :
			isize!=(num_vecs+1)*sizeof(uint32_t) ? "Wrong idx file size" :
			rsize!=num_vals*sizeof(uint32_t) ? "Wrong rows file size" :
			vsize!=num_vals*sizeof(D) ? "Wrong values file size" :
			"Wrong refs file size";
		idx = NULL;
		rows = NULL;
		vals = NULL;
		refs = NULL;
		throw BasicException(string(error));
	}
}

template<typename D>
uint32_t ColReader<D>::read_columns(MatrixDense<uint32_t, D>& mat,
                                    vector<uint32_t>* col_refs) {
	
	mat.set(0);
	uint32_t num_read_cols = 0;
	for(uint32_t i = 0; i< mat.width; i++) {
		if(current >= num_vecs)
			break;

		uint32_t start = idx[current],
			end = idx[current+1];

		if(refs && col_refs)
			(*col_refs)[i] = refs[current];
		
		for(uint32_t q = start; q < end; ++q)
			mat.data[i + mat.width * rows[q]] = vals[q];
		
		num_read_cols += 1;
		current += 1;
	}
	return num_read_cols;
}

#ifndef CPU_ONLY
template<typename D>
uint32_t ColReader<D>::read_columns(MatrixDense<uint32_t, D>& mat,
                                    ClMatrixDense<uint32_t, D>& matCL,
                                    cl::CommandQueue& queue,
                                    vector<uint32_t>* col_refs) {
	uint32_t num_read_cols = read_columns(mat, col_refs);
	matCL.write(mat, queue);
	return num_read_cols;
}
#endif

template<typename D>
ColReader<D>::~ColReader() {
	MMap::unmap_file(idx, (num_vecs+1)*sizeof(uint32_t));
	MMap::unmap_file(rows, num_vals*sizeof(uint32_t));
	MMap::unmap_file(vals, num_vals*sizeof(D));
	idx = NULL;
	rows = NULL;
	vals = NULL;
}

#ifndef CPU_ONLY
template<typename D>
void iterate_matrix_cl(const ContractionSpMArgs& a, cl::Device device) {

	cout.precision(numeric_limits<D>::max_digits10);
 
    MatrixCoo<uint32_t, D> coo_mat;
    MatrixCsr<uint32_t, D> csr_mat;

    // load main matrix
    if(a.is_binary)
        load_binary_matrix(a.matrix_file, coo_mat);
    else
        load_matrix_market(a.matrix_file, coo_mat);

    if(coo_mat.height != coo_mat.width)
	    throw UsageException("Matrix is not a square matrix!");
    uint32_t size = coo_mat.height;

    // convert to csr format
    MoveCoo2Csr(coo_mat, csr_mat);

    // input-output matrix
    MatrixDense<uint32_t, D> in(size, a.num_vecs);
    MatrixDense<uint32_t, D> out(size, a.num_vecs);
    MatrixDense<uint32_t, D> norms(1, a.num_vecs);
  
    // create context
    cl::Context context(device);
        
    //create queue to which we will push commands for the device.
    cl::CommandQueue queue(context, device);

    // create cl matrix objects
    ClMatrixDense<uint32_t, D> vsCurrentCL(size, a.num_vecs, context);
    ClMatrixDense<uint32_t, D> vsOutputCL(size, a.num_vecs, context);
    ClMatrixDense<uint32_t, D> vNormsCL(1, a.num_vecs, context);
    ClMatrixCsr<uint32_t, D> matCsrCL(csr_mat, context, queue);
    
    // load multiplier kernel
    ClMultiplierCsrRw<uint32_t, D> kMultRwCL(context, device);
    kMultRwCL.bind_mat(matCsrCL);
    if(a.run_test) {
	    kMultRwCL.bind_in(vsCurrentCL);
	    kMultRwCL.bind_out(vsOutputCL);
	    kMultRwCL.run(queue); //one run to make sure everything is loaded
	    queue.finish();
    }
    
    ClNoiseRw<uint32_t, D> kNoiseRwCL(context, device, a.noise_mod1);

    ClBasisInitRw<uint32_t, D> kInitBasisCL(context, device, a.noise_mod1);
    ColReader<D>* col_reader = NULL;
    if(a.basis_file_path)
	    col_reader = new ColReader<D>(a.basis_file_path, true);

    ClNormRw<uint32_t, D> kNormRwCL(context, device);
    kNormRwCL.bind_out(vNormsCL);

    // loop on the gpu
    Timer timer;
    double flops = 0;
    uint32_t current_basis_index = a.basis_start_element;
    uint32_t stop_at = a.basis_start_element;
    uint32_t current_num_iterations = 1; // we always hope contracts in 1 step
    if(a.fixed_num_of_iterations)
	    current_num_iterations = a.fixed_num_of_iterations;
    vector<D> Ci(current_num_iterations);
    vector<D> *basis_norms = NULL;
    if(a.basis_norms_collection_file)
	    basis_norms = new vector<D>(a.fixed_num_of_iterations * size);
    vector<uint32_t> *basis_index_refs = NULL;
    if(a.basis_file_path)
	    basis_index_refs = new vector<uint32_t>(a.num_vecs);

    // loop increasing the number of iterations when necessary
    while(true) {
	    // init some basis vectors
	    uint32_t ncols_set = 0;
	    kInitBasisCL.bind_mat(vsCurrentCL);
	    if(a.basis_file_path)
		    ncols_set = col_reader->read_columns(in, vsCurrentCL, queue, basis_index_refs); 
	    else if(a.basis_type == ContractionSpMArgs::Basis_N_Np1)
		    ncols_set = kInitBasisCL.init_N_Np1(current_basis_index, queue);
	    else if(a.basis_type == ContractionSpMArgs::Basis_0_Np1)
		    ncols_set = kInitBasisCL.init_0_Np1(current_basis_index, queue);
	    else if(a.basis_type == ContractionSpMArgs::Basis_Noise_Gap)
		    ncols_set = kInitBasisCL.init_noise_gap(current_basis_index, a.noise/2, queue);
	    else
		    throw BasicException(string("Irregular basis type?"));
	    if(ncols_set == 0) {
		    current_basis_index = 0;
		    if(stop_at == 0)
			    break;
		    else
			    continue;
	    }
	    cout << "Basis: " << current_basis_index << "..."
	         << (ncols_set + current_basis_index - 1) << endl;

	    double error = 0;
	    uint32_t num_iterations = 0;
	    bool all_norms_smaller_equal = true;

	    // count for the error
	    {
		    boost::numeric::interval_lib::rounded_math<D> rnd;
		    if(a.basis_type == ContractionSpMArgs::Basis_Noise_Gap ||
		       a.basis_file_path)
			    error = a.step_error;
		    else
			    error = rnd.mul_up(a.step_error, 2);		    
	    }

	    while(true) {
		    if(num_iterations != 0 || (!a.start_with_only_noise)) {
			    // apply the matrix
			    queue.finish();
			    kMultRwCL.bind_in(vsCurrentCL);
			    kMultRwCL.bind_out(vsOutputCL);
			    kMultRwCL.run(queue);
			    flops += 2 * csr_mat.nnz * a.num_vecs;
		    }
		    else
			    swap(vsCurrentCL, vsOutputCL);

		    if(a.noise) {
			    // apply the noise, if any
			    queue.finish();
			    kNoiseRwCL.bind_in(vsOutputCL);
			    kNoiseRwCL.bind_out(vsCurrentCL);
			    kNoiseRwCL.run(a.noise, a.noise_shift, queue);
			    flops += 6 * size * a.num_vecs;			    
		    }
		    else
			    swap(vsCurrentCL, vsOutputCL);
 
		    // compute norms
		    kNormRwCL.bind_mat(vsCurrentCL);
		    if(a.norm_type == ContractionSpMArgs::Norm_L1)
			    kNormRwCL.compute_L1(queue);
		    else if(a.norm_type == ContractionSpMArgs::Norm_Linf)
			    kNormRwCL.compute_Linf(queue);
		    flops += csr_mat.width * a.num_vecs;

		    // get norms and max them
		    vNormsCL.read(norms, queue);
		    D current_norm = 0, current_norm_plus_error;
		    for(uint32_t j = 0; j<ncols_set; j++)
			    current_norm = max(current_norm, norms.data[j]);
		    
		    // correctly compute error for NOISE too (unless included in step_error)
		    {
			    boost::numeric::interval_lib::rounded_math<D> rnd;
			    current_norm_plus_error = rnd.add_up(current_norm, error);
			    error = rnd.add_up(error, rnd.mul_up(a.step_error, current_norm));
		    }

		    bool quick_exit = current_norm_plus_error < a.stop_at_norm;

		    if(current_norm_plus_error > 10000) {
			    cout << "WHAT?!?!?!?" << endl;
			    cout << "it = " << num_iterations << "; norm = " <<
				    current_norm_plus_error << endl;
			    return;
		    }
		    
		    if(a.basis_norms_collection_file) {
			    for(uint32_t j = 0; j<ncols_set; j++) {
				    boost::numeric::interval_lib::rounded_math<D> rnd;
				    D norm_plus_error = rnd.add_up(norms.data[j], error);
				    uint32_t offset = (basis_index_refs
				                ? (*basis_index_refs)[j]
				                : current_basis_index + j) * a.fixed_num_of_iterations;
				    uint32_t set_max = quick_exit
					    ? a.fixed_num_of_iterations
					    : num_iterations+1;
				    for(uint32_t q = num_iterations; q < set_max; q++)
					    (*basis_norms)[offset + q] =
						    max( (*basis_norms)[offset + q], norm_plus_error);
			    }
		    }

		    if(quick_exit) {
			    cout << "Quick exit at " << num_iterations << endl << "Ci: ";
			    for(uint32_t r = 0; r<num_iterations; ++r)
				    cout << " " << Ci[r];
			    cout << " [" << current_norm_plus_error << "]" << endl;
			    break;
		    }

		    //cut a long story short...
		    if(current_norm_plus_error > Ci[num_iterations])
			    all_norms_smaller_equal = false;
		    if(a.norm_type == ContractionSpMArgs::Norm_L1 &&
		       num_iterations+1 < a.fixed_num_of_iterations &&
		       a.fixed_num_of_iterations &&
		       a.allow_quick_contraction_check &&
		       all_norms_smaller_equal &&
		       current_norm_plus_error <= Ci[a.fixed_num_of_iterations-1]) {
			    cout << "Already contracting! After " <<
				    (num_iterations+1) << " steps:" << endl;			    
			    cout << "Ci[0.."<<num_iterations<<"]:";
			    for(uint32_t i = 0; i < num_iterations; ++i)
				    cout << " " << Ci[i];
			    cout << " [" << current_norm_plus_error << "]" << endl;

			    D sum = 0;
			    for(uint32_t i = 0; i < current_num_iterations; ++i)
				    sum += Ci[i];
			    cout << endl << "Ci-Sum: " << sum << endl;
			    cout << "Elapsed time: " <<
				    timer.elapsed_microsec()/1000000.0 << endl;
			    double gpu_microseconds = timer.elapsed_microsec();
			    cout << "GPU Gflops: " << (flops/(gpu_microseconds*1000)) << endl;
			    
			    cout << endl;
			    break;
		    }

		    // Ci become the new maximum
		    Ci[num_iterations] = max(Ci[num_iterations], current_norm_plus_error);
		    
		    ++num_iterations;
		    if(num_iterations < current_num_iterations)
			    continue;

		    //always respect fixed num of iterations
		    if(a.fixed_num_of_iterations) {
			    cout << "Ci:";
			    D sum = 0;
			    for(uint32_t i = 0; i < current_num_iterations; ++i) {
				    cout << " " << Ci[i];
				    sum += Ci[i];
				}
			    cout << endl << "Ci-Sum: " << sum << endl;
			    cout << "Elapsed time: " <<
				    timer.elapsed_microsec()/1000000.0 << endl;
			    double gpu_microseconds = timer.elapsed_microsec();
			    cout << "GPU Gflops: " << (flops/(gpu_microseconds*1000)) << endl;
			    break;
		    }

		    cout << "C_Rate["<<num_iterations<<"]: " << current_norm << " <> "
		         << (a.target_alpha-error)<< endl;
		    
		    if((current_norm_plus_error < a.target_alpha) ||
		       (num_iterations > a.num_iterations_hard_limit))
			    break;

		    cout << "Increasing num iterations to " << (num_iterations+1) << endl;
	    }

	    // we increased the max number of iterations!
	    if(num_iterations > current_num_iterations) {
		    current_num_iterations = num_iterations;
		    Ci.resize(current_num_iterations);
		    stop_at = current_basis_index; //need to recompute all, again
	    }

	    if(num_iterations > a.num_iterations_hard_limit) {
		    cout << endl << "Hard Limit hit!!" << endl;
		    break;
	    }

	    //continue cyclically
	    current_basis_index = current_basis_index + a.num_vecs;

	    if(!a.basis_file_path) {
		    if(current_basis_index == stop_at)
			    break;
	    }

	    //if(current_basis_index > 100) break;
    }

    // save all norms
    if(a.basis_norms_collection_file) {
	    cout << "Writing " << a.basis_norms_collection_file << endl;
	    ofstream out(a.basis_norms_collection_file);
	    out.write((char*)basis_norms->data(), basis_norms->size() * sizeof(D));
    }

    // print final Ci information
    if(a.fixed_num_of_iterations) {
	    cout << "FinalCi:";
	    D sum = 0;
	    for(uint32_t i = 0; i < current_num_iterations; ++i) {
		    cout << " " << Ci[i];
		    sum += Ci[i];
	    }
	    cout << endl << "Final-Ci-Sum: " << sum << endl;

	    // save the Ci array to a binary file
	    if(a.power_norms_file) {
		    cout << "Writing norms to file " << a.power_norms_file << endl;
		    ofstream out(a.power_norms_file);
		    out.write((char*)Ci.data(), Ci.size() * sizeof(D));
	    }
    }

    queue.finish();
    if(current_num_iterations <= a.num_iterations_hard_limit &&
       !a.fixed_num_of_iterations)
	    cout << "CONTRACTION IN " << current_num_iterations << " STEPS" << endl;

    double gpu_microseconds = timer.elapsed_microsec();

    cout << endl;
    cout << "GPU Time: " << (gpu_microseconds/1000000.0) << " seconds" << endl;
    cout << "GPU Gflops: " << (flops/(gpu_microseconds*1000)) << endl;

    /* done on the cpu */
    if(a.run_test) {
    }
}
#endif

template<typename D>
void iterate_matrix_cpu(const ContractionSpMArgs& a) {

	cout.precision(numeric_limits<D>::max_digits10);
 
    MatrixCoo<uint32_t, D> mCoo;

    // load main matrix
    if(a.is_binary)
        load_binary_matrix(a.matrix_file, mCoo);
    else
        load_matrix_market(a.matrix_file, mCoo);

    if(mCoo.height != mCoo.width)
	    throw UsageException("Matrix is not a square matrix!");
    uint32_t size = mCoo.height;

    // input-output matrix
    MatrixDense<uint32_t, D> vsCurrent(size, a.num_vecs);
    MatrixDense<uint32_t, D> vsOutput(size, a.num_vecs);
    MatrixDense<uint32_t, D> vsNorms(1, a.num_vecs);
    MatrixDense<uint32_t, D> vsNoiseTmp(size*2, a.num_vecs);

    ColReader<D>* col_reader = NULL;
    if(a.basis_file_path)
	    col_reader = new ColReader<D>(a.basis_file_path, true);

    // loop on the gpu
    Timer timer;
    double flops = 0;
    uint32_t current_basis_index = a.basis_start_element;
    uint32_t stop_at = a.basis_start_element;
    uint32_t current_num_iterations = 1; // we always hope contracts in 1 step
    if(a.fixed_num_of_iterations)
	    current_num_iterations = a.fixed_num_of_iterations;
    vector<D> Ci(current_num_iterations);
    vector<D> *basis_norms = NULL;
    if(a.basis_norms_collection_file)
	    basis_norms = new vector<D>(a.fixed_num_of_iterations * size);
    vector<uint32_t> *basis_index_refs = NULL;
    if(a.basis_file_path)
	    basis_index_refs = new vector<uint32_t>(a.num_vecs);

    // loop increasing the number of iterations when necessary
    while(true) {
	    // init some basis vectors
	    uint32_t ncols_set = 0;
	    if(a.basis_file_path)
		    ncols_set = col_reader->read_columns(vsCurrent, basis_index_refs); 
	    else if(a.basis_type == ContractionSpMArgs::Basis_N_Np1)
		    ncols_set = vsCurrent.basis_N_Np1_rw(current_basis_index);
	    else if(a.basis_type == ContractionSpMArgs::Basis_0_Np1)
		    ncols_set = vsCurrent.basis_0_Np1_rw(current_basis_index);
	    else if(a.basis_type == ContractionSpMArgs::Basis_Noise_Gap) {
		    if(a.noise_mod1)
			    ncols_set = vsCurrent.basis_noiseM1_gap_rw(current_basis_index, a.noise/2);
		    else
			    ncols_set = vsCurrent.basis_noise_gap_rw(current_basis_index, a.noise/2);
	    }
	    else
		    throw BasicException(string("Irregular basis type?"));
	    if(ncols_set == 0) {
		    current_basis_index = 0;
		    if(stop_at == 0)
			    break;
		    else
			    continue;
	    }
	    cout << "Basis: " << current_basis_index << "..."
	         << (ncols_set + current_basis_index - 1) << endl;

	    double error = 0;
	    uint32_t num_iterations = 0;
	    bool all_norms_smaller_equal = true;

	    // count for the error
	    {
		    boost::numeric::interval_lib::rounded_math<D> rnd;
		    if(a.basis_type == ContractionSpMArgs::Basis_Noise_Gap ||
		       a.basis_file_path)
			    error = a.step_error;
		    else
			    error = rnd.mul_up(a.step_error, 2);		    
	    }

	    while(true) {
		    if(num_iterations != 0 || (!a.start_with_only_noise)) {
			    // apply the matrix
			    //queue.finish();
			    mCoo.apply_rw(vsCurrent, vsOutput);
			    flops += 2 * mCoo.nnz * a.num_vecs;
		    }
		    else
			    swap(vsCurrent, vsOutput);

		    if(a.noise) {
			    if(a.noise_mod1)
				    vsOutput.apply_noiseM1_rw(a.noise, a.noise_shift, vsCurrent, vsNoiseTmp);
			    else
				    vsOutput.apply_noise_rw(a.noise, a.noise_shift, vsCurrent, vsNoiseTmp);
			    flops += 6 * size * a.num_vecs;			    
		    }
		    else
		        swap(vsCurrent, vsOutput);
 
		    if(a.norm_type == ContractionSpMArgs::Norm_L1)
			    vsCurrent.norm_L1_rw(vsNorms);
		    else if(a.norm_type == ContractionSpMArgs::Norm_Linf)
			    vsCurrent.norm_Linf_rw(vsNorms);
		    flops += mCoo.width * a.num_vecs;
	
		    // get norms and max them
		    D current_norm = 0, current_norm_plus_error;
		    for(uint32_t j = 0; j<ncols_set; j++)
			    current_norm = max(current_norm, vsNorms.data[j]);
		    
		    // correctly compute error for NOISE too (unless included in step_error)
		    {
			    boost::numeric::interval_lib::rounded_math<D> rnd;
			    current_norm_plus_error = rnd.add_up(current_norm, error);
			    error = rnd.add_up(error, rnd.mul_up(a.step_error, current_norm));
		    }

		    bool quick_exit = current_norm_plus_error < a.stop_at_norm;

		    if(current_norm_plus_error > 10000) {
			    cout << "WHAT?!?!?!?" << endl;
			    cout << "it = " << num_iterations << "; norm = " <<
				    current_norm_plus_error << endl;
			    return;
		    }
		    
		    if(a.basis_norms_collection_file) {
			    for(uint32_t j = 0; j<ncols_set; j++) {
				    boost::numeric::interval_lib::rounded_math<D> rnd;
				    D norm_plus_error = rnd.add_up(vsNorms.data[j], error);
				    uint32_t offset = (basis_index_refs
				                ? (*basis_index_refs)[j]
				                : current_basis_index + j) * a.fixed_num_of_iterations;
				    uint32_t set_max = quick_exit
					    ? a.fixed_num_of_iterations
					    : num_iterations+1;
				    for(uint32_t q = num_iterations; q < set_max; q++)
					    (*basis_norms)[offset + q] =
						    max( (*basis_norms)[offset + q], norm_plus_error);
			    }
		    }

		    if(quick_exit) {
			    cout << "Quick exit at " << num_iterations << endl << "Ci: ";
			    for(uint32_t r = 0; r<num_iterations; ++r)
				    cout << " " << Ci[r];
			    cout << " [" << current_norm_plus_error << "]" << endl;
			    break;
		    }

		    //cut a long story short...
		    if(current_norm_plus_error > Ci[num_iterations])
			    all_norms_smaller_equal = false;
		    if(a.norm_type == ContractionSpMArgs::Norm_L1 &&
		       num_iterations+1 < a.fixed_num_of_iterations &&
		       a.fixed_num_of_iterations &&
		       a.allow_quick_contraction_check &&
		       all_norms_smaller_equal &&
		       current_norm_plus_error <= Ci[a.fixed_num_of_iterations-1]) {
			    cout << "Already contracting! After " <<
				    (num_iterations+1) << " steps:" << endl;			    
			    cout << "Ci[0.."<<num_iterations<<"]:";
			    for(uint32_t i = 0; i < num_iterations; ++i)
				    cout << " " << Ci[i];
			    cout << " [" << current_norm_plus_error << "]" << endl;

			    D sum = 0;
			    for(uint32_t i = 0; i < current_num_iterations; ++i)
				    sum += Ci[i];
			    cout << endl << "Ci-Sum: " << sum << endl;
			    cout << "Elapsed time: " <<
				    timer.elapsed_microsec()/1000000.0 << endl;
			    double gpu_microseconds = timer.elapsed_microsec();
			    cout << "GPU Gflops: " << (flops/(gpu_microseconds*1000)) << endl;
			    
			    cout << endl;
			    break;
		    }

		    // Ci become the new maximum
		    Ci[num_iterations] = max(Ci[num_iterations], current_norm_plus_error);
		    
		    ++num_iterations;
		    if(num_iterations < current_num_iterations)
			    continue;

		    //always respect fixed num of iterations
		    if(a.fixed_num_of_iterations) {
			    cout << "Ci:";
			    D sum = 0;
			    for(uint32_t i = 0; i < current_num_iterations; ++i) {
				    cout << " " << Ci[i];
				    sum += Ci[i];
				}
			    cout << endl << "Ci-Sum: " << sum << endl;
			    cout << "Elapsed time: " <<
				    timer.elapsed_microsec()/1000000.0 << endl;
			    double gpu_microseconds = timer.elapsed_microsec();
			    cout << "GPU Gflops: " << (flops/(gpu_microseconds*1000)) << endl;
			    break;
		    }

		    cout << "C_Rate["<<num_iterations<<"]: " << current_norm << " <> "
		         << (a.target_alpha-error)<< endl;
		    
		    if((current_norm_plus_error < a.target_alpha) ||
		       (num_iterations > a.num_iterations_hard_limit))
			    break;

		    cout << "Increasing num iterations to " << (num_iterations+1) << endl;
	    }

	    // we increased the max number of iterations!
	    if(num_iterations > current_num_iterations) {
		    current_num_iterations = num_iterations;
		    Ci.resize(current_num_iterations);
		    stop_at = current_basis_index; //need to recompute all, again
	    }

	    if(num_iterations > a.num_iterations_hard_limit) {
		    cout << endl << "Hard Limit hit!!" << endl;
		    break;
	    }

	    //continue cyclically
	    current_basis_index = current_basis_index + a.num_vecs;

	    if(!a.basis_file_path) {
		    if(current_basis_index == stop_at)
			    break;
	    }

	    //if(current_basis_index > 100) break;
    }

    // save all norms
    if(a.basis_norms_collection_file) {
	    cout << "Writing " << a.basis_norms_collection_file << endl;
	    ofstream out(a.basis_norms_collection_file);
	    out.write((char*)basis_norms->data(), basis_norms->size() * sizeof(D));
    }

    // print final Ci information
    if(a.fixed_num_of_iterations) {
	    cout << "FinalCi:";
	    D sum = 0;
	    for(uint32_t i = 0; i < current_num_iterations; ++i) {
		    cout << " " << Ci[i];
		    sum += Ci[i];
	    }
	    cout << endl << "Final-Ci-Sum: " << sum << endl;

	    // save the Ci array to a binary file
	    if(a.power_norms_file) {
		    cout << "Writing norms to file " << a.power_norms_file << endl;
		    ofstream out(a.power_norms_file);
		    out.write((char*)Ci.data(), Ci.size() * sizeof(D));
	    }
    }

    //queue.finish();
    if(current_num_iterations <= a.num_iterations_hard_limit &&
       !a.fixed_num_of_iterations)
	    cout << "CONTRACTION IN " << current_num_iterations << " STEPS" << endl;

    double gpu_microseconds = timer.elapsed_microsec();

    cout << endl;
    cout << "GPU Time: " << (gpu_microseconds/1000000.0) << " seconds" << endl;
    cout << "GPU Gflops: " << (flops/(gpu_microseconds*1000)) << endl;

    /* done on the cpu */
    if(a.run_test) {
    }
}

int main(int argc, char* argv[]) {
	    
    init_log(argv[0]);
    
    try {
	    ContractionSpMArgs a;
	    if(!a.init(argc, argv))
		    return 0;

	    if(a.use_cpu) {
		    if(a.precision == ContractionSpMArgs::Extended)
			    iterate_matrix_cpu<long double>(a);
		    else if(a.precision == ContractionSpMArgs::Double)
			    iterate_matrix_cpu<double>(a);
		    else
			    iterate_matrix_cpu<float>(a);
	    }
	    else {
#ifndef CPU_ONLY
		    if(a.precision == ContractionSpMArgs::Extended)
			    throw UsageException("long double precision not supported by OpenCl");
		    
		    cl::Platform platform = get_platform(a.platform_index);
		    cl::Device device = get_device(platform, CL_DEVICE_TYPE_ALL, a.device_index);

		    if(a.precision == ContractionSpMArgs::Double) {
			    if(device.getInfo<CL_DEVICE_EXTENSIONS>().find("cl_khr_fp64") ==
			           string::npos)
				    throw UsageException("double precision not supported by this device");
			    iterate_matrix_cl<double>(a, device);
		    }
		    else
			    iterate_matrix_cl<float>(a, device);
#else
		    std::cerr << "ERROR: GPU support not enabled at compile time." << std::endl;
		    return 1;
#endif
	    }
    }
#ifndef CPU_ONLY
    catch (cl::Error& err) {
        std::cerr << "ERROR: " << err.what() << "(" << err.err() << ")" << std::endl;
        print_opencl_error_code(err.err());
    }
 #endif
    catch (BasicException& e) {
        std::cerr << "Exception: " << e.message() << std::endl;
    }
 
    return 0;
}
