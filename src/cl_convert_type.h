#ifndef __CL_CONVERT_TYPE_H__
#define __CL_CONVERT_TYPE_H__

#include <CL/cl.hpp>
#include "cl_matrix_dense.h"

template<typename I, typename D>
class ClConvertType {
	cl::Program p_prog;
	cl::Kernel k_convert;

 public:
	ClConvertType(cl::Context& context,
	              cl::Device& device);
	void cw_to_rw(const ClMatrixDense<I, D>& in,
	              const ClMatrixDense<I, D>& out,
	              cl::CommandQueue& queue);
	void rw_to_cw(const ClMatrixDense<I, D>& in,
	              const ClMatrixDense<I, D>& out,
	              cl::CommandQueue& queue);
};

#endif //__CL_CONVERT_TYPE_H__

// Local Variables:
// mode: c++
// End:
