
#define __CL_ENABLE_EXCEPTIONS
#include <iostream>
#include "exceptions.h"
#include "cl_utils.h"
#include "scalar.h"
#include "cl_noise.h"

using namespace std;

template<typename I, typename D>
ClNoiseCw<I, D>::ClNoiseCw(cl::Context& context,
                       cl::Device& device) {
	throw "Don't use this class, broken!";
	p_prog = create_program(context, device,
	                        {"cl/noise.cl"},
	                        Scalar<D>::Flags);
	k_compute_partial = cl::Kernel(p_prog, "compute_partial_sums_cw_simple");
	k_compute_noise = cl::Kernel(p_prog, "compute_noise_cw_simple");
}

template<typename I, typename D>
void ClNoiseCw<I, D>::bind_in(const ClMatrixDense<I, D>& in) {
	ih = in.height;
	iw = in.width;

	k_compute_partial.setArg(0, ih);
	k_compute_partial.setArg(1, iw);
	k_compute_partial.setArg(5, in.data);
}

template<typename I, typename D>
void ClNoiseCw<I, D>::bind_out(const ClMatrixDense<I, D>& out) {
	oh = out.height;
	ow = out.width;

	k_compute_noise.setArg(0, oh);
	k_compute_noise.setArg(1, ow);
	k_compute_noise.setArg(7, out.data);

}

template<typename I, typename D>
void ClNoiseCw<I, D>::bind_tmp(const ClMatrixDense<I, D>& tmp) {
	th = tmp.height;
	tw = tmp.width;

	k_compute_partial.setArg(2, th);
	k_compute_partial.setArg(6, tmp.data);
	
	k_compute_noise.setArg(2, th);
	k_compute_noise.setArg(6, tmp.data);
}

template<typename I, typename D>
void ClNoiseCw<I, D>::run(I noise_size, I shift, cl::CommandQueue& queue) {
	I sec_size = noise_size - 1;
	I num_ker = (ih + sec_size - 1) / sec_size;
	I up_round_h = num_ker * sec_size;
	if(up_round_h != ih)
		throw UsageException("Noise size not multiple of ih, not implemented yet!");
	if(oh != ih || ow != iw || th < up_round_h || tw < 2*iw)
		throw UsageException("Noise wrong sizes: %d, in: %dx%d, tmp: %dx%d, out: %dx%d",
		                     noise_size, ih, iw, th, tw, oh, ow);
	if(shift > sec_size || shift < 0)
		throw UsageException("Shift %d negative or bigger than sec size %d",
		                     shift, sec_size);
	
	k_compute_partial.setArg(3, sec_size);
	k_compute_partial.setArg(4, shift);
	queue.enqueueNDRangeKernel(k_compute_partial,
	                           cl::NullRange,
	                           cl::NDRange(num_ker, iw),
	                           cl::NDRange(8, 8)
	                           );

	I left = sec_size / 2;
	I right = sec_size - left;
	D factor = D(1.0) / noise_size;

	k_compute_noise.setArg(3, left);
	k_compute_noise.setArg(4, right);
	k_compute_noise.setArg(5, factor);
	queue.enqueueNDRangeKernel(k_compute_noise,
	                           cl::NullRange,
	                           cl::NDRange(oh, iw) );
}

template<typename I, typename D>
ClNoiseRw<I, D>::ClNoiseRw(cl::Context& context,
                           cl::Device& device,
                           bool mod1) {
	p_prog = create_program(context, device,
	                        {"cl/noise.cl"},
	                        Scalar<D>::Flags);
	//k_compute_partial = cl::Kernel(p_prog, "compute_partial_sums_rw_simple");
	//k_compute_noise = cl::Kernel(p_prog, "compute_noise_rw_simple");
	if(!mod1)
		k_compute_smart = cl::Kernel(p_prog, "compute_noise_rw_smart_check");
	else
		k_compute_smart = cl::Kernel(p_prog, "compute_noise_rw_smart_check_mod_1");
	k_compute_smart_nocheck = cl::Kernel(p_prog, "compute_noise_rw_smart_nocheck");
}

template<typename I, typename D>
void ClNoiseRw<I, D>::bind_in(const ClMatrixDense<I, D>& in) {
	ih = in.height;
	iw = in.width;

	//k_compute_partial.setArg(0, ih);
	//k_compute_partial.setArg(1, iw);
	//k_compute_partial.setArg(4, in.data);

	//k_compute_noise.setArg(7, in.data);
	//cout << "A" << endl;	
	k_compute_smart.setArg(5, in.data);
	k_compute_smart_nocheck.setArg(5, in.data);
	//cout << "A1" << endl;
}

template<typename I, typename D>
void ClNoiseRw<I, D>::bind_out(const ClMatrixDense<I, D>& out) {
	oh = out.height;
	ow = out.width;

	//k_compute_noise.setArg(0, oh);
	//k_compute_noise.setArg(1, ow);
	//k_compute_noise.setArg(9, out.data);

	//cout << "B" << endl;
	k_compute_smart.setArg(0, oh);
	k_compute_smart.setArg(1, ow);
	k_compute_smart.setArg(6, out.data);
	k_compute_smart_nocheck.setArg(0, oh);
	k_compute_smart_nocheck.setArg(1, ow);
	k_compute_smart_nocheck.setArg(6, out.data);
	//cout << "B1" << endl;
}

template<typename I, typename D>
void ClNoiseRw<I, D>::bind_tmp(const ClMatrixDense<I, D>& tmp) {
	th = tmp.height;
	tw = tmp.width;

	//k_compute_partial.setArg(2, tw);
	//k_compute_partial.setArg(5, tmp.data);

	//k_compute_noise.setArg(2, tw);
	//k_compute_noise.setArg(8, tmp.data);
}

template<typename I, typename D>
void ClNoiseRw<I, D>::run(I noise_size, I shift, cl::CommandQueue& queue) {
	I sec_size = noise_size - 2;
	I num_ker = (ih + sec_size - 1) / sec_size;
	//I up_round_h = num_ker * sec_size;
	if(oh != ih || ow != iw) // || th < ih || tw < 2*iw)
		throw UsageException("Noise wrong sizes: %d, in: %dx%d, tmp: %dx%d, out: %dx%d",
		                     noise_size, ih, iw, th, tw, oh, ow);
	if(shift > sec_size || shift < 0)
		throw UsageException("Shift %d negative or bigger than sec size %d",
		                     shift, sec_size);

	//if(false) {
	//	k_compute_partial.setArg(3, sec_size);
	//	queue.enqueueNDRangeKernel(k_compute_partial,
	//	                           cl::NullRange,
	//	                           cl::NDRange(num_ker, iw),
	//	                           cl::NDRange(1, 16) );
	//}

	I left = sec_size / 2;
	I right = sec_size - left;
	D factor = D(1.0) / noise_size;
	if(true) {
		k_compute_smart.setArg(2, sec_size);
		k_compute_smart.setArg(3, factor);
		k_compute_smart.setArg(4, shift);
		k_compute_smart_nocheck.setArg(2, sec_size);
		k_compute_smart_nocheck.setArg(3, factor);
		k_compute_smart_nocheck.setArg(4, shift);
		/*queue.enqueueNDRangeKernel(k_compute_smart,
		                           cl::NullRange,
		                           cl::NDRange(num_ker, iw),
		                           cl::NDRange(1, 32) );*/
		queue.enqueueNDRangeKernel(k_compute_smart,
		                           cl::NullRange,
		                           cl::NDRange(2, iw),
		                           cl::NDRange(1, 32) );
		queue.enqueueNDRangeKernel(k_compute_smart,
		                           cl::NDRange(num_ker-2,0),
		                           cl::NDRange(2, iw),
		                           cl::NDRange(1, 32) );
		queue.enqueueNDRangeKernel(k_compute_smart_nocheck,
		                           cl::NDRange(2, 0),
		                           cl::NDRange(num_ker-4, iw),
		                           cl::NDRange(1, 32) );
	}
	/*if(false) {
		k_compute_noise.setArg(3, left);
		k_compute_noise.setArg(4, right);
		k_compute_noise.setArg(5, factor);
		queue.enqueueNDRangeKernel(k_compute_noise,
	                           cl::NullRange,
	                           cl::NDRange(oh, iw),
		                       cl::NDRange(1, 16)
	                           );
	}*/
}

template class ClNoiseCw<uint32_t, float>;
template class ClNoiseCw<uint32_t, double>;
template class ClNoiseRw<uint32_t, float>;
template class ClNoiseRw<uint32_t, double>;
