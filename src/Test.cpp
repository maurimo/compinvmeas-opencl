
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <system_error>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <inttypes.h>
#include <boost/numeric/interval/hw_rounding.hpp>
#include <boost/filesystem.hpp>
#include <glog/logging.h>
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#include <limits>
#include "exceptions.h"
#include "utils.h"
#include "matrix.h"
#include "utils.h"
#include "cl_basis_init.h"
#include "cl_noise.h"
#include "cl_norm.h"
#include "cl_matrix_csr.h"
#include "cl_utils.h"
#include "cl_pretty_print.h"
#include "mmap.h"
#include "scalar.h"


using namespace std;

#define DEFAULT_BASIS_TYPE Basis_N_Np1
#define DEFAULT_NORM_TYPE Norm_L1
#define DEFAULT_ALPHA 0.5
#define DEFAULT_NUM_ITERATIONS_HARD_LIMIT 1000
#define DEFAULT_NUM_VECTORS 128
#define DEFAULT_OUT_FILE "out.txt"

template<typename D>
bool all_small_eq(D *d1, D *d2, uint32_t num) {
	for(uint32_t i = 0; i<num; i++)
		if(d1[i] > d2[i])
			return false;
	return true;
}

class Args {
public:
	enum Prec {
		Float = 1,
		Double = 2
	};
	Prec precision = Float;
	int platform_index = 0;
	int device_index = 0;
    bool show_help = false;

	bool init(int argc, char* argv[]);
};

bool Args::init(int argc, char* argv[]) {

    for(int i = 1; i < argc; i++) {
	    if(!strcmp(argv[i], "-f") && i < argc-1) {
		    i++;
		    if(!strcasecmp(argv[i],"f"))
			    precision = Float;
		    else if(!strcasecmp(argv[i],"d"))
			    precision = Double;
		    else {
			    cout << "Precision one must be one of f=float, d=double" << endl;
			    return false;
		    }
	    }
	    else if(!strcmp(argv[i], "-p") && i < argc-1)
	        platform_index = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-d") && i < argc-1)
	        device_index = atoi(argv[++i]);
        else if(!strcmp(argv[i], "-h"))
            show_help = true;
	    else
	        throw BasicException(string("Argument not understood: ")+argv[i]);
    }

    if(show_help) {
	    cout << "Usage:" << endl
             << "    -p n           The index of the OpenCl platform to use" << endl
             << "    -d n           The index of the platform's device to use" << endl
             << "    -f f|d         The float precision (f=float, d=double)" << endl
             << "    -h             Show this help" << endl;
        return false;
    }
    return true;
}


template<typename D>
void test_noise_rw(Args& a, cl::Device device) {
	int size = 256;
	int nvecs = 256;
	cout.precision(numeric_limits<D>::max_digits10);
	
    // input-output matrix
    MatrixDense<uint32_t, D> in(size, nvecs);
    MatrixDense<uint32_t, D> out(size, nvecs);
    MatrixDense<uint32_t, D> tmp(size * 2, nvecs);
    MatrixDense<uint32_t, D> gpu_out(size, nvecs);

    int noise_size = 14;

    in.set_random();
    //in.basis_N_Np1_rw(size-nvecs-1);
    in.set(0);
    for(int i = 0; i < size && i < nvecs; i++)
	    in.data[i*in.height + i] = 1.0;
    in.apply_noiseM1_rw_dummy(noise_size, 0, out);
    in.apply_noiseM1_rw(noise_size, 0, gpu_out, tmp);
    //in.apply_noiseM1_cw(noise_size, 0, gpu_out, tmp);
    //swap(in.width, in.height);
    //swap(out.width, out.height);
    //swap(gpu_out.width, gpu_out.height);
    //swap(tmp.width, tmp.height);
    save_cw_plain_text("b1.txt", out);
    //save_cw_plain_text("o1.txt", out);
    //save_cw_plain_text("o2.txt", gpu_out);
    save_cw_plain_text("tmp.txt", gpu_out);
    cout << "Distance1: " << out.distance_Linf_rw(gpu_out) << endl;

    //return;
    
    // create context
    cl::Context context(device);
        
    //create queue to which we will push commands for the device.
    cl::CommandQueue queue(context, device);

    // create cl matrix objects
    ClMatrixDense<uint32_t, D> vsInCL(size, nvecs, context);
    ClMatrixDense<uint32_t, D> vsOutCL(size, nvecs, context);
    ClMatrixDense<uint32_t, D> vNoiseTmpCL(size, nvecs * 2, context);

    ClNoiseRw<uint32_t, D> kNoiseRwCL(context, device, true);
    kNoiseRwCL.bind_tmp(vNoiseTmpCL);
    kNoiseRwCL.bind_in(vsInCL);
    kNoiseRwCL.bind_out(vsOutCL);
    vsInCL.write(in, queue);
    queue.finish();
    kNoiseRwCL.run(noise_size, 0, queue);
    queue.finish();
    vsOutCL.read(gpu_out, queue);
    queue.finish();

    save_cw_plain_text("o1.txt", out);
    save_cw_plain_text("o2.txt", gpu_out);
    cout << "Distance: " << out.distance_Linf_rw(gpu_out) << endl;
}

template<typename D>
void tests(Args& a, cl::Device device) {
	test_noise_rw<D>(a, device);	
}

int main(int argc, char* argv[]) {
	    
    init_log(argv[0]);
    
    try {
	    Args a;
	    if(!a.init(argc, argv))
		    return 0;

	    cl::Platform platform = get_platform(a.platform_index);
	    cl::Device device = get_device(platform, CL_DEVICE_TYPE_ALL, a.device_index);

	    if(a.precision == Args::Double)
		    tests<double>(a, device);
	    else
		    tests<float>(a, device);
    }
    catch (cl::Error& err) {
        std::cerr << "ERROR: " << err.what() << "(" << err.err() << ")" << std::endl;
        print_opencl_error_code(err.err());
    }
    catch (BasicException& e) {
        std::cerr << "Exception: " << e.message() << std::endl;
    }
 
    return 0;
}
