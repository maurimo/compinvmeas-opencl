
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <system_error>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <inttypes.h>
#include <boost/numeric/interval/hw_rounding.hpp>
#include <boost/filesystem.hpp>
#include <glog/logging.h>
#define __CL_ENABLE_EXCEPTIONS
#include <limits>
#include "exceptions.h"
#include "utils.h"
#include "mmap.h"

typedef double D;

using namespace std;

vector<string> split(const string &s, char delim) {
	vector<string> elems;
    stringstream ss;
    ss.str(s);
    string item;
    while (getline(ss, item, delim))
        elems.push_back(item);
    return elems;
}

int main(int argc, char* argv[]) {
	uint32_t size = atoi(argv[1]);
	char* measure_file = argv[2];
	char* observable_file = argv[3];
	char* ranges_str = argv[4];
	char* out_data_file = argv[5];

	uint64_t size1, size2;
	D* vec_meas = (D*)MMap::map_file(measure_file, &size1);
	D* vec_obs = (D*)MMap::map_file(observable_file, &size2);

	if(size1 != size*sizeof(D) ||
	   size2 != size*2*sizeof(D)) {
		cout << "Wrong file sizes!!" << endl;
		return 1;
	}

	vector<string> ranges = split(ranges_str, ',');

	D lower = 0, upper = 0;
	D obsmin = 0, obsmax = 0;
	boost::numeric::interval_lib::rounded_math<D> rnd;
	for(uint32_t i = 0; i<ranges.size(); i++) {
		vector<string> range = split(ranges[i], '-');
		if(range.size() != 2) {
			cout << "Invalid range \""<<ranges[i]<<"\""<<endl;
			return 1;
		}
		uint32_t from = atoi(range[0].c_str());
		uint32_t to = atoi(range[1].c_str());

		for(uint32_t j = from; j<to; j++) {
			obsmin = min(obsmin, vec_obs[2*j]);
			obsmax = max(obsmax, vec_obs[2*j+1]);
			lower = rnd.add_down(lower, rnd.mul_down(vec_meas[j], vec_obs[2*j]));
			upper = rnd.add_up(upper, rnd.mul_up(vec_meas[j], vec_obs[2*j+1]));
		}
	}

	ofstream out(out_data_file);
	out.write((char*)&lower, sizeof(D));
	out.write((char*)&upper, sizeof(D));
	out.write((char*)&obsmin, sizeof(D));
	out.write((char*)&obsmax, sizeof(D));
	
	return 0;
}
