
#ifndef __MMAP_H__
#define __MMAP_H__

#include <inttypes.h>

namespace MMap {

/*! \brief Maps a file in memory.
    \param file_name the path of the file to map.
    \param size the pointer to an uint32_t variable to store the size of the map
    \return a pointer to the map. */
const void *map_file(const char* file_name, uint64_t* size);

/*! \brief Unmaps a file.
    \param addr the address of the map
    \param size the size of the map. */
void unmap_file(const void* addr, uint64_t size);

} //end namespace MMap

#endif //__MMAP_H__
