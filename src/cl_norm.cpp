
#define __CL_ENABLE_EXCEPTIONS
#include "exceptions.h"
#include "cl_utils.h"
#include "scalar.h"
#include "cl_norm.h"

#define LOCAL_SIZE 32

template<typename I, typename D>
ClNormCw<I, D>::ClNormCw(cl::Context& context,
                         cl::Device& device) {
	norm_prog = create_program(context, device,
	                           {"cl/norm.cl"},
	                           Scalar<D>::Flags);
	k_L1 = cl::Kernel(norm_prog, "norm_L1_cw");
	k_Linf = cl::Kernel(norm_prog, "norm_Linf_cw");
}

template<typename I, typename D>
void ClNormCw<I, D>::bind_mat(const ClMatrixDense<I, D>& mat) {
	mh = mat.height;
	mw = mat.width;
	k_L1.setArg(0, mh);
	k_L1.setArg(1, mat.data);
	k_Linf.setArg(0, mh);
	k_Linf.setArg(1, mat.data);
}

template<typename I, typename D>
void ClNormCw<I, D>::bind_out(const ClMatrixDense<I, D>& out) {
	oh = out.height;
	ow = out.width;
	k_L1.setArg(2, out.data);
	k_Linf.setArg(2, out.data);
}

template<typename I, typename D>
void ClNormCw<I, D>::compute_L1(cl::CommandQueue& queue, uint32_t from, uint32_t to) {
	if(ow != mw || oh != 1)
		throw UsageException("CW norm M_%ux%u -> M_%ux%u",
		                     mh, mw, oh, ow);
	if(to == 0)
		to = mw;
	queue.enqueueNDRangeKernel(k_L1,
	                           cl::NDRange(from, 0),
	                           cl::NDRange(to-from, LOCAL_SIZE),
	                           cl::NDRange(1, LOCAL_SIZE)
	                           );
}

template<typename I, typename D>
void ClNormCw<I, D>::compute_Linf(cl::CommandQueue& queue, uint32_t from, uint32_t to) {
	if(ow != mw || oh != 1)
		throw UsageException("CW norm M_%ux%u -> M_%ux%u",
		                     mh, mw, oh, ow);
	if(to == 0)
		to = mw;
	queue.enqueueNDRangeKernel(k_Linf,
	                           cl::NDRange(from, 0),
	                           cl::NDRange(to-from, LOCAL_SIZE),
	                           cl::NDRange(1, LOCAL_SIZE)
	                           );
}


template<typename I, typename D>
ClNormRw<I, D>::ClNormRw(cl::Context& context,
                         cl::Device& device) {
	norm_prog = create_program(context, device,
	                           {"cl/norm.cl"},
	                           Scalar<D>::Flags);
	k_L1 = cl::Kernel(norm_prog, "norm_L1_rw");
	k_Linf = cl::Kernel(norm_prog, "norm_Linf_rw");
	int ws = k_L1.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(device);
	k_L1_d2size = std::min(8, ws/LOCAL_SIZE);
	ws = k_Linf.getWorkGroupInfo<CL_KERNEL_WORK_GROUP_SIZE>(device);
	k_Linf_d2size = std::min(8, ws/LOCAL_SIZE);
	std::cout << "ws = " << ws << ", s = " << k_Linf_d2size << std::endl;
}

template<typename I, typename D>
void ClNormRw<I, D>::bind_mat(const ClMatrixDense<I, D>& mat) {
	mh = mat.height;
	mw = mat.width;
	k_L1.setArg(0, mh);
	k_L1.setArg(1, mw);
	k_L1.setArg(2, mat.data);
	k_Linf.setArg(0, mh);
	k_Linf.setArg(1, mw);
	k_Linf.setArg(2, mat.data);
}

template<typename I, typename D>
void ClNormRw<I, D>::bind_out(const ClMatrixDense<I, D>& out) {
	oh = out.height;
	ow = out.width;
	k_L1.setArg(3, out.data);
	k_Linf.setArg(3, out.data);
}

template<typename I, typename D>
void ClNormRw<I, D>::compute_L1(cl::CommandQueue& queue, uint32_t from, uint32_t to) {
	if(ow != mw || oh != 1)
		throw UsageException("CW norm M_%ux%u -> M_%ux%u",
		                     mh, mw, oh, ow);
	if(to == 0)
		to = mw;
	queue.enqueueNDRangeKernel(k_L1,
	                           cl::NDRange(from, 0),
	                           cl::NDRange(to-from, LOCAL_SIZE),
	                           cl::NDRange(k_L1_d2size, LOCAL_SIZE)
	                           );
}

template<typename I, typename D>
void ClNormRw<I, D>::compute_Linf(cl::CommandQueue& queue, uint32_t from, uint32_t to) {
	if(ow != mw || oh != 1)
		throw UsageException("CW norm M_%ux%u -> M_%ux%u",
		                     mh, mw, oh, ow);
	if(to == 0)
		to = mw;
	queue.enqueueNDRangeKernel(k_Linf,
	                           cl::NDRange(from, 0),
	                           cl::NDRange((to-from), LOCAL_SIZE),
	                           cl::NDRange(k_Linf_d2size, LOCAL_SIZE)
	                           );
}

/* explicit template instantiation */
template class ClNormCw<uint32_t, float>;
template class ClNormCw<uint32_t, double>;
template class ClNormRw<uint32_t, float>;
template class ClNormRw<uint32_t, double>;
