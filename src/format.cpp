
#include <cstdio>
#include <alloca.h>
#include "format.h"

std::string strf(const char *fmt, ...) {
	va_list ap;

	va_start(ap, fmt);
	std::string retv = vstrf(fmt, ap);
	va_end(ap);

	return retv;
}

std::string vstrf(const char *fmt, va_list ap) {
	va_list aptmp;
	va_copy(aptmp, ap);

	int l = vsnprintf(NULL, 0, fmt, aptmp);

	char *cstr = (char*)alloca(l+1);
	vsnprintf(cstr, l+1, fmt, ap);

	return std::string(cstr, cstr+l);
}
