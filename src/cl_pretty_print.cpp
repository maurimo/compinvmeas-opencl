
#include <sstream>
#include <iomanip>
#include <glog/logging.h>
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#include "cl_pretty_print.h"

using namespace std;

string desc_enum(unsigned long v, const char* names[], unsigned int size) {
	if(v < size)
		return names[v];
	return "<UNKNOWN>";
}

string desc_bf(unsigned long v, const char* names[], unsigned int size) {
    string retv;
	bool first = true;

	for(unsigned int i = 0; i < size; ++i) {
		if((v & (1 << i)) == 0)
			continue;
		if(!first)
			retv += "|";
		first = false;
		retv += names[i];
	}
	return retv.size() ? retv : "<NONE>";
}

string desc_mem(double m) {
    static const char* sf[] = { "b", "Kb", "Mb", "Gb", "Tb" };
	unsigned int i = 0;
    while(m>2*1024 && i < sizeof(sf)/sizeof(sf[0])) {
		m /= 1024;
        ++i;
    }
	ostringstream os;
	os << fixed << setprecision(1) << m << " " << sf[i];
	return os.str();
}

template<cl_uint N, typename T>
string prettyInfo(T& t);

template<>
string prettyInfo<CL_DEVICE_GLOBAL_MEM_SIZE, cl::Device>(cl::Device& d) {
    return desc_mem(d.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>());
}

template<>
string prettyInfo<CL_DEVICE_LOCAL_MEM_SIZE, cl::Device>(cl::Device& d) {
    return desc_mem(d.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>());
}

template<>
string prettyInfo<CL_DEVICE_LOCAL_MEM_TYPE, cl::Device>(cl::Device& d) {
    static const char* n[] = { "<NONE>", "LOCAL", "GLOBAL" };
	return desc_enum(d.getInfo<CL_DEVICE_LOCAL_MEM_TYPE>(), n, sizeof(n)/sizeof(n[0]));
}

static const char* fp_c[] = {
	"DENORM", "INF_NAN", "ROUND_TO_NEAREST", "ROUND_TO_ZERO",
	"ROUND_TO_INF", "FMA", "SOFT_FLOAT", "CORRECTLY_ROUNDED_DIVIDE_SQRT"
};

template<>
string prettyInfo<CL_DEVICE_HALF_FP_CONFIG, cl::Device>(cl::Device& d) {
	return desc_bf(d.getInfo<CL_DEVICE_HALF_FP_CONFIG>(), fp_c, sizeof(fp_c)/sizeof(fp_c[0]));
};

template<>
string prettyInfo<CL_DEVICE_SINGLE_FP_CONFIG, cl::Device>(cl::Device& d) {
	return desc_bf(d.getInfo<CL_DEVICE_SINGLE_FP_CONFIG>(), fp_c, sizeof(fp_c)/sizeof(fp_c[0]));
};

template<>
string prettyInfo<CL_DEVICE_DOUBLE_FP_CONFIG, cl::Device>(cl::Device& d) {
	return desc_bf(d.getInfo<CL_DEVICE_DOUBLE_FP_CONFIG>(), fp_c, sizeof(fp_c)/sizeof(fp_c[0]));
};

template<>
string prettyInfo<CL_DEVICE_TYPE, cl::Device>(cl::Device& d) {
	static const char* n[] = { "DEFAULT", "CPU", "GPU", "ACCELERATOR", "CUSTOM" };
	return desc_bf(d.getInfo<CL_DEVICE_TYPE>(), n, sizeof(n)/sizeof(n[0]));
}

template<>
string prettyInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES, cl::Device>(cl::Device& d) {
	vector<size_t> workItemSizes = d.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
	ostringstream os;
	for(unsigned int i =0; i<workItemSizes.size(); ++i) {
		if(i > 0)
			os << ",";
		os << workItemSizes[i];
	}
	return os.str();
}

template<>
void logInfo(ostream& os, cl::Platform& p, int index) {
	os << index << ". \"" << p.getInfo<CL_PLATFORM_NAME>() << "\"" << endl;
	os << "   Pl Ext's..: " << p.getInfo<CL_PLATFORM_EXTENSIONS>() << endl;
	os << "   Pl Vendor.: " << p.getInfo<CL_PLATFORM_VENDOR>() << endl;
	os << "   Pl Profile: " << p.getInfo<CL_PLATFORM_PROFILE>() << endl;
	os << "   Pl Version: " << p.getInfo<CL_PLATFORM_VERSION>() << endl;
}

template<>
void logInfo(ostream& os, cl::Device& d, int index) {
	os << index << ". \"" << d.getInfo<CL_DEVICE_NAME>() << "\"" << endl;
	os << "   Blt'n Kern: " << d.getInfo<CL_DEVICE_BUILT_IN_KERNELS>() << endl;
	string extensions = d.getInfo<CL_DEVICE_EXTENSIONS>();
	os << "   Dev Ext's.: " << extensions << endl;
	os << "   Dev Type..: " << prettyInfo<CL_DEVICE_TYPE>(d) << endl;
	os << "   G-Mem Size: " << prettyInfo<CL_DEVICE_GLOBAL_MEM_SIZE>(d) << endl;
	os << "   L-Mem Type: " << prettyInfo<CL_DEVICE_LOCAL_MEM_TYPE>(d) << endl;
	os << "   L-Mem Size: " << prettyInfo<CL_DEVICE_LOCAL_MEM_SIZE>(d) << endl;
	os << "   WG-size...: " << d.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << endl;	
	os << "   Item-sizes: " << prettyInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>(d) << endl;	
	os << "   Fp16-Conf.: " << (extensions.find("cl_khr_fp16") != string::npos
	                            ? prettyInfo<CL_DEVICE_HALF_FP_CONFIG>(d)
	                            : "(UNSUPPORTED)") << endl;
	os << "   Fp32-Conf.: " << prettyInfo<CL_DEVICE_SINGLE_FP_CONFIG>(d) << endl;
	os << "   Fp64-Conf.: " << (extensions.find("cl_khr_fp64") != string::npos
	                            ? prettyInfo<CL_DEVICE_DOUBLE_FP_CONFIG>(d)
	                            : "(UNSUPPORTED)") << endl;
	os << "   Addr Bits.: " << d.getInfo<CL_DEVICE_ADDRESS_BITS>() << endl;
	os << "   Max Dim...: " << d.getInfo<CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS>() << endl;
	os << "   Lit'l End.: " << d.getInfo<CL_DEVICE_ENDIAN_LITTLE>() << endl;
	os << "   Vendor....: " << d.getInfo<CL_DEVICE_VENDOR>() << endl;
	os << "   Profile...: " << d.getInfo<CL_DEVICE_PROFILE>() << endl;
	os << "   Version...: " << d.getInfo<CL_DEVICE_VERSION>() << endl;
	os << "   Drv. Ver..: " << d.getInfo<CL_DRIVER_VERSION>() << endl;
}
