#ifndef __CL_PRETTY_PRINT_H__
#define __CL_PRETTY_PRINT_H__

#include <ostream>
#include <CL/cl.hpp>

std::string desc_enum(unsigned long v, const char* names[], unsigned int size);

std::string desc_bf(unsigned long v, const char* names[], unsigned int size);

std::string desc_mem(double m);

template<cl_uint N, typename T>
std::string prettyInfo(T& t);

template<cl_uint N, typename T>
std::string prettyInfo(T& t);

template<typename T>
void logInfo(std::ostream& os, T& t, int index = 0);

#endif //__CL_PRETTY_PRINT_H__

// Local Variables:
// mode: c++
// End:
