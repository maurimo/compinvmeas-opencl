
#include "mmap.h"

namespace MMap {

#if defined(_WIN32) || defined(_WIN64)

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>

void *map_file(const char *file_name, uint64_t *size)
{
    HANDLE file, fmap;
    uint64_t offset = 0;
    DWORD hiword, loword;
    void *retv;

    file = CreateFile(file_name, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE,
                                    NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if(file==INVALID_HANDLE_VALUE)
        return NULL;

    loword = GetFileSize(file, &hiword);
    if(loword == INVALID_FILE_SIZE)
    {
        CloseHandle(file);
        return(NULL);
    }
    *size = loword | ((uint64_t)hiword<<32);

    fmap = CreateFileMapping(file, NULL, PAGE_READONLY, 0, 0, NULL);
    if(fmap == INVALID_HANDLE_VALUE)
    {
        CloseHandle(file);
        return NULL;
    }

    retv = MapViewOfFile(fmap, FILE_MAP_READ, (DWORD)(offset >> 32),
                         (DWORD)(offset % 0xffffffff), 0);

    CloseHandle(fmap);
    CloseHandle(file);
    return retv;
}

void unmap_file(void *addr, uint64_t size)
{
    UnmapViewOfFile(addr);
}

#else //defined(_WIN32) || defined(_WIN64)

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>

const void *map_file(const char *file_name, uint64_t *size)
{
    struct stat st;
    void *addr;
    int fd;

    fd = open( file_name, O_RDONLY);
    if(fd == -1)
        return NULL;

    fstat(fd, &st);
    if(size)
     *size = st.st_size;

    addr = mmap( NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if(addr == (void*)-1)
    {
        close(fd);
        return NULL;
    }

    close(fd);
    return addr;
}

void unmap_file(const void* addr, uint64_t size)
{
    munmap((void*)addr, size);
}

#endif //defined(_WIN32) || defined(_WIN64)

} //end namespace MMap
