#!/bin/bash

DIR=$PWD

GLOG=glog-0.6.0
rm -rf glog-*/
tar xzvf "$GLOG".tar.gz
cd "$GLOG"
cmake -S . -B build -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$DIR/inst -DBUILD_SHARED_LIBS=OFF || exit 1
cmake --build build || exit 1
cmake --build build --target install || exit 1