  
#ifndef DOUBLE
  #define DOUBLE 0
#endif

#if DOUBLE
  #pragma OPENCL EXTENSION cl_khr_fp64 : enable
  #define scalar_t   double
  #define scalar2_t  double2
  #define scalar3_t  double3
  #define scalar4_t  double4
  #define scalar8_t  double8
  #define scalar16_t double16
#else
  #define scalar_t   float
  #define scalar2_t  float2
  #define scalar3_t  float3
  #define scalar4_t  float4
  #define scalar8_t  float8
  #define scalar16_t float16
#endif

// unsupported?!?
//#pragma OPENCL EXTENSION cl_khr_select_fprounding_mode : enable
//#pragma select_rounding_mode rtp

// Local Variables:
// mode: c
// End: