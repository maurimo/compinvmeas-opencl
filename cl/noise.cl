
#include "scalar.cl"

#define CHUNK 32
#define LSIZE 64

__kernel void compute_partial_sums_cw_simple(int height,
                                             int width,
                                             int tmp_height,
                                             int sec_size,
                                             int shift,
                                             const __global scalar_t* restrict in,
                                             __global scalar_t* restrict tmp) {
	int roff = get_global_id(0) * sec_size;                  //row offset
	int row_off = get_global_id(1) * height;                //in col index	
	int ioff = roff + row_off;                //in col index	
	int t1off = roff + get_global_id(1) * tmp_height;           //tmp col index
	int t2off = roff + (get_global_id(1) + width) * tmp_height; //tmp col index, 2

	// TODO: handle the case that height is not multiple of sec_size
	scalar_t acc = 0;
	for(int i = 0; i < sec_size; i++) {
		if(roff + i - shift >= 0)
			acc += in[row_off + (roff + i - shift)];
		if(roff + i + shift >= width)
			acc += in[row_off + 2*width - 1 - (roff + i + shift)];
		tmp[t1off + i] = acc;
	}

	acc = 0;
	for(int i = sec_size - 1; i >= 0; i--) {
		if(roff + i - shift >= 0)
			acc += in[row_off + (roff + i - shift)];
		if(roff + i + shift >= width)
			acc += in[row_off + 2*width - 1 - (roff + i + shift)];
		tmp[t2off + i] = acc;
	}
}

__kernel void compute_partial_sums_cw_simple_test(int height,
                                             int width,
                                             int tmp_height,
                                             int sec_size,
                                             const __global scalar_t* restrict in,
                                             __global scalar_t* restrict tmp) {
	int roff = get_global_id(0) * sec_size;                  //row offset
	int ioff = roff + get_global_id(1) * height;                //in col index	
	int t1off = roff + get_global_id(1) * tmp_height;           //tmp col index
	int t2off = roff + (get_global_id(1) + width) * tmp_height; //tmp col index, 2

	// TODO: handle the case that height is not multiple of sec_size
	scalar_t acc = 0, acc2 = 0;
	for(int i = 0; i < sec_size; i++) {
		acc += in[ioff + i];
		tmp[t1off + i] = acc;
		acc2 += in[ioff + sec_size - 1 - i];
		tmp[t2off + sec_size - 1 - i] = acc2;
	}
}

__kernel void compute_noise_cw_simple(unsigned int height,
                                      unsigned int width,
                                      unsigned int tmp_height,
                                      unsigned int left,
                                      unsigned int right,
                                      scalar_t factor,
                                      const __global scalar_t* restrict tmp,
                                      __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0);
	unsigned int ooff = get_global_id(1) * height;                //tmp col index
	unsigned int t1off = get_global_id(1) * tmp_height;           //tmp col index
	unsigned int t2off = (get_global_id(1) + width) * tmp_height; //tmp col index, 2
	
	scalar_t sum;
	if(r < left) // left reflection
		sum = tmp[t1off + r + right] + tmp[t1off + left - r - 1];
	else if(r + right >= tmp_height) { // right reflection
		unsigned int idx = 2*tmp_height - 1 - r - right;
		sum = tmp[t2off + idx] + tmp[t2off + r - left];
	}
	else
		sum = tmp[t1off + r + right] + tmp[t2off + r - left];
	out[ooff + r] = sum * factor;
}


__kernel void compute_partial_sums_rw_simple(unsigned int height,
                                             unsigned int width,
                                             unsigned int tmp_width,
                                             unsigned int sec_size,
                                             const __global scalar_t* restrict in,
                                             __global scalar_t* restrict tmp) {
	unsigned int c = get_global_id(1);                                //col offset	
	unsigned int r = get_global_id(0) * sec_size;
	unsigned int rioff =  r * width;                                  //row offset
	unsigned int rt1off = r * tmp_width;                              //row offset, tmp/1
	unsigned int rt2off = width + rt1off;                             //row offset, tmp/2

	unsigned int len = min(height - r, sec_size);
	
	scalar_t acc = 0;
	const __global scalar_t* in1 = in + rioff + c;
	__global scalar_t* tmp1 = tmp + rt1off + c;
	for(int i = 0; i < len; i++) {
		acc += *in1;
		*tmp1 = acc;
		in1 += width;
		tmp1 += tmp_width;
	}

	scalar_t acc2 = 0;
	const __global scalar_t* in2 = in + rioff + c + (len-1)*width;
	__global scalar_t* tmp2 = tmp + rt2off + c + (len-1)*tmp_width;
	for(int i = len - 1; i >= 0; i--) {
		acc2 += *in2;
		*tmp2 = acc2;
		in2 -= width;
		tmp2 -= tmp_width;
	}
}


__kernel void compute_noise_rw_simple(unsigned int height,
                                      unsigned int width,
                                      unsigned int tmp_width,
                                      unsigned int left,
                                      unsigned int right,
                                      scalar_t factor,
                                      const __global scalar_t* restrict in,
                                      const __global scalar_t* restrict tmp,
                                      __global scalar_t* restrict out) {
	unsigned int c = get_global_id(1);                                //col offset
	unsigned int r = get_global_id(0);
	unsigned int sec_size = left + right;
	unsigned int rem = height % sec_size;
	unsigned int clamp = rem > 0 ? height - rem : 0;
	const __global scalar_t* restrict tmp2 = tmp + width;

	//printf("K: %d %d\n", height, width);
	scalar_t sum;
	if(r < left) // left reflection
		sum = tmp[c + (r + right)*tmp_width] + tmp[c + (left - r - 1)*tmp_width];
	else if(r + right >= height) { // right reflection
		unsigned int idx1 = 2*height - 1 - r - right;
		unsigned int idx2 = r - left;
		sum = tmp2[c + idx1*tmp_width] + tmp2[c + idx2*tmp_width];
		if(idx1 < clamp)
			sum += tmp2[c + clamp*tmp_width];
		if(idx2 < clamp)
			sum += tmp2[c + clamp*tmp_width];
	}
	else
		sum = tmp[c + (r + right)*tmp_width] + tmp2[c + (r - left)*tmp_width];
	int lh = (r >= left+1) ? (r-left-1) : (-r+left);
	int rh = (r+right+1 < height) ? (r+right+1) : (2*height - 2 - r - right);
	sum += (in[c + lh*width] + in[c + rh*width]) / 2;
	out[c + r*width] = sum * factor;
}


__kernel void compute_noise_rw_smart_4(unsigned int height,
                                     unsigned int width,
                                     unsigned int sec_size,
                                     scalar_t factor,
                                     const __global scalar4_t* restrict in,
                                     __global scalar4_t* restrict out) {
	unsigned int c = get_global_id(1);                                //col offset	
	unsigned int r = get_global_id(0) * sec_size;
	unsigned int rmid = get_global_id(0) * sec_size + sec_size/2;

	//int ok = c == 0 && r == sec_size;
	scalar4_t tmp1[256];
	scalar4_t tmp2[256];

	
	scalar4_t accum = 0;
	for(int i = 0; i < sec_size; i++) {
		int idx = rmid + i;
		idx = idx < 0 ? -1-idx : idx >= height ? 2*height-1-idx : idx;
		accum += in[c + idx*width/4];
		tmp1[i] = accum;
	}
	accum = 0;
	for(int i = sec_size-1; i >= 0; i--) {
		int idx = rmid - sec_size + i;
		idx = idx < 0 ? -1-idx : idx >= height ? 2*height-1-idx : idx;
		accum += in[c + idx*width/4];
		tmp2[i] = accum;
	}
	for(int i = 0; i < sec_size; i++) {
		int idx1 = rmid + i - sec_size - 1;
		idx1 = idx1 < 0 ? -1-idx1 : idx1 >= height ? 2*height-1-idx1 : idx1;
		int idx2 = rmid + i + 1;
		idx2 = idx2 < 0 ? -1-idx2 : idx2 >= height ? 2*height-1-idx2 : idx2;
		scalar4_t sum = tmp1[i] + tmp2[i]
			+ in[c + idx1*width/4] / 2
			+ in[c + idx2*width/4] / 2;
		if(r + i < height)
			out[c + (r + i)*width/4] = sum * factor;
	}
}

__kernel void compute_noise_rw_smart(unsigned int height,
                                     unsigned int width,
                                     unsigned int sec_size,
                                     scalar_t factor,
                                     const __global scalar_t* restrict in,
                                     __global scalar_t* restrict out) {
	unsigned int c = get_global_id(1);                                //col offset	
	unsigned int r = get_global_id(0) * sec_size;
	unsigned int rmid = r + sec_size/2;

	//int ok = c == 0 && r == sec_size;
	scalar_t tmp1[4096];
	scalar_t tmp2[4096];

	if(r > 2*sec_size && rmid + 2*sec_size < height) {
		scalar_t accum = 0;
		for(int i = 0; i < sec_size; i++) {
			int idx = rmid + i;
			//idx = idx < 0 ? -1-idx : idx >= height ? 2*height-1-idx : idx;
			accum += in[c + idx*width];
			tmp1[i] = accum;
		}
		accum = 0;
		for(int i = sec_size-1; i >= 0; i--) {
			int idx = rmid - sec_size + i;
			//idx = idx < 0 ? -1-idx : idx >= height ? 2*height-1-idx : idx;
			accum += in[c + idx*width];
			tmp2[i] = accum;
		}	
		for(int i = 0; i < sec_size; i++) {
			int idx1 = rmid + i - sec_size - 1;
			//idx1 = idx1 < 0 ? -1-idx1 : idx1 >= height ? 2*height-1-idx1 : idx1;
			int idx2 = rmid + i + 1;
			//idx2 = idx2 < 0 ? -1-idx2 : idx2 >= height ? 2*height-1-idx2 : idx2;
			scalar_t sum = tmp1[i] + tmp2[i]
				+ in[c + idx1*width] / 2
				+ in[c + idx2*width] / 2;
			int pos = r + i;
			out[c + pos*width] = sum * factor;
		}
	}
	else {
		scalar_t accum = 0;
		for(int i = 0; i < sec_size; i++) {
			int idx = rmid + i;
			idx = idx < 0 ? -1-idx : idx >= height ? 2*height-1-idx : idx;
			accum += in[c + idx*width];
			tmp1[i] = accum;
		}
		accum = 0;
		for(int i = sec_size-1; i >= 0; i--) {
			int idx = rmid - sec_size + i;
			idx = idx < 0 ? -1-idx : idx >= height ? 2*height-1-idx : idx;
			accum += in[c + idx*width];
			tmp2[i] = accum;
		}	
		for(int i = 0; i < sec_size; i++) {
			int idx1 = rmid + i - sec_size - 1;
			idx1 = idx1 < 0 ? -1-idx1 : idx1 >= height ? 2*height-1-idx1 : idx1;
			int idx2 = rmid + i + 1;
			idx2 = idx2 < 0 ? -1-idx2 : idx2 >= height ? 2*height-1-idx2 : idx2;
			scalar_t sum = tmp1[i] + tmp2[i]
				+ in[c + idx1*width] / 2
				+ in[c + idx2*width] / 2;
			int pos = r + i;
			out[c + pos*width] = sum * factor;
		}
	}
}


__kernel void compute_noise_rw_smart_nocheck(unsigned int height,
                                             unsigned int width,
                                             unsigned int sec_size,
                                             scalar_t factor,
                                             unsigned int shift,
                                             const __global scalar_t* restrict in,
                                             __global scalar_t* restrict out) {
	unsigned int c = get_global_id(1);                                //col offset	
	unsigned int r = get_global_id(0) * sec_size;
	unsigned int rmid = r + sec_size/2;

	scalar_t tmp2[4096];

	scalar_t accum = 0;
	for(int i = sec_size-1; i >= 0; i--) {
		int idx = rmid - sec_size + i - shift;
		accum += in[c + idx*width];
		tmp2[i] = accum;
	}
	accum = 0;
	scalar_t valp1 = in[c + rmid*width];
	for(int i = 0; i < sec_size; i++) {
		int idx1 = rmid + i - sec_size - 1 - shift;
		int idx2 = rmid + i + 1 - shift;
		accum += valp1;
		valp1 = in[c + idx2*width];
		scalar_t sum = accum + tmp2[i]
			+ in[c + idx1*width] / 2
			+ valp1 / 2;
		out[c + (r + i)*width] = sum * factor;
	}
}

__kernel void compute_noise_rw_smart_check(int height,
                                     int width,
                                     int sec_size,
                                     scalar_t factor,
                                     int shift,
                                     const __global scalar_t* restrict in,
                                     __global scalar_t* restrict out) {
	int c = get_global_id(1);                                //col offset	
	int r = get_global_id(0) * sec_size;
	int rmid = r + sec_size/2;

	scalar_t tmp2[4096];

	scalar_t accum = 0;
	for(int i = sec_size-1; i >= 0; i--) {
		int idx = rmid - sec_size + i;
		idx = idx < 0 ? -1-idx : idx >= height ? 2*height-1-idx : idx;
		if(idx-shift >= 0)
			accum += in[c + (idx-shift)*width];
		if(idx+shift >= height)
			accum += in[c + (2*height-1-(idx+shift))*width];
		tmp2[i] = accum;
	}
	accum = 0;
	int idx = rmid < 0 ? -1-rmid : rmid >= height ? 2*height-1-rmid : rmid;
	scalar_t valp1 = 0;;
	if(idx-shift >= 0)
		valp1 += in[c + (idx-shift)*width];
	if(idx+shift >= height)
		valp1 += in[c + (2*height-1-(idx+shift))*width];

	for(int i = 0; i < sec_size; i++) {
		int idx1 = rmid + i - sec_size - 1;
		idx1 = idx1 < 0 ? -1-idx1 : idx1 >= height ? 2*height-1-idx1 : idx1;
		int idx2 = rmid + i + 1;
		idx2 = idx2 < 0 ? -1-idx2 : idx2 >= height ? 2*height-1-idx2 : idx2;
		accum += valp1;

		valp1 = 0;
		if(idx2-shift >= 0)
			valp1 += in[c + (idx2-shift)*width];
		if(idx2+shift >= height)
			valp1 += in[c + (2*height-1-(idx2+shift))*width];
		//valp1 = in[c + idx2*width];

		scalar_t extr = 0;
		if(idx1-shift >= 0)
			extr += in[c + (idx1-shift)*width];
		if(idx1+shift >= height)
			extr += in[c + (2*height-1-(idx1+shift))*width];
		scalar_t sum = accum + tmp2[i]
			+ extr / 2
			+ valp1 / 2;
		if(r + i < height)
			out[c + (r + i)*width] = sum * factor;
	}
}

__kernel void compute_noise_rw_smart_check_mod_1(int height,
                                     int width,
                                     int sec_size,
                                     scalar_t factor,
                                     int shift,
                                     const __global scalar_t* restrict in,
                                     __global scalar_t* restrict out) {
	int c = get_global_id(1);
	int r = get_global_id(0) * sec_size;
	int rmid = r + sec_size/2;

	scalar_t tmp2[4096];

	scalar_t accum = 0;
	for(int i = sec_size-1; i >= 0; i--) {
		int idx = rmid - sec_size + i;
		idx = (idx+height) % height;
		if(idx-shift >= 0)
			accum += in[c + (idx-shift)*width];
		if(idx+shift >= height)
			accum += in[c + (2*height-1-(idx+shift))*width];
		tmp2[i] = accum;
	}
	accum = 0;
	int idx = (rmid + height) % height;
	scalar_t valp1 = 0;
	if(idx-shift >= 0)
		valp1 += in[c + (idx-shift)*width];
	if(idx+shift >= height)
		valp1 += in[c + (2*height-1-(idx+shift))*width];

	for(int i = 0; i < sec_size; i++) {
		int idx1 = rmid + i - sec_size - 1;
		idx1 = (idx1 + height) % height;
		int idx2 = rmid + i + 1;
		idx2 = (idx2 + height) % height;
		accum += valp1;

		valp1 = 0;
		if(idx2-shift >= 0)
			valp1 += in[c + (idx2-shift)*width];
		if(idx2+shift >= height)
			valp1 += in[c + (2*height-1-(idx2+shift))*width];

		scalar_t extr = 0;
		if(idx1-shift >= 0)
			extr += in[c + (idx1-shift)*width];
		if(idx1+shift >= height)
			extr += in[c + (2*height-1-(idx1+shift))*width];
		scalar_t sum = accum + tmp2[i]
			+ extr / 2
			+ valp1 / 2;
		if(r + i < height)
			out[c + (r + i)*width] = sum * factor;
	}
}
