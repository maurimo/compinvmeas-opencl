
#include "scalar.cl"

__kernel void min_rw(unsigned int from,
                     unsigned int to,
                     unsigned int target,
                     unsigned int width,
                     const __global scalar_t* restrict in,
                     __global scalar_t* restrict out) {
	unsigned int c = get_global_id(0); //col index
	unsigned int inptr = in + from * width + c;
	unsigned int outptr = out + target * width + c;

	scalar_t val = *inptr;
	for(unsigned int i = from+1;
	    i < to;
	    i++, inptr += width)
	  val = min(val, *inptr);

	*outptr = val;
}

// Local Variables:
// mode: c
// End:
