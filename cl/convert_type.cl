
#include "scalar.cl"

__kernel void convert_cw_to_rw(unsigned int height,
                               unsigned int width,
                               const __global scalar_t* restrict in,
                               __global scalar_t* restrict out) {
    unsigned int r = get_global_id(0);
    unsigned int c = get_global_id(1);                                //col offset	
    
    out[r*width + c] = in[r + c*height];
}