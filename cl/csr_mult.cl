
#include "scalar.cl"

__kernel void csr_mult_cw(const __global scalar_t* restrict data,
                          const __global unsigned int * restrict row_ptr,
                          const __global unsigned int * restrict col_id,
                          unsigned int mat_width,
                          unsigned int mat_height,
                          const __global scalar_t* restrict in,
                          unsigned int num_vectors, //n. of col vectors, = I/O width
                          __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int q = get_global_id(1); //I/O vec index
	unsigned int idx = row_ptr[r];   //first col/data we will read
	unsigned int end = row_ptr[r+1]; //where we will stop
	const __global scalar_t* in_vec = in + q*mat_width;
	__global scalar_t* out_vec = out + q*mat_height;
	scalar_t sum = 0;

	if(q>=num_vectors)
		return;

	while(idx != end) {
		sum += data[idx] * in_vec[col_id[idx]];
		//sum = fma(data[idx], in_vec[col_id[idx]], sum);
		idx++;
	}

	out_vec[r] = sum;
}

__kernel void csr_mult_rw_vec(const __global scalar_t* restrict data,
                              const __global unsigned int * restrict row_ptr,
                              const __global unsigned int * restrict col_id,
                              const __global scalar_t* restrict in,
                              unsigned int num_vectors, //n. of col vectors, = I/O width
                              __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int q = get_global_id(1); //I/O vec index
	unsigned int idx = row_ptr[r];   //first col/data we will read
	unsigned int end = row_ptr[r+1]; //where we will stop
	const __global scalar_t* in_vec = in + q*8;
	__global scalar_t* out_vec = out + q*8;
	scalar8_t sum = 0;
	
	if(q>=num_vectors)
		return;

	while(idx != end) {
		sum += data[idx] * vload8(0, in_vec + col_id[idx] * num_vectors);
		//sum = fma(data[idx], in_vec[col_id[idx] * num_vectors], sum);
		idx++;
	}

	vstore8(sum, 0, out_vec + r * num_vectors);
}

/* the unoptimized function */
__kernel void csr_mult_rw_simple(const __global scalar_t* restrict data,
                                 const __global unsigned int * restrict row_ptr,
                                 const __global unsigned int * restrict col_id,
                                 const __global scalar_t* restrict in,
                                 unsigned int num_vectors, //n. of col vectors, = I/O width
                                 __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int q = get_global_id(1); //I/O vec index
	unsigned int idx = row_ptr[r];   //first col/data we will read
	unsigned int end = row_ptr[r+1]; //where we will stop
	const __global scalar_t* in_vec = in + q;
	__global scalar_t* out_vec = out + q;
	scalar_t sum = 0;
	
	if(q>=num_vectors)
		return;
	
	while(idx != end) {
		sum += data[idx] * in_vec[col_id[idx] * num_vectors];
		//sum = fma(data[idx], in_vec[col_id[idx] * num_vectors], sum);
		idx++;
	}

	out_vec[r * num_vectors] = sum;
}

__kernel void csr_mult_rw(const __global scalar_t* restrict data,
                          const __global unsigned int * restrict row_ptr,
                          const __global unsigned int * restrict col_id,
                          const __global scalar_t* restrict in,
                          unsigned int num_vectors, //n. of col vectors, = I/O width
                          __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int q = get_global_id(1); //I/O vec index
	unsigned int idx = row_ptr[r];   //first col/data we will read
	unsigned int end = row_ptr[r+1]; //where we will stop
	const __global scalar_t* in_vec = in + q;
	__global scalar_t* out_vec = out + q;
	scalar_t sum = 0;
	
	if(q>=num_vectors)
		return;
	
	while(idx+4 <= end) {
		scalar4_t d = vload4(0, data+idx);
		uint4 c = vload4(0, col_id+idx)*num_vectors;
		scalar4_t v = (scalar4_t)(in_vec[c.x], in_vec[c.y],
		                          in_vec[c.z], in_vec[c.w]);
		sum += dot(d, v);
		idx += 4;
	}

	if(idx+2 <= end) {
		scalar2_t d = vload2(0, data+idx);
		uint2 c = vload2(0, col_id+idx)*num_vectors;
		scalar2_t v = (scalar2_t)(in_vec[c.x], in_vec[c.y]);
		sum += dot(d, v);
		idx += 2;
	}

	if(idx != end) {
		sum += data[idx] * in_vec[col_id[idx] * num_vectors];
		idx++;
	}
	
	out_vec[r * num_vectors] = sum;
}

// Local Variables:
// mode: c
// End:
