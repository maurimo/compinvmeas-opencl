
#include "scalar.cl"

__kernel void V_basis_0_Np1_cw(unsigned int height,
                               unsigned int start_col,
                               __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int c = get_global_id(1); //col index
	unsigned int bidx = start_col + c; //nth vector of this basis
	out[r + c*height] =  (r == 0) ? 1.0f : (r == bidx+1) ? -1.0f : 0;
}

__kernel void V_basis_N_Np1_cw(unsigned int height,
                               unsigned int start_col,
                               __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int c = get_global_id(1); //col index
	unsigned int bidx = start_col + c; //nth vector of this basis
	out[r + c*height] = (r == bidx) ? 1.0f : (r == bidx+1) ? -1.0f : 0;		
}

__kernel void V_basis_0_Np1_rw(unsigned int width,
                               unsigned int height,
                               unsigned int start_col,
                               __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int c = get_global_id(1); //col index
	//unsigned int bidx = height - 1 - (start_col + c); //nth vector of this basis
	unsigned int bidx = 1 + (start_col + c); //nth vector of this basis
	out[r*width + c] = (r == 0) ? 1.0f : (r == bidx) ? -1.0f : 0;
}

__kernel void V_basis_N_Np1_rw(unsigned int width,
                               unsigned int start_col,
                               __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int c = get_global_id(1); //col index
	unsigned int bidx = start_col + c; //nth vector of this basis
	out[r*width + c] = (r == bidx) ? 1.0f : (r == bidx+1) ? -1.0f : 0;		
}

__kernel void V_basis_noise_gap_rw(unsigned int width,
                                   unsigned int height,
                                   unsigned int start_col,
                                   unsigned int gap,
                                   __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int c = get_global_id(1); //col index
	unsigned int bidx = start_col + c; //nth vector of this basis
	unsigned int bidx1 = (bidx>=gap) ? bidx-gap : gap-1-bidx;
	unsigned int bidx2 = (bidx+gap) < height ? bidx+gap : 2*height-bidx-gap-1;

	//if(r == 0 && c < 50)
	//	printf("KK: %d %d %d %d\n", bidx, gap, bidx1, bidx2);
	out[r*width + c] = (r == bidx1) ? 0.5f : ((r == bidx2) ? -0.5f : 0);
}

__kernel void V_basis_noise_gap_rw_mod_1(unsigned int width,
                                         unsigned int height,
                                         unsigned int start_col,
                                         unsigned int gap,
                                         __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	unsigned int c = get_global_id(1); //col index
	unsigned int bidx = start_col + c; //nth vector of this basis
	unsigned int bidx1 = (bidx-gap+height) % height;
	unsigned int bidx2 = (bidx+gap+height) % height;

	//if(r == 0 && c < 50)
	//	printf("KK: %d %d %d %d\n", bidx, gap, bidx1, bidx2);
	out[r*width + c] = (r == bidx1) ? 0.5f : ((r == bidx2) ? -0.5f : 0);
}

// Local Variables:
// mode: c
// End:
