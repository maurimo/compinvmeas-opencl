
#include "scalar.cl"

#define gi_mult_cw_vec gi_mult_cw

#define LOCAL_SIZE 1024

__kernel void gi_mult_cw_simple(const __global scalar_t* restrict data,
                               const __global unsigned int * restrict row_ptr,
                               const __global unsigned int * restrict loc_col,
                               const __global unsigned int * restrict lookup,
                               const __global unsigned int * restrict lookup_range,
                               unsigned int height,
                               const __global scalar_t* restrict in,
                               __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	if(r > height)
		return;

	unsigned int idx = row_ptr[r];   //first col/data we will read
	unsigned int end = row_ptr[r+1]; //where we will stop
	scalar_t sum = 0;

	__local scalar_t buf[LOCAL_SIZE];

	unsigned int lsize = get_local_size(0);
	unsigned int lid = get_local_id(0);
	unsigned int gid = get_group_id(0);
	unsigned int i = lid;
	unsigned int lk_begin = lookup_range[gid];
	int lk_len = lookup_range[gid+1] - lk_begin;

	while(i < lk_len) {
		buf[i] = in[lookup[lk_begin+i]];
		i += lsize;
	}

	/*if(r == 0) {
		for(int i = 0; i<lk_len; i++)
			printf("buf[%d] = %f (%dth)\n", i, buf[i], lookup[lk_begin+i]);
	}*/

	barrier(CLK_LOCAL_MEM_FENCE);

	
	while(idx != end) {
		sum += data[idx] * buf[loc_col[idx]];
		idx++;
	}

	out[r] = sum;
}

__kernel void gi_mult_cw_vec(const __global scalar_t* restrict data,
                            const __global unsigned int * restrict row_ptr,
                            const __global unsigned int * restrict loc_col,
                            const __global unsigned int * restrict lookup,
                            const __global unsigned int * restrict lookup_range,
                            unsigned int height,
                            const __global scalar_t* restrict in,
                            __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	if(r > height)
		return;
	
	unsigned int idx = row_ptr[r];   //first col/data we will read
	unsigned int end = row_ptr[r+1]; //where we will stop
	scalar_t sum = 0;

	__local scalar_t buf[LOCAL_SIZE];

	unsigned int lsize = get_local_size(0);
	unsigned int lid = get_local_id(0);
	unsigned int gid = get_group_id(0);
	unsigned int i = lid;
	unsigned int lk_begin = lookup_range[gid];
	int lk_len = lookup_range[gid+1] - lk_begin;

	while(i < lk_len) {
		buf[i] = in[lookup[lk_begin+i]];
		i += lsize;
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	while(idx+4 <= end) {
		scalar4_t d = vload4(0, data+idx);
		uint4 c = vload4(0, loc_col+idx);
		scalar4_t v = (scalar4_t)(buf[c.s0], buf[c.s1],
		                          buf[c.s2], buf[c.s3]);
		sum += dot(d, v);
		idx += 4;
	}

	if(idx+2 <= end) {
		scalar2_t d = vload2(0, data+idx);
		uint2 c = vload2(0, loc_col+idx);
		scalar2_t v = (scalar2_t)(buf[c.s0], buf[c.s1]);
		sum += dot(d, v);
		idx += 2;
	}

	if(idx != end) {
		sum += data[idx] * buf[loc_col[idx]];
		idx++;
	}	

	out[r] = sum;
}


__kernel void gi_mult_cw_fma(const __global scalar_t* restrict data,
                            const __global unsigned int * restrict row_ptr,
                            const __global unsigned int * restrict loc_col,
                            const __global unsigned int * restrict lookup,
                            const __global unsigned int * restrict lookup_range,
                            unsigned int height,
                            const __global scalar_t* restrict in,
                            __global scalar_t* restrict out) {
	unsigned int r = get_global_id(0); //row index
	if(r > height)
		return;

	unsigned int idx = row_ptr[r];   //first col/data we will read
	unsigned int end = row_ptr[r+1]; //where we will stop
	scalar_t sum = 0;

	__local scalar_t buf[LOCAL_SIZE];

	unsigned int lsize = get_local_size(0);
	unsigned int lid = get_local_id(0);
	unsigned int gid = get_group_id(0);
	unsigned int i = lid;
	unsigned int lk_begin = lookup_range[gid];
	int lk_len = lookup_range[gid+1] - lk_begin;

	while(i < lk_len) {
		buf[i] = in[lookup[lk_begin+i]];
		i += lsize;
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	
	while(idx != end) {
		sum = fma(data[idx], buf[loc_col[idx]], sum);
		idx++;
	}

	out[r] = sum;
}

// Local Variables:
// mode: c
// End:
