
#include "scalar.cl"

#define LOCAL_SIZE 32

__kernel void norm_L1_cw(unsigned int height,
                           const __global scalar_t* restrict in,
                           __global scalar_t* restrict out) {
	__local scalar_t local_buf[LOCAL_SIZE];
	unsigned int c = get_global_id(0); //col index
	unsigned int lid = get_local_id(1);

	const __global scalar_t* incol = in + height*c;
	scalar_t norm = 0;
	for(unsigned int i = lid; i < height; i += LOCAL_SIZE)
		norm += fabs(incol[i]);
	local_buf[lid] = norm;

	barrier(CLK_LOCAL_MEM_FENCE);

	if(lid != 0)
		return;

	norm = 0;
	for(unsigned int i = 0; i < LOCAL_SIZE; i++)
		norm += local_buf[i];
	out[c] = norm;
}

__kernel void norm_Linf_cw(unsigned int height,
                           const __global scalar_t* restrict in,
                           __global scalar_t* restrict out) {
	__local scalar_t local_buf[LOCAL_SIZE];
	unsigned int c = get_global_id(0); //col index
	unsigned int lid = get_local_id(1);

	const __global scalar_t* incol = in + height*c;
	scalar_t norm = 0;
	for(unsigned int i = lid; i < height; i += LOCAL_SIZE)
		norm = max(norm, fabs(incol[i]));
	local_buf[lid] = norm;

	barrier(CLK_LOCAL_MEM_FENCE);

	if(lid != 0)
		return;

	norm = 0;
	for(unsigned int i = 0; i < LOCAL_SIZE; i++)
		norm = max(norm, local_buf[i]);
	out[c] = norm;
}

__kernel void norm_L1_rw(unsigned int height,
                         unsigned int width,
                         const __global scalar_t* restrict in,
                         __global scalar_t* restrict out) {
	unsigned int c = get_global_id(0); //col index
	unsigned int lid = get_local_id(1);
	__local scalar_t __local_buf[LOCAL_SIZE*16];
	__local scalar_t* local_buf = __local_buf + LOCAL_SIZE*get_local_id(0);

	const __global scalar_t* incol = in + c + lid*width;
	scalar_t norm = 0;
	for(unsigned int i = lid; i < height; i += LOCAL_SIZE) {
		norm += fabs(*incol);
		incol += width*LOCAL_SIZE;
	}
	local_buf[lid] = norm;

	barrier(CLK_LOCAL_MEM_FENCE);

	if(lid != 0)
		return;

	norm = 0;
	for(unsigned int i = 0; i < LOCAL_SIZE; i++)
		norm += local_buf[i];
	out[c] = norm;
}

__kernel void norm_Linf_rw(unsigned int height,
                               unsigned int width,
                               const __global scalar_t* restrict in,
                               __global scalar_t* restrict out) {
	unsigned int c = get_global_id(0); //col index
	unsigned int lid = get_local_id(1);
	__local scalar_t __local_buf[LOCAL_SIZE*16];
	__local scalar_t* local_buf = __local_buf + LOCAL_SIZE*get_local_id(0);

	const __global scalar_t* incol = in + c + lid*width;
	scalar_t norm = 0;
	for(unsigned int i = lid; i < height; i += LOCAL_SIZE) {
		norm = max(norm, fabs(*incol));
		incol += width*LOCAL_SIZE;
	}
	local_buf[lid] = norm;

	barrier(CLK_LOCAL_MEM_FENCE);

	if(lid != 0)
		return;

	norm = 0;
	for(unsigned int i = 0; i < LOCAL_SIZE; i++)
		norm = max(norm, local_buf[i]);
	out[c] = norm;
}

// Local Variables:
// mode: c
// End:
